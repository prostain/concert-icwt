import api from './api';
import { BASE_URL } from "../env";
const axios = require('axios');

export async function login (email, password) {
    return axios.post(BASE_URL+'/login', {email, password})
}

export function getArticles () {
    return api.get(BASE_URL+'/articles')
}

export function getConcerts () {
    return api.get(BASE_URL+'/concerts')
}

export function getConcertsByLocation (location) {
    return api.get(BASE_URL+'/concerts?location='+location)
}

export function getConcert (id) {
    return api.get(BASE_URL+'/concerts/'+id)
}

export function getAvailableSeats (idConcert) {
    return api.get(BASE_URL+`/concerts/${idConcert}/places`)
}

export function getHomeData () {
    return api.get(BASE_URL+'/home')
}

export function setNewsLetter (data) {
    return api.post(BASE_URL+'/newsletter',data)
}

export function getConcertsSorted(location, musicCategoryId, dateStart, dateEnd) {
    return api.get(BASE_URL+'/programmation?location=' + location + '&musicCategoryId=' + musicCategoryId + '&dateStart=' + dateStart + '&dateEnd=' + dateEnd);
}

export function getConcertsBySearch (searchText) {
    return api.get(BASE_URL+'/search?searchText='+searchText)
}

export function setNewConcert (data) {
    return api.post(BASE_URL+'/admin/concerts/new',data)
}

export function getRooms () {
    return api.get(BASE_URL+'/rooms')
}

export function saveBooking (data) {
    return api.post(BASE_URL+'/booking/new',data);
}

export function register (data) {
    return api.post(BASE_URL+'/users/new', data);
}

