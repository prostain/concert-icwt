import styled from "styled-components";
import { color } from "../../components/variable/color.style";
//import { headerLaptopBreakpoint } from "../../components/variable/common.style";

export const HeaderReservationContainer = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    background-color: ${color.darkGrey2};
    padding: 30px 0;
    margin-bottom: 20px;
`;

export const Separator = styled.div`
    margin: 0 20px;
`;

export const ReservationStepItem = styled.div`
    font-weight: ${props => props.isActive ? 'bold' : 'normal'};
`;

