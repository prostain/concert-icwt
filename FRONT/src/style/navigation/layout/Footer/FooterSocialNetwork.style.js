import styled from "styled-components";
import { headerLaptopBreakpoint } from "../../../components/variable/common.style";

export const ReseauxSociauxContainer = styled.div`
  width:50%;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 0 5px;
  font-size: 20px;

@media (min-width: ${headerLaptopBreakpoint}) {
    font-size: unset;
  }
`;

export const ReseauxSociaux = styled.div`
  margin-top: 20px;
  display: flex;
  flex-direction: row;
  align-items: center;

`;

export const ReseauxSociauxItem = styled.a`
  margin: 0 5px;
  cursor: click;
`;