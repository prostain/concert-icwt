import styled from "styled-components";
import { headerLaptopBreakpoint } from "../../../components/variable/common.style";

export const NewsLetterContainer = styled.div`
  width:50%;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 0 5px;
  font-size: 20px;

@media (min-width: ${headerLaptopBreakpoint}) {
    font-size: unset;
  }
`;

export const NewsLetter = styled.div`
  display:flex;
  align-items: center;
  flex-direction: column;

  @media (min-width: ${headerLaptopBreakpoint}) {
    flex-direction: row;

  }
`;
