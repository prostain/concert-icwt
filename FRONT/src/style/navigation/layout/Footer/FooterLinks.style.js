import styled from "styled-components";
import { Link } from "react-router-dom";
import { headerLaptopBreakpoint } from "../../../components/variable/common.style";

export const LinkContainer = styled.div`
display:flex;

align-items:center;
justify-content: center;
flex-direction: column;
font-size: 20px;

@media (min-width: ${headerLaptopBreakpoint}) {
    font-size: unset;
    flex-direction: row;
    justify-content: flex-start;
    align-items: flex-start;
  }
`;

export const LinkCard = styled.div`
display:flex;
flex-direction: column;
`;

export const LinkTag = styled(Link)`
color: white;
margin-bottom: 5px;

position: relative;
text-decoration: none;
&::before{
  content: "";
  position: absolute;
  width: 100%;
  height: 1px;
  bottom: 0;
  left: 0;
  background-color: white;
  visibility: hidden;
  transform: scaleX(0);
  transition: all 0.3s ease-in-out 0s;
}
&:hover::before{
  visibility: visible;
  transform: scaleX(1);
}
`;

export const Spacer = styled.div`
  width: 180px;
  height:2px;
  background-color: white;
  margin: 20px 0px;
  border-radius: 5px;
@media (min-width: ${headerLaptopBreakpoint}) {
    width: 2px;
    height:180px;
    margin: 0 20px;
  }
`;