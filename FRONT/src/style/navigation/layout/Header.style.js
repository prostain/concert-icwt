import styled from "styled-components";
import { headerLaptopBreakpoint } from "../../components/variable/common.style";

export const Container = styled.ul`
  display: none;
  flex-direction: column;
  height: 0;
  padding: 0;
  justify-content: center;
  @media (min-width: ${headerLaptopBreakpoint}) {
    display: flex;
    flex-direction: row;
    height: auto;
  }
`;

export const Burger = styled.div`
  & img {
    height: 50px;
    width: auto;
  }
  &:hover ${Container}, &:focus ${Container} {
    display: flex;
    height: auto;
  }
  &:hover > img, &:focus > img{
    animation-name: spin;
    animation-duration: 1s;
    @keyframes spin { 
        from { 
            transform: rotate(0deg); 
        } to { 
            transform: rotate(360deg); 
        }
    }
    transform: rotate(360deg);
  }
  @media (min-width: ${headerLaptopBreakpoint}) {
    & > img {
        display: none;
    }
  }
`;

