import styled from "styled-components";
import {color} from "../../components/variable/color.style";

export const Container = styled.div`
  padding: 0 15% 10px 15%;
  margin-top: 50px;
  background-color: ${color.darkGrey2};
  display: flex;
  flex-direction: column;
  text-align:center;
  justify-content:space-between;
`;

export const ContactContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-bottom: 50px;
`;

export const ContainerCard = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  align-items: flex-start;
  margin-bottom: 50px;
`;
