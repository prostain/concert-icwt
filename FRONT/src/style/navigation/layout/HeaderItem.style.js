import styled from "styled-components";
import { Link } from "react-router-dom";
import { headerLaptopBreakpoint } from "../../components/variable/common.style";

export const SubContainer = styled.div`
  display: none;
  flex-direction: column;
  z-index: 2;
  position: absolute;
  background-color: white;
  width: calc(100% - 20px);
  border-radius: 3px;
  right: -100%;
  padding: 10px;
  @media (min-width: ${headerLaptopBreakpoint}) {
    top: 50px;
    left: 0;
  }
`;

export const HeaderLi = styled.li`
  list-style-type: none;
  padding-left: 10px;
  padding-right: 10px;
  position: relative;
  text-align: center;
  display: flex;
  flex-direction: row;
  width: calc((100% - 150px)/7);
  &:nth-of-type(1), &:nth-last-of-type(1), &:nth-last-of-type(2) {
    width: 50px;
  }
  &:hover ${SubContainer} {
    display: flex;
  }
  @media (min-width: ${headerLaptopBreakpoint}) {
    flex-direction: column;
  }
`;

export const HeaderLink = styled(Link)`
  color: black;
  text-decoration: none;
  text-align: center;
  margin: 5px 0;
  font-size: ${props => props.issmall ? '0.7em' : '1em'};
  &:hover {
    color: rgba(0,0,0,0.7);
  }
`;
