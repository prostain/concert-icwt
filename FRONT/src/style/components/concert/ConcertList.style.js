import styled from "styled-components";
import { Link } from "react-router-dom";
import { headerLaptopBreakpoint } from "../../components/variable/common.style";

export const Container = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  margin: auto;
  max-width: 400px;

    @media (min-width: ${headerLaptopBreakpoint}) {
      flex-direction: row;
      flex-wrap: wrap;
      max-width: none;
      margin: 15px;
  }
`;

export const LinkTag = styled(Link)`
  color: white;
  text-decoration: none;
  background-color: transparent;
  border: 0;

  position: relative;
  text-decoration: none;
  &::before{
    content: "";
    position: absolute;
    width: 100%;
    height: 1px;
    bottom: 0;
    left: 0;
    background-color: white;
    visibility: hidden;
    transform: scaleX(0);
    transition: all 0.3s ease-in-out 0s;
  }
  &:hover::before{
    visibility: visible;
    transform: scaleX(1);
  }
`;

