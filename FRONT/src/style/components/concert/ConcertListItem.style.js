import styled from "styled-components";
import { color } from "../variable/color.style";
import { headerLaptopBreakpoint } from "../../components/variable/common.style";

const BUTTON_HEIGHT = '50px';

export const Container = styled.div`
  border-radius: 10px; 
  border: 1px solid black;
  background-color: ${color.white};
  color: ${color.mainBlack};
  position: relative;
  margin: 10px;
  padding: 10px;
`;

export const Box = styled.div`
  margin: 10px;
  display: flex;
  border: none;
  flex-direction: column;
  align-items: center;
  margin-bottom: 10px;

  @media (min-width: ${headerLaptopBreakpoint}) {
    flex-direction: ${props => props.horizontal ? 'row' : 'column'};
    max-width: ${props => props.horizontal ? '250px' : '150px'};

  }
`;

export const ConcertImg = styled.img`
width:100%;
  @media (min-width: ${headerLaptopBreakpoint}) {
    width: ${props => props.horizontal ? '100px' : '150px'};
    height: ${props => props.horizontal ? '100px' : '200px'};

  }
`;

export const ButtonContainer = styled.div`
    display: flex;
    justify-content: center;
    width: 100%;
    margin-bottom:10px;
`;
