import styled from "styled-components";
import { color } from "../../components/variable/color.style";

export const Container = styled.div`
    display:flex;
    flex-direction:center;
    flex-direction: column;
`;

export const Titre = styled.h2`
    width: 100%;
    padding: 20px 0;
    text-align:center;
    background-color: ${color.grey4};
    margin-bottom: 20px;
`;

export const ListPuce = styled.ul`
    display: flex;
    flex-direction: row;
    margin: 0 20px;

`;

export const Puce = styled.li`
    background-color: ${color.darkGrey1};
    list-style-type: none;
    cursor: pointer;
    padding: 10px;
    border: 1px solid ${color.grey4};
    margin: 0 1px;
    font-weight: ${props => props.isActive ? "bold" : "normal"};
    text-decoration: ${props => props.isActive ? "underline" : "normal"};;
    
    &:hover{
        background-color: ${color.grey5};
        color:${color.grey1};
    }
    &:active{
        background-color: ${color.darkGrey2};
        color:${color.grey1};
    }
`;