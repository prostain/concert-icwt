import styled from "styled-components";
import { color } from "../../../components/variable/color.style";

export const Container = styled.div`
    margin: 0px 15px;
    padding: 15px;
    border: 1px solid ${color.grey4};
    border-radius: 5px;
`;
