import styled from "styled-components";
import { color } from "../../../components/variable/color.style";

export const Container = styled.div`
    margin: 0px 15px;
    padding: 15px;
    border: 1px solid ${color.grey4};
    border-radius: 5px;
`;

export const AdminConcertHeader = styled.div`
    display: flex;
    justify-content: space-between;
    align-items:center;
    margin: 0px 15px;
    padding: 15px 30px;
    border-radius: 5px;
    background-color: ${color.grey4};
`;

export const HeaderTimestamp = styled.div`

`;

export const LastConcerts = styled.div`
    margin: 30px 15px;
`;