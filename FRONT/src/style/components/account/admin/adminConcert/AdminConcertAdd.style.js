import styled from "styled-components";
import { color } from "../../../../components/variable/color.style";
import { headerLaptopBreakpoint } from "../../../../components/variable/common.style";

export const Container = styled.form`
    margin: 0px 15px;
    padding: 15px;
    border: 1px solid ${color.grey4};
    border-radius: 5px;
    display:flex;
    flex-direction:column;
`;

export const InfoConcert = styled.div`
    display:flex;
    flex-direction:row;
    margin: 0 20%;
    justify-content: space-between;
`;

export const ConcertDesc = styled.div`
    display:flex;
    flex-direction:column;
    margin-bottom: 50px;
`;

export const ConcertTextArea = styled.textarea`
    width: 75%;
    resize: none;
    &:invalid {
        border: 1px dashed red;
    }
    &:valid {
        border: 1px solid lime;
    }
`

export const ImgConcert = styled.img`
    width:300px;
    height:500px;
`;


export const ImgInput = styled.div`
    width:100%;
`;


export const ConcertDetails = styled.div`
    display:flex;
    flex-direction: column;
    align-items:flex-start;
    box-sizing: border-box;
    width: 100%;
`;

export const ButtonList = styled.div`
    display:flex;
    flex-direction: row;
    justify-content: flex-end;

    button{
        margin: 0 10px;
    }
`;

export const ConcertDetailsItem = styled.div`
    display:flex;
    flex-direction: row;
    align-items:center;
    box-sizing: border-box;
    width: 100%;

`;

export const Col25 = styled.div`

        width: 25%;
  margin-top: 6px;
  margin-right: 10px;
text-align: end;
`;

export const Col75 = styled.div`
  width: 75%;
  margin-top: 6px;
  box-sizing: border-box;
`;

export const SmallInput = styled.input`
  width: 75px;
`;

export const InputButton = styled.input`
    padding: 10px 25px 10px 25px;
    border: 0px;
    border-radius: 5px;
    font-family: Helvetica;
    outline: none;
    height: 45px;
    cursor: pointer;
    box-shadow: 0px 1px 3px rgb(0 0 0 / 40%);
    background-color: #FFFFFF;
    margin: 0 10px;
`;

