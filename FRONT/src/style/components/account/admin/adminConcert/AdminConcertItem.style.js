import styled from "styled-components";
import { color } from "../../../../components/variable/color.style";
import { headerLaptopBreakpoint } from "../../../../components/variable/common.style";

export const Container = styled.div`
    display:flex;
    flex-direction:row;
    align-items:center;
    justify-content:flex-start;
    margin-bottom:20px;
`;

export const ConcertImg = styled.img`
    width:100%;
    object-fit: cover;
    max-width: 125px;
    width: 100%;
    height: 150px;
  @media (min-width: ${headerLaptopBreakpoint}) {
    max-width: none;
    width:30%;
    height: auto;
  }
`;

export const ConcertDetail = styled.div`
    display:flex;
    flex-direction:column;
    align-items:flex-start;
    justify-content:flex-start;
`;