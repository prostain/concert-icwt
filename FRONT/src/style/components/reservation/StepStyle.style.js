import styled from "styled-components";
import { color } from "../variable/color.style";
import { headerLaptopBreakpoint } from "../../components/variable/common.style";

export const StepContainer = styled.div`
    display: block;
    margin: 15px;
    
    @media (min-width: ${headerLaptopBreakpoint}) {
    margin: 0 15%;
  }
`;

export const StepInnerContainer = styled.div`
    background-color: ${color.grey4};
   border-radius: 0.5rem;
   padding: 20px;
   margin-bottom: 20px;
`;

export const ReservationTitle = styled.h2`
    margin-bottom: 20px;
    text-align: left;
`;

export const FlexContainer = styled.div`
   margin: 0 10%;
   display: flex;
   justify-content: space-between;
   flex-direction: row;
`;

export const NumberContainer = styled.div`
margin: 0 10%;
   display: flex;
   justify-content: space-between;
   flex-direction: row;
`;

export const AnimateDots = styled.h2`
  animation-name: ease;
  animation-duration: 2s;
  animation-iteration-count: infinite;
  overflow: hidden;
  vertical-align: middle;
  color: $orange;
  @keyframes ease {
    from {
      width: 0;
    }
    to {
      width: 20px;
    }
  }

`;
