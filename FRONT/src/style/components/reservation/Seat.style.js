import styled from "styled-components";
import { color } from "../variable/color.style";

const variable = `
    fill: ${color.darkGrey1};
    width: 16px;
    height: 16px;
    padding: 5px;
    border: 1px solid;
    border-radius: 5px;
    cursor: pointer;
    border-color: ${color.mainBlack};
`;

export const AvailableSeat = styled.svg`
${variable}
fill: ${color.white};
`;

export const UnavailableSeat = styled.svg`
${variable}
fill: ${color.grey5};
cursor: not-allowed;
`;

export const SelectedSeat = styled.svg`
${variable}
fill: ${color.mainOrange};

`;

export const SeatContainer = styled.div`
    position: relative;
`;

export const PriceIndicator = styled.div`
    position: absolute;
    left: 100%;
    background-color: black;
    padding: 5px;
    border-radius: 5px;
    z-index: 9;
`;
