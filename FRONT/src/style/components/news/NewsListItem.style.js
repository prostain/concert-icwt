import styled from "styled-components";
import { headerLaptopBreakpoint } from "../../components/variable/common.style";

export const Container = styled.div`
  width: 100%;
  @media (min-width: ${headerLaptopBreakpoint}) {
    width: 50%;
  }
  
`;

export const ContainerInner = styled.div`
  margin: 10px;
  display: flex;
  flex-direction: column;
`;

export const Box = styled.button`
  border-radius: 10px; 
  border: 1px solid black;
  padding: 10px;
  margin: 10px;
  max-width: 400px;
  @media (min-width: ${headerLaptopBreakpoint}) {
    max-width: none;
  }
`;

export const NewsImg = styled.img`
width:100%;
object-fit: cover;
min-width: 125px;
width: 100%;

  @media (min-width: ${headerLaptopBreakpoint}) {
    height: 125px;
  }
`