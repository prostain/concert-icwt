import styled from "styled-components";
import { Link } from "react-router-dom";

export const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

export const LinkTag = styled(Link)`
  color: white;
  text-decoration: none;
  &:hover{
    text-decoration: underline;
  }
`;


