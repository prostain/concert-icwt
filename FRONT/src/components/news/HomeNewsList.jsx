import React from 'react';
import { Container, LinkTag } from "../../style/components/concert/ConcertList.style";
import NewsListItem from "./NewsListItem";

const HomeNewsList = ({ data }) => {

    let content;
    if(data) {
        content = data.map(i=><NewsListItem item={i} />)
    } else {
        content = <p>Chargement ...</p>
    }
    return (
        <div>
            <h2>ACTUALITÉS</h2>
            <Container>
                {content}
            </Container>
            <LinkTag to={'/programmation'}>
                Voir toute la programmation
            </LinkTag>
        </div>
    )
};

export default HomeNewsList;
