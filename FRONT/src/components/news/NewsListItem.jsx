import React from "react";
import {
  Box,
  Container,
  ContainerInner,
  NewsImg,
} from "../../style/components/news/NewsListItem.style";
import { useHistory } from "react-router-dom";
import Button from "../shared/Button";

const NewsListItem = ({ item }) => {
  const history = useHistory();
  const onClick = () => history.push("/programmation");

  return (
    <Container>
      <Box onClick={onClick}>
        <ContainerInner onClick={onClick}>
          <NewsImg src={item.image.url} alt={"Artiste"} />
          <span>
            <h3>{item.title}</h3>
            <p>{item.description}</p>
            <Button
              text={"En savoir plus"}
              action={onClick}
              buttonStyle={"primary"}
            />
          </span>
        </ContainerInner>
      </Box>
    </Container>
  );
};

export default NewsListItem;
