import React from "react";
import { MyMap } from "../../style/components/shared/Map.style";
import { MapContainer, TileLayer, Marker, Popup} from "react-leaflet";

const Map = (props) => {
    const position = [props.lat, props.lng];
    return (
        <MyMap>
            <link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.1/dist/leaflet.css" />
            <MapContainer style={{height: 200 + 'px'}} center={position} zoom={13}>
                <TileLayer
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                <Marker position={position}>
                    <Popup>
                        A pretty CSS3 popup. <br /> Easily customizable.
                    </Popup>
                </Marker>
            </MapContainer>
        </MyMap>
    )
}

export default Map;
