import React, { useEffect, useState } from "react";
import DotLoader from "./DotLoader";
import ErrorMessage from "./ErrorMessage";
import Button from "./Button";

const ItemList = (props) => {
  const { apiRequest, renderItem, container, itemProps } = props;
  const [items, setItems] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(true);
  const [nextPage, setNextPage] = useState("");
  const ItemTag = renderItem; //capitalize for React
  const ContainerTag = container;

  async function fetch(url) {
    try {
      !isLoading && setIsLoading(true);
      const data = await apiRequest(url);
      const next = data["hydra:view"]["hydra:next"] ?? "";
      setItems((i) => [...i, ...data["hydra:member"]]);
      setNextPage(next);
      setIsError(false);
    } catch (e) {
      setIsError(true);
    } finally {
      setIsLoading(false);
    }
  }

  useEffect(() => {
    fetch();
  }, []);

  let content;
  if (isLoading && items.length === 0) {
    content = <DotLoader />;
  } else if (isError) {
    content = (
      <ErrorMessage
        message={"Erreur de récupération des données"}
        retry={fetch}
      />
    );
  } else if (items.length > 0) {
    content = items.map((i) => (
      <ItemTag key={`item_${i.id}`} item={i} {...itemProps} />
    ));
  }
  return (
    <ContainerTag>
      {content}
      {isLoading && items.length > 0 && <p>Chargement ...</p>}
      {nextPage && (
        <Button
          action={() => fetch(nextPage)}
          buttonStyle={"primary"}
          text={"Charger plus"}
        />
      )}
    </ContainerTag>
  );
};

export default ItemList;
