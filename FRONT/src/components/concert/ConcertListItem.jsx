import React from "react";
import Button from "../shared/Button";
import {
  Box,
  ButtonContainer,
  Container,
  ConcertImg,
} from "../../style/components/concert/ConcertListItem.style";
import { useHistory } from "react-router-dom";
import { getCategories } from "../../utils/concert";

const ConcertListItem = ({ item, horizontal, displayPriceRange }) => {
  const history = useHistory();

  const onClickDetail = () => {
    history.push({
      pathname: `/concert/${item.id}`,
    });
    window.location.reload();
  };

  const onClickReservation = () => {
    history.push({
      pathname: `/reservation/${item.id}`,
    });
  };

  return (
    <Container>
      <Box onClick={onClickDetail} horizontal={horizontal}>
        <div>
          <ConcertImg
            style={{ objectFit: "cover" }}
            src={item.image.url}
            alt={"Artiste"}
          />
        </div>
        <span style={{ padding: "5px" }}>
          <strong>{item.tourName}</strong>
          <p>{item.artist.name}</p>
          <p>
            {item.date}, {item.location}, {item.hour}
          </p>
          <p>Musique : {item.artist.musicCategory.name}</p>
          {displayPriceRange && <p>Tarifs : de {getCategories(item).reverse()[0].priceMin} € à {item.price.toFixed(2)} €</p>}
        </span>
      </Box>
      <ButtonContainer>
        <Button
          text={"Réserver"}
          action={onClickReservation}
          buttonStyle={"primary"}
        />
      </ButtonContainer>
    </Container>
  );
};

export default ConcertListItem;
