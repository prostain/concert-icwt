import React from 'react';
import ConcertListItem from "./ConcertListItem";
import { Container, LinkTag } from "../../style/components/concert/ConcertList.style";

const SearchConcertList = ({ data }) => {
    let content;
    if(data) {
        content = data.map((i, pos)=><ConcertListItem key={`concert_list_item_${pos}`} item={i} horizontal={true}/>)
    } else {
        content = <p>Pas de concert pour cette recherche.</p>
    }
    return (
        <div>
            <Container>
                {content}
            </Container>
            <LinkTag to={'/programmation'}>Voir toute la programmation</LinkTag>
        </div>
    )
};

export default SearchConcertList;
