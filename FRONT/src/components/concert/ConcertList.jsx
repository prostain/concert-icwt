import React from "react";
import { BASE_URL } from "../../env";
import axios from "axios";
import ConcertListItem from "./ConcertListItem";
import ItemList from "../shared/Itemlist";
import { Container } from "../../style/components/concert/ConcertList.style";

let url = BASE_URL + "/concerts";

const ConcertList = (props) => {
    async function fetchData(customUrl = null) {
        const fullUrl = customUrl ? BASE_URL + customUrl.slice(4) : url;
        const { data } = await axios.get(fullUrl);
        return data;
    }

    return (
        <div>
            <h2>Prochainement dans nos salles</h2>
            <ItemList apiRequest={fetchData} renderItem={ConcertListItem} itemProps={props} container={Container}/>
        </div>
    )
};

export default ConcertList;

