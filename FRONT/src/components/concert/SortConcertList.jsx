import React from 'react';
import ConcertListItem from "./ConcertListItem";
import { Container } from "../../style/components/concert/ConcertList.style";

const SortConcertList = ({ data }) => {
    let content;
    if(data) {
        content = data.map((i, pos)=><ConcertListItem key={`concert_list_item_${pos}`} item={i} horizontal={true}/>)
    } else {
        content = <p>Pas de concert pour cette recherche.</p>
    }
    return (
        <div>
            <Container>
                {content}
            </Container>
        </div>
    )
};

export default SortConcertList;
