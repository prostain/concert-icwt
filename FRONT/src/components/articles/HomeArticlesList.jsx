import React from 'react';
import { Container, LinkTag } from "../../style/components/concert/ConcertList.style";
import ArticlesListItem from "./ArticlesListItem";

const HomeArticlesList = ({ data }) => {

    let content;
    if(data) {
        content = data.map(i=><ArticlesListItem item={i} />)
    } else {
        content = <p>Chargement ...</p>
    }
    return (
        <div>
            <h2>ACTUALITÉS</h2>
            <Container>
                {content}
            </Container>
        </div>
    )
};

export default HomeArticlesList;
