import React from "react";
import {
  Box,
  Container,
  ContainerInner,
  ArticlesImg,
} from "../../style/components/articles/ArticlesListItem.style";
import { useHistory } from "react-router-dom";
import Button from "../shared/Button";

const ArticlesListItem = ({ item }) => {
  const history = useHistory();
  const onClick = () => history.push("/programmation");

  return (
    <Container>
      <Box onClick={onClick}>
        <ContainerInner onClick={onClick}>
          <ArticlesImg src={item.image.url} alt={"Artiste"} />
          <span>
            <h3>{item.title}</h3>
            <p>{item.description}</p>
            <Button
              text={"En savoir plus"}
              action={onClick}
              buttonStyle={"primary"}
            />
          </span>
        </ContainerInner>
      </Box>
    </Container>
  );
};

export default ArticlesListItem;
