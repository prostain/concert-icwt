import React from "react";
import { Container } from "../../../style/components/account/admin/AdminArticle.style";

const AdminArticle = () => {
  return (
    <Container>
      <h2>Gestion des articles</h2>
    </Container>
  );
};

export default AdminArticle;
