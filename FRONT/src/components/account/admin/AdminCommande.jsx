import React from "react";
import { Container } from "../../../style/components/account/admin/AdminCommande.style";

const AdminCommande = () => {
  return (
    <Container>
      <h2>Gestion des commandes</h2>
    </Container>
  );
};

export default AdminCommande;
