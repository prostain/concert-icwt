import React from "react";
import {
  Container,
  ConcertImg,
  ConcertDetail,
} from "../../../../style/components/account/admin/adminConcert/AdminConcertItem.style";
import { getCategories } from "../../../../utils/concert";

const AdminConcertItem = (props) => {
  const { item } = props;
  console.log(item);
  let date = new Date(item.date)
    .toLocaleString("fr-FR", {
      year: "numeric",
      month: "numeric",
      day: "numeric",
      hour: "numeric",
      minute: "numeric",
    })
    .replace(", ", " à ");

  const categories = getCategories(item).map((cat) => (
    <span>
      Catégorie {cat.number}, de {cat.priceMin}€ à {cat.priceMax}€
    </span>
  ));

  return (
    <Container>
      <ConcertImg src={item.image.url} alt={"Artiste"}></ConcertImg>
      <ConcertDetail>
        <span>{item.artist.name}</span>
        <span>{item.tourName}</span>
        <span>Date du concert : {date}</span>
        <span>Heure d'ouverture : {item.hourOpening}</span>
        <span>Lieu : {item.location}</span>
        <span>Catégorie de musique : {item.artist.musicCategory.name}</span>
        <span>Tarifs :</span>
        {categories}
      </ConcertDetail>
    </Container>
  );
};

export default AdminConcertItem;
