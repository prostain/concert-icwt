import React, { useState, useEffect } from "react";
import {
  Container,
  InfoConcert,
  ConcertDesc,
  ImgConcert,
  ImgInput,
  ConcertDetails,
  ButtonList,
  Col25,
  Col75,
  ConcertDetailsItem,
  SmallInput,
  InputButton,
  ConcertTextArea,
} from "../../../../style/components/account/admin/adminConcert/AdminConcertAdd.style";
import Textbox from "../../../shared/Textbox";
import DatePicker from "react-datepicker";
import { setNewConcert, getRooms } from "../../../../api/wrapper";
import { NUMBER_IN_ROW } from "../../../../utils/concert";
const currentDate = new Date();

const AdminConcertAdd = () => {
  const [concertImage, SetconcertImage] = useState("");
  const [concertName, SetconcertName] = useState("");
  const [artistName, SetartistName] = useState("");
  const [date, SetDate] = useState("");
  const [time, SetTime] = useState("");
  const [startTime, SetStartTime] = useState("");
  const [city, SetCity] = useState("");
  const [categoryStates, SetCategoryStates] = useState([]);
  const [maxPrice, SetMaxPrice] = useState("");
  const [decliningRatePercentage, SetDecliningRatePercentage] = useState("");
  const [categories, setCategories] = useState([
    {
      number: 1,
      priceMin: 0,
      priceMax: 0,
    },
    {
      number: 2,
      priceMin: 0,
      priceMax: 0,
    },
    {
      number: 3,
      priceMin: 0,
      priceMax: 0,
    },
  ]);

  const [restaurant, SetRestaurant] = useState("");
  const [parking, SetParking] = useState("");
  const [descriptionConcert, SetDescriptionConcert] = useState("");
  const [apiResponse, SetResponse] = useState("");
  const [roomList, SetRoomList] = useState("");

  let formData = {
    roomId: 2 /** TODO */,
    artistId: 1 /** TODO */,
    tourName: concertName,
    date: date,
    descriptionConcert: descriptionConcert,
    hour: time,
    hourOpening: startTime,
    priceMax: maxPrice,
    decliningRatePercentage: decliningRatePercentage,
    hasParking: parking ? 1 : 0,
    hasRestaurant: restaurant ? 1 : 0,
    tourReference: "",
    priceCategories: 2 /** TODO */,
  };

  /**TODO (Add rooms to the select) */
  async function fetchRooms() {
    const { data: response } = await getRooms();
    SetRoomList(response["hydra:member"]);
  }

  function updateCategoryState (val, index) {
    let pute = [...categoryStates];
    pute[index] = val.target.checked;
    SetCategoryStates(pute);
  }

  useEffect(() => {
    fetchRooms();
  }, []);

  useEffect(() => {
    const nbCategories = categoryStates.filter(state => state !== false).length;
    const numberOfRow = Math.trunc(108/NUMBER_IN_ROW)
    const categoryBreakPoint = Math.round(numberOfRow / nbCategories);
    let categories = [];
    for (let i = 0; i < nbCategories; i++) {
      const priceMaxFactor = 1 - decliningRatePercentage/100*categoryBreakPoint*i;
      const priceMinFactor = 1 - decliningRatePercentage/100*(categoryBreakPoint*(i+1)-1);

      categories.push({
        number: i+1,
        priceMax: Math.ceil(maxPrice * priceMaxFactor).toFixed(2),
        priceMin: Math.ceil(maxPrice * priceMinFactor).toFixed(2)
      })
    }
    setCategories(categories);
  }, [maxPrice, decliningRatePercentage, categoryStates]);

  const sendForm = async () => {
    try {
      const response = await setNewConcert(formData);
      let responseData = response.data;
      SetResponse(responseData.message);
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <Container action={sendForm}>
      <InfoConcert>
        <div>
          <Textbox
            type="text"
            placeHolder={"Mettez l'url de l'image"}
            action={(val) => {
              SetconcertImage(val.target.value);
            }}
          />
          <ImgConcert src={concertImage}></ImgConcert>
        </div>
        <ConcertDetails>
          <ConcertDetailsItem>
            <Col25>
              <span>Nom de l'artiste / groupe :</span>
            </Col25>
            <Col75>
              <Textbox
                isWhite={true}
                action={(val) => {
                  SetartistName(val.target.value);
                }}
                placeHolder={"Nom de l'artiste / groupe"}
                boxWidth={300}
              ></Textbox>
            </Col75>
          </ConcertDetailsItem>
          <ConcertDetailsItem>
            <Col25>
              <span>Nom du concert :</span>
            </Col25>
            <Col75>
              <Textbox
                isWhite={true}
                action={(val) => {
                  SetconcertName(val.target.value);
                }}
                placeHolder={"Nom du concert"}
                boxWidth={300}
              ></Textbox>
            </Col75>
          </ConcertDetailsItem>
          <ConcertDetailsItem>
            <Col25>
              <span>Date : </span>
            </Col25>
            <Col75>
              <DatePicker
                selected={currentDate}
                onChange={(val) => {
                  SetDate(val);
                }}
                showTimeSelect
                dateFormat="Pp"
              />
            </Col75>
          </ConcertDetailsItem>
          <ConcertDetailsItem>
            <Col25>
              <span>Heure :</span>
            </Col25>
            <Col75>
              <input
                onChange={(val) => {
                  SetTime(val.target.value);
                }}
                type="time"
              />
            </Col75>
          </ConcertDetailsItem>
          <ConcertDetailsItem>
            <Col25>
              <span>Heure d'ouverture :</span>
            </Col25>
            <Col75>
              <input
                onChange={(val) => {
                  SetStartTime(val.target.value);
                }}
                type="time"
              />
            </Col75>
          </ConcertDetailsItem>
          <ConcertDetailsItem>
            <Col25>
              <span>Lieu (ville) : </span>
            </Col25>
            <Col75>
              <select
                name="villes"
                id=""
                onChange={(val) => {
                  SetCity(val.target.value);
                }}
              >
                <option value="aix-en-provence">Aix-en-Provence</option>
                <option value="bourges">Bourges</option>
                <option value="cannes">Cannes</option>
                <option value="dunkerque">Dunkerque</option>
                <option value="echirolles">Echirolles</option>
              </select>
            </Col75>
            {/* RECUPERER LA LISTE DES VILLES DANS LA BDD SI ON A LE TEMPS DE LE FAIRE (PLUS PROPRE) */}
            {/* TODO */}
          </ConcertDetailsItem>
          <ConcertDetailsItem>
            <Col25>
              <span>Catégorie de tarifs : </span>
            </Col25>
            <Col75>
              <div>
                <input
                  type="checkbox"
                  onInput={(val)=>updateCategoryState(val, 0)}
                />
                <span>Catégorie 1</span>
                <input
                  type="checkbox"
                  onInput={(val)=>updateCategoryState(val, 1)}

                />
                <span>Catégorie 2</span>
                <input
                  type="checkbox"
                  onInput={(val)=>updateCategoryState(val, 2)}
                />
                <span>Catégorie 3</span>
              </div>
            </Col75>
          </ConcertDetailsItem>
          <ConcertDetailsItem>
            <Col25>
              <span>Tarif Max : </span>
            </Col25>
            <Col75>
              <SmallInput
                type="number"
                onChange={(val) => {
                  SetMaxPrice(val.target.value);
                }}
              />
            </Col75>
          </ConcertDetailsItem>
          <ConcertDetailsItem>
            <Col25>
              <span>Pourcentage tarif dégressif : </span>
            </Col25>
            <Col75>
              <SmallInput
                type="number"
                onChange={(val) => {
                  SetDecliningRatePercentage(val.target.value);
                }}
              />
            </Col75>
          </ConcertDetailsItem>
          <ConcertDetailsItem style={{ display: "flex", alignItems: "center" }}>
            <Col25>
              <span>Tranches de tarifs :</span>
            </Col25>
            <Col75>
              <div style={{ display: "flex", flexDirection: "column" }}>
                {/* TODO */}
                {categories.map(c => (
                    <div>
                      <span>Catégorie {c.number} :</span>{" "}
                      <SmallInput readOnly type="number" value={c.priceMin} />
                      <span> à </span>
                      <SmallInput readOnly type="number" value={c.priceMax} />
                    </div>
                ))}
              </div>
            </Col75>
          </ConcertDetailsItem>
          <ConcertDetailsItem>
            <Col25>
              <span>Parking :</span>
            </Col25>
            <Col75>
              <div>
                <input
                  id="parkingYes"
                  name="parking"
                  type="radio"
                  onInput={() => {
                    SetParking(true);
                  }}
                />
                <label htmlFor="parkingYes">Oui</label>
                <input
                  id="parkingNo"
                  name="parking"
                  type="radio"
                  onInput={() => {
                    SetParking(false);
                  }}
                />
                <label htmlFor="parkingNo">Non</label>
              </div>
            </Col75>
          </ConcertDetailsItem>
          <ConcertDetailsItem>
            <Col25>
              <span>Restaurant :</span>
            </Col25>
            <Col75>
              <div>
                <input
                  id="restaurantYes"
                  name="restaurant"
                  type="radio"
                  onInput={() => {
                    SetRestaurant(true);
                  }}
                />
                <label htmlFor="restaurantYes">Oui</label>
                <input
                  id="restaurantNo"
                  name="restaurant"
                  type="radio"
                  onInput={() => {
                    SetRestaurant(false);
                  }}
                />
                <label htmlFor="restaurantNo">Non</label>
              </div>
            </Col75>
          </ConcertDetailsItem>
        </ConcertDetails>
      </InfoConcert>
      <ConcertDesc>
        <h2>Description du concert</h2>
        <ConcertTextArea
          maxLength={1000}
          minLength={10}
          rows="10"
          onInput={(val) => {
            SetDescriptionConcert(val.target.value);
          }}
        />
      </ConcertDesc>
      <ButtonList>
        <InputButton type="button" value="Annuler" />
        <InputButton type="reset" value="Effacer" />
        <InputButton
          type="button"
          onClick={sendForm}
          value="Créer le concert"
        />
      </ButtonList>
      <div>{apiResponse}</div>
    </Container>
  );
};

export default AdminConcertAdd;
