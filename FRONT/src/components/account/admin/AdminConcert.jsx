import React from "react";
import {
  Container,
  AdminConcertHeader,
  HeaderTimestamp,
  LastConcerts,
} from "../../../style/components/account/admin/AdminConcert.style";
import Button from "../../shared/Button";
import AdminConcertItem from "./adminConcert/AdminConcertItem";
import { getConcerts } from "../../../api/wrapper";
import ItemList from "../../shared/Itemlist";

let today = new Date();
let dd = String(today.getDate()).padStart(2, "0");
let mm = String(today.getMonth() + 1).padStart(2, "0");
let yyyy = today.getFullYear();

let fetchConcerts = async () => {
  try {
    const response = await getConcerts();
    let responseData = response.data;
    return responseData;
  } catch (err) {
    console.log(err);
  }
};

today = dd + "/" + mm + "/" + yyyy;
const AdminConcert = (props) => {
  const { action } = props;
  return (
    <Container>
      <h2>Gestion des concerts</h2>

      <AdminConcertHeader>
        <HeaderTimestamp>Aujourd'hui nous somme le {today}</HeaderTimestamp>
        <Button text={"Ajouter un concert"} action={action}></Button>
      </AdminConcertHeader>
      <LastConcerts>Les derniers concerts créés :</LastConcerts>
      <ItemList
        apiRequest={fetchConcerts}
        renderItem={AdminConcertItem}
        itemProps={props}
        container={Container}
      />
    </Container>
  );
};

export default AdminConcert;
