import React, { useState } from "react";
import {
  Container,
  Titre,
  ListPuce,
  Puce,
} from "../../style/components/account/AccountAdmin.style";
import AdminArticle from "./admin/AdminArticle";
import AdminCommande from "./admin/AdminCommande";
import AdminConcert from "./admin/AdminConcert";
import AdminConcertAdd from "./admin/adminConcert/AdminConcertAdd";

const AccountAdmin = (props) => {
  const { CurrentUser } = props;
  console.log(CurrentUser);
  const [adminItem, SetAdminItem] = useState("AdminConcert");

  const AddNewConcert = () => {
    SetAdminItem("AdminConcertAdd");
  };
  const CurrentTab = () => {
    switch (adminItem) {
      case "AdminConcert":
        return <AdminConcert action={AddNewConcert}></AdminConcert>;
        break;
      case "AdminCommande":
        return <AdminCommande></AdminCommande>;
        break;
      case "AdminArticle":
        return <AdminArticle></AdminArticle>;
        break;
      case "AdminConcertAdd":
        return <AdminConcertAdd></AdminConcertAdd>;
        break;
    }
  };

  return (
    <Container>
      <Titre>BONJOUR {CurrentUser.firstname} - CONCERTS</Titre>
      <ListPuce>
        <Puce
          onClick={() => SetAdminItem("AdminConcert")}
          isActive={adminItem === "AdminConcert"}
        >
          Concert
        </Puce>
        <Puce
          onClick={() => SetAdminItem("AdminCommande")}
          isActive={adminItem === "AdminCommande"}
        >
          Commandes clients
        </Puce>
        <Puce
          onClick={() => SetAdminItem("AdminArticle")}
          isActive={adminItem === "AdminArticle"}
        >
          Articles
        </Puce>
      </ListPuce>
      {CurrentTab()}
    </Container>
  );
};

export default AccountAdmin;
