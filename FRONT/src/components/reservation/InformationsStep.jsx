import React from 'react';
import { StepContainer } from "../../style/components/reservation/StepStyle.style";
import StepSwitcher from "./StepSwitcher";
import CartLogin from "./informationStep/CartLogin";
import CartUnregistered from "./informationStep/CartUnregistered";
import { connect } from "react-redux";
import useCart from "./CartProvider";

const InformationStep = ({ stateToken }) => {
    const { goToNextStep } = useCart();
    if(stateToken) {
        goToNextStep();
    }
    return (
        <StepContainer>
            <div style={{display: 'flex'}}>
            <CartLogin />
            <CartUnregistered />
            </div>
        </StepContainer>
    )
}

const mapStateToProps = (state) => ({
    stateToken: state.user.token,
});

export default connect(mapStateToProps, {})(InformationStep);
