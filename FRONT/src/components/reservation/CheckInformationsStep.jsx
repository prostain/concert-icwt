import React from 'react';
import { StepContainer } from "../../style/components/reservation/StepStyle.style";
import StepSwitcher from "./StepSwitcher";
import { connect } from "react-redux";
import CartRegister from "./informationStep/CartRegister";
import CartCheckLogin from "./informationStep/CartCheckLogin";

const CheckInformationsStep = (props) => {
    const { stateToken } = props;
    return (
        <StepContainer>
            {stateToken ? <CartCheckLogin token={stateToken} /> : <CartRegister />}
            <StepSwitcher />
        </StepContainer>
    )
}

const mapStateToProps = (state) => ({
    stateToken: state.user.token,
});

export default connect(mapStateToProps, {})(CheckInformationsStep);
