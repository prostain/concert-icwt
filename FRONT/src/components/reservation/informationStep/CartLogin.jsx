import React, { useState } from "react";
import { useHistory } from "react-router-dom";

import { connect } from "react-redux";
import { login } from "../../../api/wrapper";
import { ButtonList, LoginContainer } from "../../../style/views/Login.style";
import Textbox from "../../shared/Textbox";
import Button from "../../shared/Button";
import useCart from "../CartProvider";

let textboxUsername = "";
let textboxPassword = "";

function CartLogin({ setToken }) {
    let textboxType = "text";
    let passwordboxType = "password";
    const [buttonStatus, setButtonStatus] = useState("disable");
    const {goToNextStep} = useCart();

    const onClick = async () => {
        if (buttonStatus === "disable") {
            return;
        }
        try {
            const {
                data: { token },
            } = await login(textboxUsername, textboxPassword);
            if (token) {
                setToken(token);
                goToNextStep();
            }
        } catch (e) {
            console.log(e);
        }
    };
    const onInputUsername = (val) => {
        textboxUsername = val.target.value;
        if (textboxUsername !== "" && textboxPassword !== "") {
            setButtonStatus("primary");
        } else {
            setButtonStatus("disable");
        }
    };
    const onInputPassword = (val) => {
        textboxPassword = val.target.value;
        if (textboxUsername !== "" && textboxPassword !== "") {
            setButtonStatus("primary");
        } else {
            setButtonStatus("disable");
        }
    };

    return (
        <>
            <LoginContainer>
                <h2 style={{color: 'black'}}>Vous avez déjà un compte ?</h2>
                <b style={{color: 'black', marginBottom: '40px'}}>Connectez-vous</b>
                <Textbox
                    title={"Nom d'utilisateur"}
                    type={textboxType}
                    action={onInputUsername}
                    placeHolder={"Votre nom d'utilisateur"}
                    boxWidth={200}
                />
                <Textbox
                    title={"Mot de passe"}
                    type={passwordboxType}
                    action={onInputPassword}
                    placeHolder={"Votre mot de passe"}
                    boxWidth={200}
                />
                <ButtonList>
                    <Button
                        text={"Valider"}
                        action={onClick}
                        buttonStyle={buttonStatus}
                    />
                    <Button
                        text={"Mot de passe oublié"}
                        action={onClick}
                        buttonStyle={"secondary"}
                    />
                </ButtonList>
            </LoginContainer>
        </>
    );
}

const mapStateToProps = (state) => ({
    stateToken: state.user.token,
});

const mapDispatchToProps = (dispatch) => {
    return {
        setToken: (token) => dispatch({ type: "user/setToken", payload: token }),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CartLogin);
