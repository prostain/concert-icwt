import React from "react";

import Button from "../../shared/Button";
import useCart from "../CartProvider";
import { color } from "../../../style/components/variable/color.style";

function CartUnregistered() {
    const { goToNextStep } = useCart();

    return (
        <div style={{marginLeft: '30px',color: 'black', display: 'flex', flexDirection: 'column', alignItems: 'center', padding: '20px', backgroundColor: color.white}}>
            <h2>Vous n'avez pas de compte ?</h2>
            <b style={{marginBottom: '40px'}}>Créez votre compte</b>
            <p>
                Créez votre compte et simplifiez vos réservations. Conservez vos données en toute sécurité et évitez de remlir vos informations à chaque commande. <br/>
                Gérez vos alertes e-mails pour vos artistes ou salles préférées. Téléchargez et imprimez vos E-Tickets et factures d'achat directement depuis votre compte.
            </p>

            <Button
                text={"Créer mon compte"}
                action={goToNextStep}
                buttonStyle={'primary'}
            />
        </div>
    );
}

export default CartUnregistered;
