import React, { useState } from "react";
import { useHistory } from "react-router-dom";

import { connect } from "react-redux";
import { login } from "../../../api/wrapper";
import { ButtonListRegister, LoginContainer } from "../../../style/views/Login.style";
import Textbox from "../../shared/Textbox";
import Button from "../../shared/Button";
import useCart from "../CartProvider";
import { ButtonGroup, ToggleButton } from 'react-bootstrap';
import DatePicker from "react-datepicker";
import 'react-datepicker/dist/react-datepicker.css';
import { format } from 'date-fns';
import { register } from "../../../api/wrapper";

function CartRegister({ setToken }) {
  let textboxConfirmEmail = "";
  let textboxConfirmPassword = "";

  let textboxType = "text";
  let passwordboxType = "password";
  const [buttonStatus, setButtonStatus] = useState("disable");
  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState(null);
  const [civility, setCivility] = useState('');
  const [lastname, setLastname] = useState(null);
  const [firstname, setFirstname] = useState(null);
  const [address, setAddress] = useState(null);
  const [accomodationType, setAccomodationType] = useState("");
  const [additionalAddress, setAdditionalAddress] = useState("");
  const [codePostal, setCodePostal] = useState(null);
  const [city, setCity] = useState(null);
  const [country, setCountry] = useState(null);
  const [phone, setPhone] = useState(null);
  const [dateBirthday, setDateBirthday] = useState(null);
  const { goToNextStep } = useCart();

  const onClick = async () => {
    if (buttonStatus === "disable") {
        return;
    }
    try {
      const data = saveUser();
      await register(data);
      const {
        data: { token },
      } = await login(email, password);
      setToken(token);
      goToNextStep();
    } catch (e) {
      document.querySelector('.confirm-mail').innerHTML = "L'email est déjà utilisé.";
      //alert(e);
    }
  }

  const onInputEmail = (val) => {
    setEmail(val.target.value);
  };
  const onInputConfirmMail = (val) => {
    textboxConfirmEmail = val.target.value;
    if(email !== textboxConfirmEmail){
      document.querySelector('.confirm-mail').innerHTML = "L'email n'est pas identique au précédent.";
      setButtonStatus("disable");
    } else {
      document.querySelector('.confirm-mail').innerHTML = "";
      setButtonStatus("primary");
    }
  };
  const onInputPassword = (val) => {
    setPassword(val.target.value);
  };
  const onInputConfirmPassword = (val) => {
    textboxConfirmPassword = val.target.value;
    if(password !== textboxConfirmPassword){
      document.querySelector('.confirm-password').innerHTML = "Le mot de passe n'est pas identique au précédent.";
      setButtonStatus("disable");
    } else {
      document.querySelector('.confirm-password').innerHTML = "";
      setButtonStatus("primary");
    }
  };
  const onInputCivility = (val) => {
    setCivility(val.target.value);
  };
  const onInputFirstname = (val) => {
    setFirstname(val.target.value);
  };
  const onInputLastname = (val) => {
    setLastname(val.target.value);
  };
  const onInputAddress = (val) => {
    setAddress(val.target.value);
  };
  const onInputAccomodation = (val) => {
    setAccomodationType(val.target.value);
  };
  const onInputAdditionalAddress = (val) => {
    setAdditionalAddress(val.target.value);
  };
  const onInputCodePostal = (val) => {
    setCodePostal(val.target.value);
  };
  const onInputCity = (val) => {
    setCity(val.target.value);
  };
  const onInputCountry = (val) => {
    setCountry(val.target.value);
  };
  const onInputPhone = (val) => {
    setPhone(val.target.value);
  };

  function onClickClear () {
    window.location.reload();
  }

  function saveUser () {
    const user = {
      email: email,
      password: password,
      civility: civility,
      lastname: lastname,
      firstname: firstname,
      address: address,
      accomodationType: accomodationType,
      additionalAddress: additionalAddress,
      codePostal: codePostal,
      city: city,
      country: country,
      phone: phone,
      dateBirthday: format(dateBirthday, 'dd/MM/yyyy'),
    };
    return JSON.stringify(user);
}

  const civilities = [
    { name: 'Madame', value: 'Madame'},
    { name: 'Monsieur', value: 'Monsieur'}
  ];

  return (
    <>
      <h1>CRÉATION DE VOTRE COMPTE</h1>
      <LoginContainer>
        <Textbox
          title={"Adresse e-mail *"}
          type={textboxType}
          action={onInputEmail}
          placeHolder={"Email..."}
          boxWidth={200}
        />
        <Textbox
          title={"Confirmation e-mail *"}
          type={textboxType}
          action={onInputConfirmMail}
          placeHolder={"Confirmer email..."}
          boxWidth={200}
        />
        <div className="confirm-mail" style={{textAlign: 'center', color: 'red'}}>L'email n'est pas identique au précédent.</div>
        <Textbox
          title={"Mot de passe *"}
          type={passwordboxType}
          action={onInputPassword}
          placeHolder={"Votre mot de passe..."}
          boxWidth={200}
        />
        <Textbox
          title={"Confirmation mot de passe *"}
          type={passwordboxType}
          action={onInputConfirmPassword}
          placeHolder={"Confirmer mot de passe..."}
          boxWidth={200}
        />
        <div className="confirm-password" style={{textAlign: 'center', color: 'red'}}>Le mot de passe n'est pas identique au précédent.</div>
        <p style={{textAlign: 'center', color: 'black'}}>Civilité *</p>
        <ButtonGroup toggle style={{color: 'black', marginBottom: '35px'}}>
          {civilities.map((civ, index) => (
            <ToggleButton
              key={index}
              type="radio"
              variant="contained"
              value={civ.value}
              checked={civility === civ.value}
              onChange={onInputCivility}
            >
              {civ.name}
            </ToggleButton>
          ))}
        </ButtonGroup>
        <Textbox
          title={"Nom *"}
          type={textboxType}
          action={onInputLastname}
          placeHolder={"Votre nom..."}
          boxWidth={200}
        />
        <Textbox
          title={"Prénom *"}
          type={textboxType}
          action={onInputFirstname}
          placeHolder={"Votre prénom..."}
          boxWidth={200}
        />
        <Textbox
          title={"N° et libellé de la voie *"}
          type={textboxType}
          action={onInputAddress}
          boxWidth={200}
        />
        <Textbox
          title={"Immeuble, Bâtiment, Résidence"}
          type={textboxType}
          action={onInputAccomodation}
          boxWidth={200}
        />
        <Textbox
          title={"Lieu-dit, boîte postale, etc"}
          type={textboxType}
          action={onInputAdditionalAddress}
          boxWidth={200}
        />
        <Textbox
          title={"Code postal *"}
          type={textboxType}
          action={onInputCodePostal}
          boxWidth={200}
        />
        <Textbox
          title={"Ville *"}
          type={textboxType}
          action={onInputCity}
          boxWidth={200}
        />
        <Textbox
          title={"Pays *"}
          type={textboxType}
          action={onInputCountry}
          boxWidth={200}
        />
        <Textbox
          title={"Téléphone *"}
          type={textboxType}
          action={onInputPhone}
          boxWidth={200}
        />
        <p style={{textAlign: 'center', color: 'black'}}>Date de naissance *</p>
          <DatePicker
            closeOnScroll={true}
            selected={dateBirthday}
            dateFormat='dd/MM/yyyy'
            showMonthDropdown
            showYearDropdown
            isClearable
            dropdownMode="select"
            placeholderText="Votre date de naissance..."
            onChange={dateBirthday => setDateBirthday(dateBirthday)}
          />
        <ButtonListRegister>
          <Button
            text={"Créer mon compte"}
            action={onClick}
            buttonStyle={buttonStatus}
          />
        </ButtonListRegister>
      </LoginContainer>
    </>
  );
}

const mapStateToProps = (state) => ({
  stateToken: state.user.token,
});

const mapDispatchToProps = (dispatch) => {
  return {
    setToken: (token) => dispatch({ type: "user/setToken", payload: token }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CartRegister);
