import React from "react";

import { LoginContainer } from "../../../style/views/Login.style";

import jwtDecode from "jwt-decode";


function CartCheckLogin({ token }) {
    const userInfos = jwtDecode(token);

    return (
        <>
            <LoginContainer style={{color: 'black'}}>
                <h2>Vérifier vos informations personnelles</h2>
                <select
                    name="civility"
                    id=""
                    disabled
                >
                    <option value="Madame" selected={userInfos.civility === 'Madame' ? 'selected' :''}>Madame</option>
                    <option value="Monsieur" selected={userInfos.civility === 'Monsieur' ? 'selected' :''}>Monsieur</option>
                    <option value="Autre">Autre</option>
                </select>
                <p>Adresse e-mail : {userInfos.email}</p>
                <p>Nom : {userInfos.lastname}</p>
                <p>Prénom : {userInfos.firstname}</p>
            </LoginContainer>
        </>
    );
}

export default CartCheckLogin;
