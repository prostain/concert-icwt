import React from 'react';
import Button from '../shared/Button'
import useCart from "./CartProvider";

const StepSwitcher = () => {
    const { goToPrevStep, goToNextStep, isFirstStepComplete, step, stateToken} = useCart();
    const shouldEnableNext = ((step > 0) || isFirstStepComplete())
    const shouldDisplayNext = ((step !== 3) || (step === 3 && stateToken))
    const shouldEnablePrev = ((step > 0) && isFirstStepComplete()) || step === 0

    let prevLabel = '';
    let nextLabel = '';

    switch (step) {
        case 4:
            prevLabel = 'ANNULER';
            nextLabel = 'Valider et payer';
            break;
        case 5:
            prevLabel = 'Faire une autre commande';
            nextLabel = 'Retour à la page d\'accueil';
            break;
        default:
            prevLabel = 'ANNULER';
            nextLabel = 'VALIDER';
            break;
    }
    return (
        <div style={{display: 'flex', justifyContent: goToPrevStep ? 'space-between' : 'flex-end'}}>
            {<Button disabled={!shouldEnablePrev ? 'disabled' : ''} action={goToPrevStep} buttonStyle={'primary'} text={prevLabel} />}
            {shouldDisplayNext && <Button disabled={!shouldEnableNext ? 'disabled' : ''} action={goToNextStep} buttonStyle={'primary'} text={nextLabel} />}
        </div>
    )
}

export default StepSwitcher;
