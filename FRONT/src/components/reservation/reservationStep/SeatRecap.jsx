import React from 'react';
import { getSeatPosition } from "../../../utils/concert";
import useCart from "../CartProvider";


const SeatRecap = () => {

    const { reservedSeats } = useCart();

    if(reservedSeats.length === 0){
        return <p style={{textAlign: 'center', padding: '50px', fontWeight: 'bold'}}>Aucune place choisie</p>
    }

    const total = reservedSeats.reduce((total, seat) => total+seat.price*1, 0)
    return (
        <div style={{border: '1px solid black', margin: '10px 2px', padding: '10px'}}>
            <p>Nombre de places choisies : </p>
            {reservedSeats.map((seat, index)=> (
                <div style={{display: 'flex', justifyContent: 'space-between', width: '50%', marginLeft: '20px', padding: '2px 0'}} key={`seat_recap_${index}`}>
                    <span>{index + 1}.</span>
                    <span>{getSeatPosition(seat)}</span>
                    <span>{seat.price} €</span>
                </div>
            ))}
            <p>Vous avez choisi {reservedSeats.length} places pour un montant total de {total} €.</p>
        </div>
    )
}

export default SeatRecap;
