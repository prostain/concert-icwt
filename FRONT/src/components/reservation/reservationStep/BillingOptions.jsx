import React, { useEffect } from 'react';
import useCart from "../CartProvider";

const BillingOptions = () => {
    const { billingOption, updateBillingOption } = useCart();

    function onChangeValue(event) {
        updateBillingOption(event.target.value)
    }

    return (
        <div style={{border: '1px solid black', margin: '10px 2px', padding: '10px', display: 'flex'}}>
            <div onChange={onChangeValue}>
                <div style={{display: 'flex', padding: '10px', alignItems: 'center', justifyContent: 'space-between'}}>
                    <div style={{display: 'flex', padding: '10px', alignItems: 'center'}}>
                        <input id={'e-ticket'} type="radio" value="1" name="option" defaultChecked={billingOption === 'e-ticket'} />
                        <label htmlFor="e-ticket">E-Ticket gratuit</label>
                    </div>
                </div>
                <div style={{display: 'flex', padding: '10px', alignItems: 'center', justifyContent: 'space-between'}}>
                    <div style={{display: 'flex', padding: '10px', alignItems: 'center'}}>
                        <input id={'retrait'} type="radio" value="2" name="option" defaultChecked={billingOption === 'retrait'} />
                        <label htmlFor="retrait"> Retrait au guichet 1,80 €</label>
                    </div>
                </div>
                <div style={{display: 'flex', padding: '10px', alignItems: 'center', justifyContent: 'space-between'}}>
                    <div style={{display: 'flex', padding: '10px', alignItems: 'center'}}>
                      <input id={'envoi'} type="radio" value="3" name="option" defaultChecked={billingOption === 'envoi'} />
                      <label htmlFor="envoi"> Envoi postal 8,00 €</label>
                    </div>
                </div>
            </div>
                <div style={{display: 'flex', justifyContent: 'space-around', flexDirection: 'column'}}>
                <div>Imprimez vos billets chez vous dès la fin de votre commande et recevez-les également par e-mail en format pdf.</div>
                <div>Retirez vos billets auprès de nos guichets (comprend des frais de transaction).</div>
                <div>Recevez vos billets à votre domicile ou sur votre lieu de travail. <br/> Envoi suivi avec remise contre signature. Livraison sous 24 à 48h.</div>
            </div>
        </div>
    )
}

export default BillingOptions;
