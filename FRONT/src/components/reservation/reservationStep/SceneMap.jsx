import React from 'react';
import { color } from "../../../style/components/variable/color.style";
import { FlexContainer, NumberContainer } from "../../../style/components/reservation/StepStyle.style";
import Seat from "./Seat";
import { getAvailableSeats } from "../../../api/wrapper";
import { getCategories, searchAndUpdateSeats, updatePrices } from "../../../utils/concert";
import useCart from "../CartProvider";

const ROW_LETTERS = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

const SceneMap = (props) => {
    const { places, setPlaces, concert } = useCart();
    const { readOnly } = props;
    const categoryBreakPoint = Math.round(places.length / concert.priceCategories);

    const choosingSeat = async (row, placeInRow) => {
        const { data: refreshedPlaces } = await getAvailableSeats(concert.id);
        const newPlaces = [...places];
        searchAndUpdateSeats(newPlaces, refreshedPlaces);
        if (newPlaces[row][placeInRow].available === "unavailable") {
            return;
        }

        newPlaces[row][placeInRow].available =
            newPlaces[row][placeInRow].available === "selected"
                ? "available"
                : "selected";
        setPlaces(newPlaces);
    };

    function getRowNumbers() {
        return (
            <NumberContainer>
                <div style={{width: '28px', justifySelf: 'flex-start'}} />
                {places[0].map((place, index) => <div style={{ width: '20px', textAlign: 'center'}} key={`row_number_${index}`}>{index + 1}</div>)}
            </NumberContainer>
        )
    }

    let categoryIndicators = getCategories(concert).map(category => <p>CAT {category.number} - De {category.priceMax}€ à {category.priceMin}€</p>)

    return (
        <div style={{ display: 'flex', minWidth: '650px', width: '100%' }}>
            <div style={{ display: 'flex', marginTop: '100px', flexDirection: 'column', justifyContent: 'space-around' }}>
                {categoryIndicators}
            </div>
            <div style={{ position: "relative", backgroundColor: color.darkGrey1,  padding: '20px 0', width: '80%' }}>
                <FlexContainer style={{justifyContent: 'center', padding: '20px 0', marginBottom: '20px', backgroundColor: color.grey2, color: 'black'}}>SCENE</FlexContainer>
                <div>
                    {getRowNumbers()}
                    {places.map((items, i) => {
                        const shouldBreak = (i + 1) % categoryBreakPoint === 0;
                        return (
                            <FlexContainer
                                style={{ marginBottom: shouldBreak ? "50px" : 0 }}
                                key={`place_row_${i}`}
                            >
                                <span style={{width: '10px', padding: '5px 10px'}}>{ROW_LETTERS[i]}</span>
                                {items.map((p, index) => {
                                    return (
                                        <Seat
                                            id={p.id}
                                            seatStyle={p.available}
                                            price={p.price}
                                            action={readOnly ? undefined : () => choosingSeat(i, index)}
                                            key={`seat_number_${i}_${index}`}
                                        />
                                    );
                                })}
                            </FlexContainer>
                        );
                    })}
                </div>
            </div>
        </div>
    )
}

export default SceneMap;
