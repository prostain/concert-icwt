import React from 'react';
import { StepContainer } from "../../style/components/reservation/StepStyle.style";
import StepSwitcher from "./StepSwitcher";
import { connect } from "react-redux";
import jwtDecode from "jwt-decode";
import ReservationRecap from "../ReservationRecap";

const PaymentStep = (props) => {
    const userInfos = jwtDecode(props.stateToken);

    return (
        <StepContainer>
            <h1>Bonjour {userInfos.firstname}</h1>
            <ReservationRecap readOnly/>
            <div>
                <h2 style={{textAlign: 'left', textDecoration: 'underline'}}>Votre paiement</h2>
                <div  style={{padding: '10px 0'}}>
                    <label htmlFor="numeroCarte">Numéro de carte : </label><input id={'numeroCarte'} type="text"/>
                </div>
                <div  style={{padding: '10px 0'}}>
                    <label htmlFor="expiration">Date d'expiration : </label><input id={'expiration'} type="text"/>
                </div>
                <div  style={{padding: '10px 0'}}>
                    <label htmlFor="crypto">Cryptogramme visuel : </label><input id={'crypto'} type="text"/>
                </div>
            </div>
            <StepSwitcher />
        </StepContainer>
    )
}

const mapStateToProps = (state) => ({
    stateToken: state.user.token,
});

export default connect(mapStateToProps, {})(PaymentStep);

