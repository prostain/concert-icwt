import React from 'react';
import { StepContainer } from "../../style/components/reservation/StepStyle.style";
import StepSwitcher from "./StepSwitcher";
import { connect } from "react-redux";
import jwtDecode from "jwt-decode";
import ReservationRecap from "../ReservationRecap";
import useCart from "./CartProvider";

const RecapStep = (props) => {
    const userInfos = props.stateToken ? jwtDecode(props.stateToken) : null;
    const { cartId } = useCart();

    return (
        <StepContainer>
            <h1>Merci {userInfos?.firstname} pour votre achat !</h1>
            <p>
                La référence de cette réservation est le {cartId}.
                Vous allez recevoir un e-mail de confirmation. <br/>
                Si vous avez opté pour l'obtention des billets en mode "E-Ticket" vous pouvez le télécharger et l'imprimer depuis votre compte (ou sur cette page en cliquant sur VOIR). <br/>
                Si vous avez réservé une place de parking et/ou une place de au restaurant, vous pouvez saisir la référence de la réservation ou présenter le billet pour y accéder.
            </p>
            <ReservationRecap title={'Détails des réservations'} readOnly/>
            <StepSwitcher />
        </StepContainer>
    )
}

const mapStateToProps = (state) => ({
    stateToken: state.user.token,
});

export default connect(mapStateToProps, {})(RecapStep);

