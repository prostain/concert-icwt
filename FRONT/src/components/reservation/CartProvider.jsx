import React, { createContext, useContext, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { CART_STORAGE_KEY, getCart, getSeats, searchAndUpdateSeats, updatePrices } from "../../utils/concert";
import { getAvailableSeats, saveBooking } from "../../api/wrapper";
import { useHistory } from "react-router-dom";
import jwtDecode from "jwt-decode";

const CartContext = createContext({});

const Provider = ({ children, stateToken }) => {
    const [concert, setConcert] = useState(getCart().concert ?? null);
    const [reservedSeats, setReservedSeats] = useState(getCart().reservedSeats ?? []);
    const [places, setPlaces] = useState(getCart().places ?? null);
    const [billingOption, setBillingOption] = useState(getCart().billingOption ?? null);
    const [cartId, setCartId]  = useState(getCart().cartId ?? null);
    const [step, setStep] = useState(cartId ? 5 : 0);

    const history = useHistory();
    const userInfos = stateToken ? jwtDecode(stateToken) : null;

    useEffect(()=>{
        if(!places || places.length < 1){
            return
        }
        const reserved = [];
        places.forEach(row => row.forEach(seat => seat.available === 'selected' && reserved.push(seat)))
        setReservedSeats(reserved);
        saveCart({
            reservedSeats: reserved
        });
    }, [places]);

    async function saveConcert (unSavedConcert) {
        setConcert(unSavedConcert);
        const unSavedPlaces = getSeats(unSavedConcert);
        setPlaces(unSavedPlaces);
        const { data: reserved } = await getAvailableSeats(unSavedConcert.id);
        const newPlaces = [...unSavedPlaces];
        updatePrices(newPlaces, unSavedConcert);
        searchAndUpdateSeats(newPlaces, reserved);
        setReservedSeats([]);
        saveCart({
            concert: unSavedConcert,
            places: unSavedPlaces,
            reservedSeats: [],
            cartId
        });//always persist the data
    }

    function updateBillingOption (option) {
        setBillingOption(option);
        saveCart({
            billingOption: option
        });
    }

    function removeSeatReservation (seat) {
        const newSeats = reservedSeats.filter(r=>r.id !== seat.id)
        setReservedSeats(newSeats);
        const newPlaces = [...places]
        newPlaces.forEach(row=>{
            const target = row.findIndex(s=>s.id === seat.id)
            row.splice(target,1)
        })
        setPlaces(newPlaces)

        saveCart({
            places: newPlaces,
            reservedSeats: newSeats
        });
    }

    function saveCart (newCart) {
        const cart = {
            concert: newCart?.concert ?? concert,
            reservedSeats: newCart?.reservedSeats ?? reservedSeats,
            places: newCart?.places ?? places,
            billingOption: newCart?.billingOption ?? billingOption,
            cartId: cartId
        };
        localStorage.setItem(CART_STORAGE_KEY, JSON.stringify(cart));
    }

    function goToPrevStep() {
        if(stateToken && step === 3) {
            setStep(step - 2)
        }
        else if(step === 5) {
            localStorage.removeItem(CART_STORAGE_KEY);
            history.replace('/programmation')
            return;
        }
        else if (step > 0) {
            setStep(step - 1);
        } else {
            history.replace('/concert/'+concert.id);
        }
    }

    function goToNextStep() {
        if(stateToken && step === 1) {
            setStep(step + 2)
        }
        else if(!stateToken && step === 3) {
            return;
        }
        else if(step === 4) {
            createBooking();
            setStep(step + 1)
        }
        else if (step < 5) {
            setStep(step + 1);
        } else {
            history.push('/programmation');
        }
    }

    async function createBooking () {
        const cart = {
            seats: reservedSeats.map(seat => seat.id),
            billingOption: billingOption*1,
            concertId: concert.id,
            userId: userInfos.userId
        }
        const { data: { id } } = await saveBooking(cart);
        setCartId(id.toFixed().padStart(7, "0"));
    }

    function isFirstStepComplete () {
        return (concert && reservedSeats.length > 0 && billingOption )
    }

    const providerValues = {
        concert,
        reservedSeats,
        places,
        step,
        setStep,
        isFirstStepComplete,
        saveConcert,
        goToPrevStep,
        goToNextStep,
        updateBillingOption,
        removeSeatReservation,
        cartId,
        setCartId,
        billingOption,
        setPlaces,
        stateToken
    };

    return (
        <CartContext.Provider value={providerValues}>
            {children}
        </CartContext.Provider>
    );
};

const mapStateToProps = (state) => ({
    stateToken: state.user.token,
});

export const CartProvider = connect(mapStateToProps, null)(Provider);

export default function useCart() {
    return useContext(CartContext);
}
