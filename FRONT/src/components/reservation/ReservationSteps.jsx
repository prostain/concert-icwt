import React from "react";
import CartStep from "./CartStep";
import InformationStep from "./InformationsStep";
import PaymentStep from "./PaymentStep";
import RecapStep from "./RecapStep";
import ReservationStep from "./ReservationStep";
import HeaderReservation from "../../navigation/layout/HeaderReservation";
import CheckInformationsStep from "./CheckInformationsStep";
import useCart from "./CartProvider";
import { useHistory } from "react-router-dom";

function ReservationSteps(props) {
    const { step, setStep, concert, cartId } = useCart();
    let content;
    const history = useHistory()

    let concertId = props.location.pathname.replace("/reservation/", "");
    if (!/\d+/.test(concertId)) {
        concertId = null;
        setStep(1);
        if(concert && concert.id){
            history.replace('/reservation/'+concert.id)
            return <div />
        }
    }

    switch (step) {
        case 0 && (concertId || concert):
            content = <ReservationStep concertId={concertId} />;
            break;
        case 1 || !concertId:
            content = <CartStep />;
            break;
        case 2:
            content = <InformationStep />;
            break;
        case 3:
            content = <CheckInformationsStep />;
            break;
        case 4:
            content = <PaymentStep />;
            break;
        case 5:
            content = <RecapStep />;
            break;
        default:
            content = <ReservationStep />;
            break;
    }

    return (
        <>
            <HeaderReservation step={step} />
            {content}
        </>
    );
}

export default ReservationSteps;
