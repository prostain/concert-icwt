import React, { useEffect, useState } from "react";
import {
  StepContainer,
  StepInnerContainer,
  ReservationTitle,
} from "../../style/components/reservation/StepStyle.style";
import StepSwitcher from "./StepSwitcher";
import { getConcert } from "../../api/wrapper";
import SceneMap from "./reservationStep/SceneMap";
import { color } from "../../style/components/variable/color.style";
import SeatRecap from "./reservationStep/SeatRecap";
import BillingOptions from "./reservationStep/BillingOptions";
import useCart from "./CartProvider";
import DotLoader from "../shared/DotLoader";
import Map from "../shared/Map";

const ReservationStep = ({ concertId }) => {
  const { concert, saveConcert } = useCart();
  const [concertIsLoading, setConcertISLoading] = useState(true);


  useEffect(() => {
    async function fetchData() {
      setConcertISLoading(true);
      const { data: newConcert } = await getConcert(concertId);

      if (!concert || concert.id !== newConcert.id) {
        await saveConcert(newConcert);
      }
        setConcertISLoading(false);
    }
    fetchData();
  }, []);

  let content;
  if(concertIsLoading) {
    content = <DotLoader />
  }
  else {
    content = <>
        <div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
          <div style={{padding: '10px', backgroundColor: color.grey2, color: color.mainBlack}}>
            <h3>{concert.artist.name}</h3>
            <div style={{display: 'flex', alignItems: 'center'}}>
              <img width={150} height={150} src={concert.image.url} alt="Affiche du concert"/>
              <div style={{display: 'flex', alignItems: 'center', flexDirection: 'column', marginLeft: '10px'}}>
                <p>{concert.date}</p>
                <p>à {concert.location}</p>
                <p>Musique {concert.artist.musicCategory.name}</p>
              </div>
            </div>
          </div>
            <Map lat={concert.room.latitude} lng={concert.room.longitude}/>
        </div>

        <ReservationTitle>1. Choisissez vos places sur le plan :</ReservationTitle>
          <SceneMap />
          <SeatRecap />
          <ReservationTitle>2. Choisissez le mode d'obtention des billets :</ReservationTitle>
          <BillingOptions />
      </>
  }

  return (
    <StepContainer>
      <StepInnerContainer>
        {content}
      </StepInnerContainer>
      <StepSwitcher />
    </StepContainer>
  );
};

export default ReservationStep;
