import React from "react";
import { StepContainer } from "../../style/components/reservation/StepStyle.style";
import StepSwitcher from "./StepSwitcher";
import SceneMap from "./reservationStep/SceneMap";
import { getSeatPosition } from "../../utils/concert";
import useCart from "./CartProvider";
import { Link } from "react-router-dom";
import ReservationRecap from "../ReservationRecap";

const CartStep = () => {
    const { concert, reservedSeats, isFirstStepComplete} = useCart();

    if(!isFirstStepComplete()) {
        return (
            <StepContainer>
                <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'center'}}>
                <h2>Votre panier est vide</h2>
                <Link  style={{width: '100%', textAlign: 'center'}} to={'/programmation'}>Parcourir les concerts à venir</Link>
                </div>
            </StepContainer>
        )
    }
  return (
    <StepContainer>
        <div style={{display: 'flex', justifyContent: 'space-between'}}>
            {concert && <SceneMap readOnly />}
            <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'space-around', margin: '10px', padding: '10px', border: '1px solid black', textAlign: 'center'}}>
                <div>
                    <p>{concert.artist.name}</p>
                    <p>{concert.tourName}</p>
                    <p>{concert.location}</p>
                </div>
                {<div>
                    {reservedSeats.map(s => <p key={`seat_recap_cart_step_${s.id}`}>{getSeatPosition(s)}</p>)}
                </div>}
            </div>
        </div>
        {reservedSeats.length > 0 ? <ReservationRecap /> : <div style={{textAlign: 'center'}}>Aucune place réservée</div>}
        <StepSwitcher />
    </StepContainer>
  );
};

export default CartStep;
