import React from 'react';
import useCart from "./reservation/CartProvider";
import { getOptionLabel, getOptionPrice } from "../utils/concert";

const ReservationRecap = (props) => {
    const { readOnly, title } = props;
    const { concert, reservedSeats, removeSeatReservation, billingOption } = useCart();

    function getCartTotal() {
        let billingPrice = billingOption === 'retrait' ? 1.80 : billingOption === 'envoi' ? 8.00 : 0;
        return (reservedSeats.reduce((total, seat)=>total+seat.price*1, 0) +  billingPrice).toFixed(2);
    }

    return (
        <div>
            <h2 style={{textAlign: 'left', textDecoration: 'underline'}}>{title || 'Récapitulatif de votre panier'}</h2>
            <table style={{width: '100%', textAlign: 'center'}}>
                <thead>
                <tr>
                    <th>N°</th>
                    <th>Nombre de places</th>
                    <th>Artiste / Groupe</th>
                    <th>Lieu</th>
                    <th>Date et heure</th>
                    <th>Catégorie de tarifs</th>
                    <th>Tarif</th>
                    {!readOnly && <th />}
                </tr>
                </thead>
                <tbody>
                {reservedSeats.map((reserved, index)=><tr>
                    <td style={{padding: '10px'}}>{index+1}.</td>
                    <td style={{padding: '10px'}}>1 place</td>
                    <td style={{padding: '10px'}}>{concert.artist.name}</td>
                    <td style={{padding: '10px'}}>{concert.location}</td>
                    <td style={{padding: '10px'}}>{new Date(concert.date).toLocaleString()}</td>
                    <td style={{padding: '10px'}}>Catégorie {reserved.category}</td>
                    <td style={{padding: '10px'}}>{reserved.price}€</td>
                    {!readOnly && <td style={{padding: '10px', cursor: 'default'}} onClick={()=> removeSeatReservation(reserved)}><img width={30} height={30} src="/assets/recycle-bin.svg" alt="Supprimer"/></td>}
                </tr>)}
                </tbody>
            </table>
            <div style={{display: 'flex', margin: '10px', padding: '10px', border: '1px solid black', justifyContent: 'space-between'}}>
                <div>Obtention des billets : </div>
                <div>{concert.artist.name}</div>
                <div>{getOptionLabel(billingOption)}</div>
                <div>{getOptionPrice(billingOption)}</div>
            </div>
            <h2 style={{textAlign: 'right'}}>MONTANT TOTAL DU PANIER {getCartTotal()} €</h2>
        </div>
    )
}

export default ReservationRecap;
