import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import './index.css';
import reportWebVitals from './reportWebVitals';
import { RootNavigator } from "./navigation/RootNavigator";
import store from "./store/store";
import Header from "./navigation/layout/Header";
import SearchBox from "./navigation/layout/SearchBox"
import Footer from "./navigation/layout/Footer";
import { HashRouter } from "react-router-dom";

const RootComponent = () => {
  return(
  <Provider store={store}>
    <HashRouter>
      <Header />
      <SearchBox />
      <RootNavigator />
      <Footer />
    </HashRouter>
  </Provider>
  )
}

ReactDOM.render(
  <React.StrictMode>
      <RootComponent />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
