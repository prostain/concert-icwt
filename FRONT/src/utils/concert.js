import React from "react";

export const NUMBER_IN_ROW = 12;
export const CART_STORAGE_KEY = 'cart_key';
const ROW_LETTERS = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

export function getSeats (concert) {
    const numberOfRow = Math.trunc(concert.room.numberSeat/NUMBER_IN_ROW)
    const scene = [];
    let increment = 1;
    for (let i = 1; i <= numberOfRow; i++) {
        scene.push([]);
        for (let j = 1; j <= NUMBER_IN_ROW; j++) {
            scene[i-1].push({
                id: increment++,
                available: "available",
                row: i-1,
                column: j-1,
                price: 1
            });
        }
    }
    return scene;
}

export function searchAndUpdateSeats(currentSeats, unavailableSeats) {
    unavailableSeats.forEach((r) => {
        currentSeats.forEach(row =>{
            const reservedPlace = row.find(p=>r.seat === p.id)
            if(reservedPlace){
                reservedPlace.available = 'unavailable';
            }
        })
    });
}


/**
 * Change le prix de chaque place en fonction du prix max
 * et du tarif dégressif
 * @param places : any[] places à actualiser
 * @param concert
 */
export function updatePrices(places, concert) {
    places.forEach((row, index) => {
        const priceFactor = 1 - concert.decliningRatePercentage/100*index
        row.forEach((place, i) => {
            place.price =  Math.ceil(concert.price * priceFactor).toFixed(2);
        })
    })
}

export function getSeatPosition (seat) {
    return `Place ${seat.column + 1}, Rang ${ROW_LETTERS[seat.row]}`;
}

export function getOptionLabel (option) {
   switch (option) {
       case 'retrait':
           return 'Retrait au guichet';
       case 'e-ticket':
           return 'E-Ticket';
       case 'envoi':
           return 'Envoi postal';
       default:
           return 'E-Ticket';
   }
}

export function getOptionPrice (option) {
   switch (option) {
       case 'retrait':
           return '1.80 €';
       case 'e-ticket':
           return 'gratuit';
       case 'envoi':
           return '8.00 €';
       default:
           return 'gratuit';
   }
}

export function getCart () {
    const cartString = localStorage.getItem(CART_STORAGE_KEY);
    if (cartString) {
        const cart = JSON.parse(cartString);
        return cart;
    } else {
        return {}
    }
}

export function getCategories (concert) {
    const numberOfRow = Math.trunc(concert.room.numberSeat/NUMBER_IN_ROW)
    const categoryBreakPoint = Math.round(numberOfRow / concert.priceCategories);
    let categories = [];
    for (let i = 0; i < concert.priceCategories; i++) {
        const priceMaxFactor = 1 - concert.decliningRatePercentage/100*categoryBreakPoint*i;
        const priceMinFactor = 1 - concert.decliningRatePercentage/100*(categoryBreakPoint*(i+1)-1);

        categories.push({
            number: i+1,
            priceMax: Math.ceil(concert.price * priceMaxFactor).toFixed(2),
            priceMin: Math.ceil(concert.price * priceMinFactor).toFixed(2)
        })
    }
    return categories;
}
