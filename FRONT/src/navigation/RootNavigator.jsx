import React from 'react';
import routes from "./routes";
import { Switch } from "react-router";
import { CustomRoute } from "./CustomRoute";

export const RootNavigator = () => {
    return (
        <Switch>
            {Object.keys(routes).map((routeName)=>
                <CustomRoute
                    key={routeName}
                    name={routeName}
                    path={routes[routeName].route}
                    component={routes[routeName].component}
                />
            )}
       </Switch>
    )
}

