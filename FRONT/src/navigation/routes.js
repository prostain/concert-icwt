import Home from "../views/Home";
import Login from "../views/Login";
import Posts from "../views/Posts";
import Programmation from "../views/Programmation";
import Restauration from "../views/Restauration";
import Parking from "../views/Parking";
import Privatisation from "../views/Privatisation";
import Actualites from "../views/Actualites";
import Infos from "../views/Infos";
import Contact from "../views/Contact";
import Panier from "../views/Panier";
import Reservation from "../views/Reservation";
import Account from "../views/Account";
import InfosFaq from "../views/Infos_faq";
import Concert from "../views/Concert";
import Search from "../views/Search";
import Mention from "../views/Mention";
import Cgu from "../views/Cgu";
import Register from "../views/Register";


const routes = {
    posts: {
        route: '/posts',
        displayName: 'Posts',
        component: Posts
    },
    programmation: {
        route: '/programmation',
        displayName: 'Programmation',
        component: Programmation
    },
    restauration: {
        route: '/restauration',
        displayName: 'Restauration',
        component: Restauration
    },
    parking: {
        route: '/parking',
        displayName: 'Parking',
        component: Parking
    },
    privatisation: {
        route: '/privatisation',
        displayName: 'Privatisation',
        component: Privatisation
    },
    actualites: {
        route: '/actualites',
        displayName: 'Actualités',
        component: Actualites
    },
    infosPratiques: {
        route: '/infos',
        displayName: 'Comment venir',
        component: Infos
    },
    infosFaq: {
        route: '/infos-faq',
        displayName: 'FAQ',
        component: InfosFaq
    },
    contact: {
        route: '/contact',
        displayName: 'Contact',
        component: Contact
    },
    login: {
        route: '/login',
        displayName: 'Connexion',
        component: Login
    },
    register: {
        route: '/register',
        displayName: 'Inscription',
        component: Register
    },
    compte: {
        route: '/compte',
        displayName: 'Profil',
        component: Account
    },
    panier: {
        route: '/panier',
        displayName: 'Panier',
        component: Panier
    },
    reservation: {
        route: '/reservation',
        displayName: 'Réservation',
        component: Reservation
    },
    concert: {
        route: '/concert',
        displayName: 'Concert',
        component: Concert
    },
    search: {
        route: '/search',
        displayName: 'Search',
        component: Search
    },
    mention: {
        route: '/politiques-mentions',
        displayName: 'Politiques-mentions',
        component: Mention
    },
    cgu: {
        route: '/politiques-cgu',
        displayName: 'Politiques-cgu',
        component: Cgu
    },
    politique: {
        route: '/politiques',
        displayName: 'Politiques',
        component: Cgu
    },
    home: {
        route: '/',
        displayName: 'Accueil',
        component: Home
    }
}

export default routes;
