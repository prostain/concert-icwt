import React, { useEffect } from 'react';
import routes from "./routes";
import { Route } from "react-router";

export const CustomRoute = (props) => {
    const { name } = props;

    useEffect(() => {
        document.title = `Concert ICWT - ${routes[name].displayName}`
    }, [name])

    return (
        <Route
            {...props}
        />
    )
}

