import React from "react";
import {
  Container,
  ContainerCard,
  ContactContainer,
} from "../../style/navigation/layout/Footer.style";
import FooterLinks from "./Footer/FooterLinks";
import FooterNews from "./Footer/FooterNews";
import FooterSocialNetwork from "./Footer/FooterSocialNetwork";

const Footer = () => (
  <Container>
    <ContactContainer>
      <ContainerCard>
        <FooterSocialNetwork />
        <FooterNews />
      </ContainerCard>
      <FooterLinks />
    </ContactContainer>
    <div>&copy; 2021 Symfony Concert - Tous droits réservés</div>
  </Container>
);

export default Footer;
