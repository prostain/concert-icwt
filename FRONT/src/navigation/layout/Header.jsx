import React from 'react';
import HeaderItem from "./HeaderItem";
import { Burger, Container } from '../../style/navigation/layout/Header.style';
import { getCart } from "../../utils/concert";

const HEADER_DROPDOWNS = [
    { title: <img width={75} style={{borderRadius: '50%'}} src={'https://e7.pngegg.com/pngimages/1013/770/png-clipart-eyedyllic-music-record-label-concert-logo-round-eyes-text-logo.png'} alt={'home'}/> , route: '/'},
    { title: 'Programmation', route: '/programmation', subLinks: [
        {title: 'Tous les évènements', route: '/programmation'},
        {title: 'Aix-en-Provence', route: '/programmation'},
        {title: 'Bourges', route: '/programmation'},
        {title: 'Cannes', route: '/programmation'},
        {title: 'Dunkerque', route: '/programmation'},
        {title: 'Echirolles', route: '/programmation'},
        {title: 'Comment réserver ?', route: '/programmation'},
    ] },
    { title: 'Restauration', route: '/restauration', subLinks: [
        {title: 'Présentation', route: '/restauration'},
        {title: 'Réserver', route: '/restauration'},
    ] },
    { title: 'Parking', route: '/parking', subLinks: [
            {title: 'Présentation', route: '/parking'},
            {title: 'Réserver', route: '/parking'},
    ] },
    { title: 'Privatisation', route: '/privatisation', subLinks: [
        {title: 'Présentation', route: '/privatisation'},
        {title: 'Réserver', route: '/privatisation'}
    ] },
    { title: 'Actualités', route: '/actualites'},
    { title: 'Infos pratiques', route: '/infos', subLinks: [
        {title: 'Comment venir ?', route: '/infos'},
        {title: 'FAQ', route: '/infos-faq'}
    ] },
    { title: 'Contact', route: '/contact'},
    { title: <img src={'/assets/user.svg'} alt={'connexion'}/>, route: '/login'},
    { title: <img src={'/assets/cart.svg'} alt={'panier'}/> , route: '/reservation'}
]
const icon = '/assets/burger.svg';

const Header = () => (
    <div>
        <Burger>
            <img src={icon} alt={'menu'} />
            <Container>
                {HEADER_DROPDOWNS.map((headerItem, index)=><HeaderItem key={`header_${index}`} item={headerItem} />)}
            </Container>
        </Burger>
    </div>
);

export default Header;
