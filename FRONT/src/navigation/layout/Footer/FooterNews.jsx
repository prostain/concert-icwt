import React, { useState } from "react";
import TextBox from "../../../components/shared/Textbox";
import Button from "../../../components/shared/Button";
import {
  NewsLetterContainer,
  NewsLetter,
} from "../../../style/navigation/layout/Footer/FooterNews.style";
import { setNewsLetter } from "../../../api/wrapper";

let TextBoxValue = "toto";
let ErrorBox = "";

const FooterNews = () => {
  const [buttonStatus, setButtonStatus] = useState("disable");

  const onClick = async (val) => {
    try {
      console.log(TextBoxValue);
      let data = {
        email: TextBoxValue,
      };
      const response = await setNewsLetter(data);
      alert(response.data.message);
    } catch (err) {
      alert("Une erreur est survenue");
    }
  };

  const onInput = (val) => {
    TextBoxValue = val.target.value;
    if (TextBoxValue !== "") {
      setButtonStatus("primary");
    } else {
      setButtonStatus("disable");
    }
  };
  return (
    <NewsLetterContainer>
      <h3>NewsLetter</h3>
      <div>Recevez notre newsletter pour ne manquer aucune information.</div>
      <NewsLetter>
        <TextBox
          type={"email"}
          content={TextBoxValue}
          action={onInput}
          placeHolder="E-mail"
          boxWidth={200}
        />
        <Button text={"Valider"} action={onClick} buttonStyle={buttonStatus} />
      </NewsLetter>
    </NewsLetterContainer>
  );
};

export default FooterNews;
