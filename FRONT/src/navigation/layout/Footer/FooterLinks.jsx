import React from 'react';
import {  LinkContainer, LinkCard, LinkTag, Spacer } from '../../../style/navigation/layout/Footer/FooterLinks.style';

const FooterLinks = () => (
    <LinkContainer>
        <LinkCard>
            <LinkTag to={'/programmation'}>PROGRAMMATION</LinkTag>
            <LinkTag to={'/programmation'}>Tous les évènements</LinkTag>
            <LinkTag to={'/programmation'}>Aix-en-Provence</LinkTag>
            <LinkTag to={'/programmation'}>Bourges</LinkTag>
            <LinkTag to={'/programmation'}>Cannes</LinkTag>
            <LinkTag to={'/programmation'}>Dunkerque</LinkTag>
            <LinkTag to={'/programmation'}>Echirolles</LinkTag>
            <LinkTag to={'/programmation'}>Comment réserver ?</LinkTag>
        </LinkCard>
        <Spacer></Spacer>
        <LinkCard>
            <LinkTag to={'/restauration'}>RESTAURATION</LinkTag>
            <LinkTag to={'/restauration'}>Présentation</LinkTag>
            <LinkTag to={'/restauration'}>Réserver</LinkTag>
            <br />
            <br />

            <LinkTag to={'/parking'}>PARKING</LinkTag>
            <LinkTag to={'/parking'}>Présentation</LinkTag>
            <LinkTag to={'/parking'}>Réserver</LinkTag>
        </LinkCard>
        <Spacer></Spacer>
        <LinkCard>
            <LinkTag to={'/privatisation'}>PRIVATISATION</LinkTag>
            <LinkTag to={'/infos'}>Présentation</LinkTag>
            <LinkTag to={'/privatisation'}>Réserver</LinkTag>
            <br />
            <br />

            <LinkTag to={'/actualites'}>ACTUALITES</LinkTag>
        </LinkCard>
        <Spacer></Spacer>

        <LinkCard>
            <LinkTag to={'/privatisation'}>INFOS PRATIQUES</LinkTag>
            <LinkTag to={'/infos'}>Comment venir ?</LinkTag>
            <LinkTag to={'/infos-faq'}>FAQ</LinkTag>
            <br />
            <br />

            <LinkTag to={'/contact'}>CONTACT</LinkTag>

        </LinkCard>
        <Spacer></Spacer>

        <LinkCard>
            <LinkTag to={'/politiques'}>POLITIQUES</LinkTag>
            <LinkTag to={'/politiques-cgu'}>CGU</LinkTag>
            <LinkTag to={'/politiques-mentions'}>Mentions légales</LinkTag>
        </LinkCard>
    </LinkContainer> 
);

export default FooterLinks;
