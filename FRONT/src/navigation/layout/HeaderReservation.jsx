import React from "react";
import {
  HeaderReservationContainer,
  Separator,
  ReservationStepItem,
} from "../../style/navigation/layout/HeaderReservation.style";

const HeaderReservation = (props) => {
  const { step } = props;
  const steps = [
    {
      id: 0,
      name: "Reservation",
    },
    {
      id: 1,
      name: "Panier d'achat",
    },
    {
      id: 2,
      name: "Coordonnées",
    },
    {
      id: 3,
      name: "Coordonnées",
    },
    {
      id: 4,
      name: "Paiement",
    },
    {
      id: 5,
      name: "Confirmation",
    },
  ];

  return (
    <HeaderReservationContainer>
      <ReservationStepItem isActive={step === steps[0].id}>
        1. {steps[0].name}
      </ReservationStepItem>
      <Separator>{" > "}</Separator>
      <ReservationStepItem isActive={step === steps[1].id}>
        2. {steps[1].name}
      </ReservationStepItem>
      <Separator>{" > "}</Separator>
      <ReservationStepItem isActive={step === steps[2].id || step === steps[3].id}>
        3. {steps[2].name}
      </ReservationStepItem>
      <Separator>{" > "}</Separator>
      <ReservationStepItem isActive={step === steps[4].id}>
        4. {steps[4].name}
      </ReservationStepItem>
      <Separator>{" > "}</Separator>
      <ReservationStepItem isActive={step === steps[5].id}>
        5. {steps[5].name}
      </ReservationStepItem>
    </HeaderReservationContainer>
  );
};

export default HeaderReservation;
