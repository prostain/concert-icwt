import React from 'react';
import { useHistory } from "react-router-dom";
import Button from '../../components/shared/Button';

let searchTextValue = "";

const SearchBox = () => {
  const history = useHistory();
  const BarStyling = {width:"30rem",background:"#F2F1F9", borderRadius:"15px", padding:"0.5rem", margin:"0 auto", display:"flex", marginBottom: "15px"};

  const onClick = () => {
    history.push("/search?searchText=" + searchTextValue);
    window.location.reload();
  }

  const onInput = (val) => {
    searchTextValue = val.target.value;
  };

    return (
      <div>
         <input
          type='search'
          style={BarStyling}
          content={searchTextValue}
          placeholder="Rechercher un concert..."
          onChange={onInput}
          />
          <Button text={"Rechercher"} action={onClick} buttonStyle={"search"} />
      </div>
    );
  }

  export default SearchBox