import React, {useState} from 'react';
import { useHistory } from "react-router-dom";
import Button from '../../components/shared/Button';
import { ButtonGroup, ToggleButton } from 'react-bootstrap';
import DatePicker from "react-datepicker";
import 'react-datepicker/dist/react-datepicker.css'
import { format } from 'date-fns';

let location = "";
let musicCategoryId = "";

const SortBox = () => {
  const history = useHistory();
  const [dateStart, setStartDate] = useState(null);
  const [dateEnd, setEndDate] = useState(null);
  const [locationState, setLocation] = useState('');
  const [musicCategoryIdState, setMusicCategoryId] = useState('');

  const locations = [
    { name: 'TOUS', value: 'TOUS'},
    { name: 'AIX-EN-PROVENCE', value: 'AIX-EN-PROVENCE'},
    { name: 'BOURGES', value: 'BOURGES'},
    { name: 'CANNES', value: 'CANNES'},
    { name: 'DUNKERQUE', value: 'DUNKERQUE'},
    { name: 'ECHIROLLES', value: 'ECHIROLLES'}
  ];

  const musicCategories = [
    { name: 'TOUTES', value: '1'},
    { name: 'Pop', value: '2'},
    { name: 'Rock', value: '3'},
    { name: 'Electro', value: '4'},
    { name: 'Rap', value: '5'},
    { name: 'Soul', value: '6'},
    { name: 'Classique', value: '7'},
    { name: 'Raggae', value: '8'},
    { name: 'World', value: '9'},
  ];

  const onClick = () => {
    let dateStartFormat = '';
    let dateEndFormat = '';

    if(dateStart){
      dateStartFormat = format(dateStart, 'dd/MM/yyyy');
    }
    if(dateEnd){
      dateEndFormat = format(dateEnd, 'dd/MM/yyyy');
    }

    history.push("/programmation?location=" + location + "&musicCategoryId=" + musicCategoryId + "&dateStart=" + dateStartFormat + "&dateEnd=" + dateEndFormat);
    window.location.reload();
  }
  
  const onInputLocation = (val) => {
    location = val.target.value;
    setLocation(location)
  };
  
  const onInputMusic = (val) => {
    musicCategoryId = val.target.value;
    setMusicCategoryId(musicCategoryId)
  };
  if(dateStart){
    console.log(dateStart.getDate());
  }
  return (
      <div>
        <div class="tri">
          <label>Lieux :   </label>
          <ButtonGroup toggle>
            {locations.map((loc, index) => (
              <ToggleButton
                key={index}
                type="radio"
                variant="contained"
                value={loc.value}
                checked={locationState === loc.value}
                onChange={onInputLocation}
              >
                {loc.name}
              </ToggleButton>
            ))}
          </ButtonGroup>
        </div>
        <div class="tri">
          <label>Catégorie de musique :   </label>
          <ButtonGroup toggle>
            {musicCategories.map((mc, index) => (
              <ToggleButton
                key={index}
                type="radio"
                variant="contained"
                value={mc.value}
                checked={musicCategoryIdState === mc.value}
                onChange={onInputMusic}
              >
                {mc.name}
              </ToggleButton>
            ))}
          </ButtonGroup>
        </div>
        <div class="tri">
          <label>Dates : Du   </label>
          <DatePicker
            closeOnScroll={true}
            selected={dateStart}
            dateFormat='dd/MM/yyyy'
            minDate={new Date()}
            showMonthDropdown
            showYearDropdown
            isClearable
            dropdownMode="select"
            placeholderText="Select date start"
            onChange={dateStart => setStartDate(dateStart)}
          />
          <label>   au    </label>
          <DatePicker
            closeOnScroll={true}
            selected={dateEnd}
            dateFormat='dd/MM/yyyy'
            minDate={new Date()}
            isClearable
            showMonthDropdown
            showYearDropdown
            dropdownMode="select"
            placeholderText="Select date end"
            onChange={dateEnd => setEndDate(dateEnd)}
          />
        </div>
        <Button text={"Valider Tri"} action={onClick} buttonStyle={"search"} />
      </div>
    );
  }

  export default SortBox;