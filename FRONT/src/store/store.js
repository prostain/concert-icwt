import { configureStore } from '@reduxjs/toolkit';
import postReducer from './counter/postSlice';
import userReducer from './user';

export default configureStore({
  reducer: {
    post: postReducer,
    user: userReducer
  },
});
