import React, { useState } from "react";

import { LoginContainer, ButtonListRegister } from "../style/views/Login.style";
import { useHistory } from "react-router-dom";
import Textbox from "../components/shared/Textbox";
import Button from "../components/shared/Button";
import { ButtonGroup, ToggleButton } from 'react-bootstrap';
import DatePicker from "react-datepicker";
import 'react-datepicker/dist/react-datepicker.css';
import { format } from 'date-fns';
import { login, register } from "../api/wrapper";
import { connect } from "react-redux";


let textboxConfirmEmail = "";
let textboxConfirmPassword = "";

function Register({ setToken }) {
  let textboxType = "text";
  let passwordboxType = "password";
  const [buttonStatus, setButtonStatus] = useState("disable");
  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState(null);
  const [civility, setCivility] = useState('');
  const [lastname, setLastname] = useState(null);
  const [firstname, setFirstname] = useState(null);
  const [address, setAddress] = useState(null);
  const [accomodationType, setAccomodationType] = useState("");
  const [additionalAddress, setAdditionalAddress] = useState("");
  const [codePostal, setCodePostal] = useState(null);
  const [city, setCity] = useState(null);
  const [country, setCountry] = useState(null);
  const [phone, setPhone] = useState(null);
  const [dateBirthday, setDateBirthday] = useState(null);
  const history = useHistory();

  const onClick = async () => {
    if (buttonStatus === "disable") {
      return;
    }
    try {
      const data = saveUser();
      await register(data);
      const {
        data: { token },
      } = await login(email, password);
      setToken(token);
      history.replace('/');
    } catch (e) {
      alert(e);
    }
  }

  function onClickCancel () {
    history.push('/panier');
  }

  const onInputEmail = (val) => {
    setEmail(val.target.value);
    if (email !== null && textboxConfirmEmail !== "" && password !== null && textboxConfirmPassword !== "" && civility !== null &&
    lastname !== null && firstname !== null && address !== null && city !== null && country !== null && phone !== null) {
      setButtonStatus("primary");
    } else {
      setButtonStatus("disable");
    }
  };
  const onInputConfirmMail = (val) => {
    textboxConfirmEmail = val.target.value;
    if(email !== textboxConfirmEmail){
      document.querySelector('.confirm-mail').innerHTML = "L'email n'est pas identique au précédent.";
      setButtonStatus("disable");
    } else {
      document.querySelector('.confirm-mail').innerHTML = "";
      setButtonStatus("primary");
    }

    if (email !== null && textboxConfirmEmail !== "" && password !== null && textboxConfirmPassword !== "" && civility !== null &&
    lastname !== null && firstname !== null && address !== null && city !== null && country !== null && phone !== null) {
      setButtonStatus("primary");
    } else {
      setButtonStatus("disable");
    }
  };
  const onInputPassword = (val) => {
    setPassword(val.target.value);
    if (email !== null && textboxConfirmEmail !== "" && password !== null && textboxConfirmPassword !== "" && civility !== null &&
    lastname !== null && firstname !== null && address !== null && city !== null && country !== null && phone !== null) {
      setButtonStatus("primary");
    } else {
      setButtonStatus("disable");
    }
  };
  const onInputConfirmPassword = (val) => {
    textboxConfirmPassword = val.target.value;
    if(password !== textboxConfirmPassword){
      document.querySelector('.confirm-password').innerHTML = "Le mot de passe n'est pas identique au précédent.";
      setButtonStatus("disable");
    } else {
      document.querySelector('.confirm-password').innerHTML = "";
    }
    if (email !== null && textboxConfirmEmail !== "" && password !== null && textboxConfirmPassword !== "" && civility !== null &&
    lastname !== null && firstname !== null && address !== null && city !== null && country !== null && phone !== null){
      setButtonStatus("disable");
    } else {
      setButtonStatus("primary");
    }
  };
  const onInputCivility = (val) => {
    setCivility(val.target.value);
    if (email !== null && textboxConfirmEmail !== "" && password !== null && textboxConfirmPassword !== "" && civility !== null &&
    lastname !== null && firstname !== null && address !== null && city !== null && country !== null && phone !== null) {
      setButtonStatus("primary");
    } else {
      setButtonStatus("disable");
    }
  };
  const onInputFirstname = (val) => {
    setFirstname(val.target.value);
    if (email !== null && textboxConfirmEmail !== "" && password !== null && textboxConfirmPassword !== "" && civility !== null &&
    lastname !== null && firstname !== null && address !== null && city !== null && country !== null && phone !== null) {
      setButtonStatus("primary");
    } else {
      setButtonStatus("disable");
    }
  };
  const onInputLastname = (val) => {
    setLastname(val.target.value);
    if (email !== null && textboxConfirmEmail !== "" && password !== null && textboxConfirmPassword !== "" && civility !== null &&
    lastname !== null && firstname !== null && address !== null && city !== null && country !== null && phone !== null){
      setButtonStatus("primary");
    } else {
      setButtonStatus("disable");
    }
  };
  const onInputAddress = (val) => {
    setAddress(val.target.value);
    if (email !== null && textboxConfirmEmail !== "" && password !== null && textboxConfirmPassword !== "" && civility !== null &&
    lastname !== null && firstname !== null && address !== null && city !== null && country !== null && phone !== null) {
      setButtonStatus("primary");
    } else {
      setButtonStatus("disable");
    }
  };
  const onInputAccomodation = (val) => {
    setAccomodationType(val.target.value);
    if (email !== null && textboxConfirmEmail !== "" && password !== null && textboxConfirmPassword !== "" && civility !== null &&
    lastname !== null && firstname !== null && address !== null && city !== null && country !== null && phone !== null) {
      setButtonStatus("primary");
    } else {
      setButtonStatus("disable");
    }
  };
  const onInputAdditionalAddress = (val) => {
    setAdditionalAddress(val.target.value);
    if (email !== null && textboxConfirmEmail !== "" && password !== null && textboxConfirmPassword !== "" && civility !== null &&
    lastname !== null && firstname !== null && address !== null && city !== null && country !== null && phone !== null){
      setButtonStatus("primary");
    } else {
      setButtonStatus("disable");
    }
  };
  const onInputCodePostal = (val) => {
    setCodePostal(val.target.value);

    if (email !== null && textboxConfirmEmail !== "" && password !== null && textboxConfirmPassword !== "" && civility !== null &&
    lastname !== null && firstname !== null && address !== null && city !== null && country !== null && phone !== null){
      setButtonStatus("primary");
    } else {
      setButtonStatus("disable");
    }
  };
  const onInputCity = (val) => {
    setCity(val.target.value);
    if (email !== null && textboxConfirmEmail !== "" && password !== null && textboxConfirmPassword !== "" && civility !== null &&
    lastname !== null && firstname !== null && address !== null && city !== null && country !== null && phone !== null) {
      setButtonStatus("primary");
    } else {
      setButtonStatus("disable");
    }
  };
  const onInputCountry = (val) => {
    setCountry(val.target.value);
    if (email !== null && textboxConfirmEmail !== "" && password !== null && textboxConfirmPassword !== "" && civility !== null &&
    lastname !== null && firstname !== null && address !== null && city !== null && country !== null && phone !== null) {
      setButtonStatus("primary");
    } else {
      setButtonStatus("disable");
    }
  };
  const onInputPhone = (val) => {
    setPhone(val.target.value);
    if (email !== null && textboxConfirmEmail !== "" && password !== null && textboxConfirmPassword !== "" && civility !== null &&
    lastname !== null && firstname !== null && address !== null && city !== null && country !== null && phone !== null) {
      setButtonStatus("primary");
    } else {
      setButtonStatus("disable");
    }
  };

  function onClickClear () {
    window.location.reload();
  }

  function saveUser () {
    const user = {
      email: email,
      password: password,
      civility: civility,
      lastname: lastname,
      firstname: firstname,
      address: address,
      accomodationType: accomodationType,
      additionalAddress: additionalAddress,
      codePostal: codePostal,
      city: city,
      country: country,
      phone: phone,
      dateBirthday: format(dateBirthday, 'dd/MM/yyyy'),
    };
    return JSON.stringify(user);
}

  const civilities = [
    { name: 'Madame', value: 'Madame'},
    { name: 'Monsieur', value: 'Monsieur'}
  ];

  return (
    <>
      <h1>CRÉATION DE VOTRE COMPTE</h1>
      <LoginContainer>
        <Textbox
          title={"Adresse e-mail *"}
          type={textboxType}
          action={onInputEmail}
          placeHolder={"Email..."}
          boxWidth={500}
        />
        <Textbox
          title={"Confirmation e-mail *"}
          type={textboxType}
          action={onInputConfirmMail}
          placeHolder={"Confirmer email..."}
          boxWidth={500}
        />
        <div className="confirm-mail" style={{textAlign: 'center', color: 'red'}}>L'email n'est pas identique au précédent.</div>
        <Textbox
          title={"Mot de passe *"}
          type={passwordboxType}
          action={onInputPassword}
          placeHolder={"Votre mot de passe..."}
          boxWidth={500}
        />
        <Textbox
          title={"Confirmation mot de passe *"}
          type={passwordboxType}
          action={onInputConfirmPassword}
          placeHolder={"Confirmer mot de passe..."}
          boxWidth={500}
        />
        <div className="confirm-password" style={{textAlign: 'center', color: 'red'}}>Le mot de passe n'est pas identique au précédent.</div>
        <p style={{textAlign: 'center', color: 'black'}}>Civilité *</p>
        <ButtonGroup toggle style={{color: 'black', marginBottom: '35px'}}>
          {civilities.map((civ, index) => (
            <ToggleButton
              key={index}
              type="radio"
              variant="contained"
              value={civ.value}
              checked={civility === civ.value}
              onChange={onInputCivility}
            >
              {civ.name}
            </ToggleButton>
          ))}
        </ButtonGroup>
        <Textbox
          title={"Nom *"}
          type={textboxType}
          action={onInputLastname}
          placeHolder={"Votre nom..."}
          boxWidth={500}
        />
        <Textbox
          title={"Prénom *"}
          type={textboxType}
          action={onInputFirstname}
          placeHolder={"Votre prénom..."}
          boxWidth={500}
        />
        <Textbox
          title={"N° et libellé de la voie *"}
          type={textboxType}
          action={onInputAddress}
          boxWidth={500}
        />
        <Textbox
          title={"Immeuble, Bâtiment, Résidence"}
          type={textboxType}
          action={onInputAccomodation}
          boxWidth={500}
        />
        <Textbox
          title={"Lieu-dit, boîte postale, etc"}
          type={textboxType}
          action={onInputAdditionalAddress}
          boxWidth={500}
        />
        <Textbox
          title={"Code postal *"}
          type={textboxType}
          action={onInputCodePostal}
          boxWidth={500}
        />
        <Textbox
          title={"Ville *"}
          type={textboxType}
          action={onInputCity}
          boxWidth={500}
        />
        <Textbox
          title={"Pays *"}
          type={textboxType}
          action={onInputCountry}
          boxWidth={500}
        />
        <Textbox
          title={"Téléphone *"}
          type={textboxType}
          action={onInputPhone}
          boxWidth={500}
        />
        <p style={{textAlign: 'center', color: 'black'}}>Date de naissance *</p>
          <DatePicker
            closeOnScroll={true}
            selected={dateBirthday}
            dateFormat='dd/MM/yyyy'
            showMonthDropdown
            showYearDropdown
            isClearable
            dropdownMode="select"
            placeholderText="Votre date de naissance..."
            onChange={dateBirthday => setDateBirthday(dateBirthday)}
          />
        <ButtonListRegister>
          <Button
            text={"Annuler"}
            action={onClickCancel}
            buttonStyle={"secondary"}
          />
          <Button
            text={"Effacer"}
            action={onClickClear}
            buttonStyle={"primary"}
          />
          <Button
            text={"Créer mon compte"}
            action={onClick}
            buttonStyle={buttonStatus}
          />
        </ButtonListRegister>
      </LoginContainer>
    </>
  );
}

const mapStateToProps = (state) => ({
  stateToken: state.user.token,
});

const mapDispatchToProps = (dispatch) => {
  return {
    setToken: (token) => dispatch({ type: "user/setToken", payload: token }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);

