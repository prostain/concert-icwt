import React, { useEffect, useState } from "react";
import Button from "../components/shared/Button";
import { Carousel } from "../style/views/Home.style";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { getConcert, getConcerts } from "../api/wrapper";
import ConcertListItem from "../components/concert/ConcertListItem";
import { ConcertContainer } from "../style/views/Concert.style";
import { useHistory } from "react-router-dom";
import { color } from "../style/components/variable/color.style";
import { getCategories } from "../utils/concert";

function Concert(props) {
  const [sliderItems, setSliderItems] = useState([]);
  const [event, setEvent] = useState({ });
  const history = useHistory();

  const id = props.location.pathname.replace("/concert/", "");
  if (!/\d+/.test(id)) {
    history.replace("/concerts");
  }

  function onClick () {
    history.push('/reservation/'+event.id)
  }

  useEffect(() => {
    async function fetchData() {
      const { data } = await getConcerts();
      const { data: concert } = await getConcert(id);
      if (data) {
        setSliderItems(data["hydra:member"]);
      }
      if (concert) {
        setEvent(concert);
      }
      return data;
    }

    fetchData();
  }, []);

  let priceMin;
  let catString;
  if (event.priceCategories) {
    priceMin = getCategories(event).reverse()[0].priceMin;
    catString = new Array(event.priceCategories).fill('CAT').map((cat, i) => cat+(i+1)).join(', ');
  }
  return (
    <>
      <ConcertContainer>
        <div
          style={{
            backgroundColor: color.grey4,
            padding: "20px",
            display: "flex",
            justifyContent: "space-around",
            alignItems: "center",
            marginBottom: "15px"
          }}
        >
          <div style={{ paddingRight: "5px", width: "50%" }}>
            <img
              style={{ borderRadius: "0.25rem", width: "100%" }}
              src={event.image?.url}
              alt="concert"
            />
          </div>
          <div>
            <p>Artiste : {event.artist?.name}</p>
            <p>Tournée : {event.tourName}</p>
            <p>Se déroule le {event.date} à {event.hour}</p>
            <p>{event.location}</p>
            <p>Catégorie de musique : {event.artist?.musicCategory.name}</p>
          </div>
        </div>
        <div>
          <table class='concertTable'>
            <thead>
              <tr>
                <th>Date</th>
                <th>Lieu</th>
                <th>Heure</th>
                <th>Ouverture</th>
                <th>Catégorie de tarifs</th>
                <th>Tarifs</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{event.date}</td>
                <td>{event.location}</td>
                <td>{event.hour}</td>
                <td>{event.hourOpening}</td>
                <td>{catString}</td>
                <td>De {priceMin}€ à {event.price && event.price.toFixed(2)}€</td>
                <td>
                  <Button
                    text={"Réserver"}
                    action={onClick}
                    buttonStyle={"primary"}
                  />
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <div>
          <h2>Présentation de l'artiste / groupe</h2>
          <p style={{width: "90%", margin: "0 auto"}}>
            <span>{event.artist?.description}</span>
          </p>
        </div>
        <h2>A ne pas manquer</h2>
        <Carousel arrows={true} dots={true} slidesToScroll={4} slidesToShow={4}>
          {sliderItems.length > 0 ? (
            sliderItems.map((item, index) => (
              <ConcertListItem
                key={item.title + index}
                item={item}
                horizontal={false}
                displayPriceRange={true}
              />
            ))
          ) : (
            <p>Chargement ...</p>
          )}
        </Carousel>
      </ConcertContainer>
    </>
  );
}

export default Concert;
