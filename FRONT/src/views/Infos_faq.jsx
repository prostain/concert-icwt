import React from 'react';
import { Link } from "react-router-dom";
import CollapsibleSection from "../components/shared/CollapsibleSection";
import { Container, Title } from "../style/views/Infos_faq.style";

const sections = [
    'Informations générales',
    'Billeterie & Réservations',
    'Accessibilité et Réservations PMR / PSH',
    'Accès - Parking',
    'Restauration',
    'Privatisation - Location de salles - Organisation d\'évènements',
]

function InfosFaq() {
    return (
      <>
          <Title>INFOS PRATIQUES - FAQ</Title>
          <Container>
          <CollapsibleSection title={sections[0]}>
              <h2>Comment puis-je réserver mes places ?</h2>
              <p>Vous pouvez réserver vos places sur le site de Symfony Concert. Choisissez le concert qui vous intéresse en allant sur la page <Link to={'/programmation'}>Tous les évènements</Link></p>
              <h2>Comment vous contacter ?</h2>
              <p>Vous pouvez nous contacter via le<Link to={'/Contact'}>Formulaire de contact</Link></p>
          </CollapsibleSection>
          <CollapsibleSection title={sections[1]}>
              <h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aliquid architecto atque cumque dolore dolorem impedit modi, natus numquam pariatur reiciendis rem tempora ut. Ducimus exercitationem quisquam quo tempora voluptatum!</h2>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur minus, neque. Delectus eos libero mollitia necessitatibus nihil numquam odio possimus quod sequi soluta! Dolorem, ratione, veniam! Ad dolor modi nesciunt.</p>
          </CollapsibleSection>
          <CollapsibleSection title={sections[2]}>
              <h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam architecto assumenda autem, corporis dignissimos dolor eligendi facere, fugit illum inventore laudantium nemo praesentium rem sapiente sint sit suscipit veniam, voluptas.</h2>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusantium aspernatur aut consequatur consequuntur distinctio doloremque, eius, eos et facilis hic libero maiores nam possimus quam repellendus reprehenderit veniam, vitae!</p>
          </CollapsibleSection>
          <CollapsibleSection title={sections[3]}>
              <h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam consectetur cum hic, inventore molestiae placeat quisquam ratione. Exercitationem, ipsum minima mollitia officia quibusdam tenetur. Ipsa ipsam magni mollitia voluptas?</h2>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque culpa laborum qui sequi sunt. Dicta error labore laborum nobis obcaecati? Adipisci excepturi porro unde. Ab facere itaque quae tempore veritatis?</p>
          </CollapsibleSection>
          <CollapsibleSection title={sections[4]}>
              <h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque odio repellat sit veniam! Aperiam architecto at, dolores nihil numquam sequi. Cupiditate dolor eveniet, in odit porro qui tenetur vitae! Ipsum.</h2>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis esse est in minima numquam praesentium reiciendis rerum. Aut autem iure laudantium magnam, molestiae soluta voluptate? Id quas soluta unde voluptatem!</p>
          </CollapsibleSection>
          <CollapsibleSection title={sections[5]}>
              <h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium assumenda at commodi, deleniti dicta dolorem eaque earum esse hic illo magnam maxime minus natus perferendis quidem repellendus repudiandae totam voluptatum!</h2>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet assumenda aut dolores doloribus fugiat ipsum labore natus obcaecati possimus quas, quod repudiandae saepe sit unde veniam vero voluptatibus. Obcaecati, saepe.</p>
          </CollapsibleSection>
          </Container>
      </>
  );
}

export default InfosFaq;
