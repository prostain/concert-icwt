import React, { useEffect, useState }from 'react';
import { getConcertsBySearch } from '../api/wrapper';
import SearchConcertList from '../components/concert/SearchConcertList';
import DotLoader from '../components/shared/DotLoader';


function Search(props) {
  let regex = /.*=(.*)/;
  let searchText = props.location.search;
  let result = regex.exec(searchText)
  const [data, setData] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const divContainerStyling = {textAlign:"center"};

  useEffect(() => {
    async function fetchData() {
      setIsLoading(true)
      try {
        const { data } = await getConcertsBySearch(result[1]);
        setData(data);
      } catch (e) {
        alert(e.message);
      }
      setIsLoading(false)
    }

    fetchData();
  }, []);

  return (
    <>
        <h1>RÉSULTATS DE RECHERCHE</h1>
        <div style={divContainerStyling}>
          {data.length > 0 ? <SearchConcertList data={data} /> : isLoading ? <DotLoader /> : <p>Aucun résultat</p>}
        </div>
    </>
  );
}

export default Search;
