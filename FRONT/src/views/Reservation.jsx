import React from "react";
import { CartProvider } from "../components/reservation/CartProvider";
import ReservationSteps from "../components/reservation/ReservationSteps";

function Reservation(props) {
  return (
      <CartProvider>
         <ReservationSteps {...props}/>
      </CartProvider>
  );
}

export default Reservation;
