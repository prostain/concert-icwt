import React, { useState, useEffect } from 'react';
import 'react-datepicker/dist/react-datepicker.css'
import { getConcertsSorted } from '../api/wrapper';
import SortBox from '../navigation/layout/SortBox';
import DotLoader from '../components/shared/DotLoader';
import SortConcertList from '../components/concert/SortConcertList';
import ConcertList from "../components/concert/ConcertList";


function Programmation(props) {
  const [data, setData] = useState({});
  const divContainerStyling = {textAlign:"center"};
  const [isLoading, setIsLoading] = useState(false);

  let location = '';
  let musicCategoryId = '';
  let dateStart = '';
  let dateEnd = '';

  function useQuery() {
    return new URLSearchParams(props.location.search);
  }

  let query = useQuery();
  location = query.get("location");
  musicCategoryId = query.get("musicCategoryId");
  dateStart = query.get("dateStart");
  dateEnd = query.get("dateEnd");

  useEffect(() => {
    async function fetchData() {
      setIsLoading(true)
      try {
        const { data } = await getConcertsSorted(location, musicCategoryId, dateStart, dateEnd);
        setData(data);
      } catch (e) {
      }
      setIsLoading(false)
    }

    fetchData();
  }, []);

  return (
    <>
        <h1>PROGRAMMATION</h1>
        <SortBox></SortBox>
        <div style={divContainerStyling}>
          {data.length > 0 ? <SortConcertList data={data} /> : isLoading ? <DotLoader /> : <ConcertList displayPriceRange={true} />}
        </div>
    </>
  );
}

export default Programmation;
