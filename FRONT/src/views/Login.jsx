import React, { useState } from "react";

import { LoginContainer, ButtonListRegister } from "../style/views/Login.style";
import { useHistory } from "react-router-dom";
import Textbox from "../components/shared/Textbox";
import Button from "../components/shared/Button";
import { login } from "../api/wrapper";
import { connect } from "react-redux";

let textboxUsername = "";
let textboxPassword = "";

function Login({ setToken }) {
  let textboxType = "text";
  let passwordboxType = "password";
  const [buttonStatus, setButtonStatus] = useState("disable");
  const history = useHistory();
  const onClick = async () => {
    if (buttonStatus === "disable") {
      return;
    }
    try {
      const {
        data: { token },
      } = await login(textboxUsername, textboxPassword);
      if (token) {
        setToken(token);
        history.push({ pathname: "/compte", state: { token } });
      }
    } catch (e) {
      console.log(e);
    }
  };

  function onClickRegister () {
    history.push('/register');
  }

  const onInputUsername = (val) => {
    textboxUsername = val.target.value;
    if (textboxUsername !== "" && textboxPassword !== "") {
      setButtonStatus("primary");
    } else {
      setButtonStatus("disable");
    }
  };
  const onInputPassword = (val) => {
    textboxPassword = val.target.value;
    if (textboxUsername !== "" && textboxPassword !== "") {
      setButtonStatus("primary");
    } else {
      setButtonStatus("disable");
    }
  };

  return (
    <>
      <LoginContainer>
        <Textbox
          title={"Nom d'utilisateur"}
          type={textboxType}
          action={onInputUsername}
          placeHolder={"Votre nom d'utilisateur"}
          boxWidth={200}
        />
        <Textbox
          title={"Mot de passe"}
          type={passwordboxType}
          action={onInputPassword}
          placeHolder={"Votre mot de passe"}
          boxWidth={200}
        />
        <ButtonListRegister>
          <Button
            text={"Se connecter"}
            action={onClick}
            buttonStyle={buttonStatus}
          />
          <Button
            text={"Mot de passe oublié"}
            action={onClick}
            buttonStyle={"secondary"}
          />
          <Button
            text={"Créer un compte"}
            action={onClickRegister}
            buttonStyle={"primary"}
          />
        </ButtonListRegister>
      </LoginContainer>
    </>
  );
}

const mapStateToProps = (state) => ({
  stateToken: state.user.token,
});

const mapDispatchToProps = (dispatch) => {
  return {
    setToken: (token) => dispatch({ type: "user/setToken", payload: token }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
