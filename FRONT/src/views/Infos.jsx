import React, { useEffect, useState } from 'react';
import Map from "../components/shared/Map";
import { getRooms } from "../api/wrapper";

function Infos() {
    const [rooms, setRooms] = useState([])

    async function fetch() {
        const {data} = await getRooms();
        setRooms(data['hydra:member']);
        console.log(data['hydra:member'])
    }

    useEffect(() => {
        fetch();
    }, []);

    return (
      <>
          <h1>Infos pratiques  - comment venir ?</h1>
          <h2>Ci-dessous les adresses de nos salles de concert : </h2>
          {rooms.length > 0 && rooms.map((r, index)=>{
              return (
                  <div>
                      <div style={{margin: '30px 30px', display: 'flex', flexDirection: index%2 === 0 ? 'row' : 'row-reverse', alignItems: 'center'}}>
                          <Map lng={r.longitude} lat={r.latitude} />
                          <div style={{padding: '20px'}}>
                              <p>{r.name}</p>
                              <p>{r.codePostal}, {r.city}</p>
                          </div>
                      </div>
                      <div style={{width: '100%', height: '1px', backgroundColor: 'black'}}/>
                  </div>
              )
          })}
      </>
  );
}

export default Infos;
