import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";

import Button from "../components/shared/Button";
import {
  ImagePrivatisation,
  PresentationContainer,
  Privatiser,
  Carousel,
  HomeContainer,
} from "../style/views/Home.style";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { getHomeData } from "../api/wrapper";
import HomeConcertList from "../components/concert/HomeConcertList";
import HomeNewsList from "../components/news/HomeNewsList";

function Home() {
  const history = useHistory();
  const [data, setData] = useState({});

  async function fetch() {
    const { data: response } = await getHomeData();
    setData(response);
  }

  useEffect(() => {
    fetch();
  }, []);

  const settings = {
    dots: true,
    infinite: true,
    slidesToShow: 1,
    arrows: true,
    style: { position: "relative" },
  };

  const onClick = () => history.push("/privatisation");

  return (
    <>
      <Carousel {...settings}>
        {data.lastGroupImages ? (
          data.lastGroupImages.map((img, index) => (
            <img
              src={img.url}
              alt={"image carousel " + index}
              className="carousel-item"
            />
          ))
        ) : (
          <p>Chargement ...</p>
        )}
      </Carousel>
      <HomeContainer>
        <HomeConcertList data={data.lastConcerts} />
        <HomeNewsList data={data.lastArticles} />
        <div>
          <PresentationContainer>
            <span>
              <ImagePrivatisation
                src="https://e7.pngegg.com/pngimages/1013/770/png-clipart-eyedyllic-music-record-label-concert-logo-round-eyes-text-logo.png"
                alt="Privatisation"
              />
            </span>
            <span>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam
                vestibulum, metus sit amet iaculis dapibus, dolor arcu facilisis
                mi, sed ornare a rcu ante eu libero. Maecenas euismod urna at mi
                semper rutrum. Vivamus in auctor felis, vitae blandit eros.
                Proin viverra sed odio nec pretium. Nulla auctor porta est,
                vitae interdum augue cursus in. Aliquam semper libero vel purus
                porta sollicitudin. Vestibulum ante ipsum primis in faucibus
                orci l uctus et ultrices posuere cubilia curae; Quisque at leo
                ultrices, malesuada lacus vel, auctor dolor. Phasellus aliquet
                quam accumsan felis accumsan i mperdiet. Phasellus auctor semper
                nulla, ac condimentum augue venenatis sed. In blandit commodo
                justo, lacinia porta ligula ullamcorper in. Ut rutrum enim quis
                placerat tempus. Vestibulum ut dapibus neque, at faucibus orci.
                Morbi semper porttitor vulputate. Nam varius porta tincidunt.
                Pellentesque nec pulvinar justo.
              </p>
            </span>
          </PresentationContainer>

          <Privatiser>
            <Button
              text={"Privatiser"}
              action={onClick}
              buttonStyle={"primary"}
            />
          </Privatiser>
        </div>
      </HomeContainer>
    </>
  );
}

export default Home;
