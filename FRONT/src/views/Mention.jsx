import React from 'react';
import { Container, Title } from "../style/views/Infos_faq.style";

function Mention() {
    return (
      <>
          <Title>POLITIQUES - MENTIONS LÉGALES</Title>
          <Container>
            <p>Bacon ipsum dolor amet frankfurter andouille doner cow fatback tongue porchetta prosciutto tail sausage hamburger. Shankle doner leberkas ball tip salami meatloaf. Pastrami frankfurter hamburger strip steak burgdoggen flank beef ribs buffalo pancetta, shankle andouille tail rump. Burgdoggen frankfurter spare ribs leberkas pork belly capicola. Burgdoggen pancetta fatback strip steak venison. Bresaola venison jowl flank, pig ribeye boudin swine. Chislic tail jerky picanha.</p>

            <p>Biltong swine tail ribeye shankle. Hamburger jerky fatback cupim spare ribs porchetta salami biltong beef pork drumstick ham hock cow sausage shankle. Hamburger bresaola cow meatball, prosciutto leberkas turducken drumstick t-bone buffalo pork belly beef pork chop pig. Tongue t-bone ham hock sirloin turkey. Prosciutto chicken ham pancetta, tri-tip pork belly fatback ball tip pork salami tail.</p>

            <p>Burgdoggen pork belly filet mignon, porchetta bacon ham hock chuck andouille tenderloin pork chop short ribs pastrami strip steak alcatra. Chicken short loin filet mignon jerky ham. Pig kielbasa beef ribs boudin. Picanha meatball tri-tip shoulder kevin landjaeger cupim t-bone boudin alcatra pastrami turducken andouille ham porchetta. Ground round chuck fatback brisket, shank chicken drumstick burgdoggen short ribs turkey cupim hamburger landjaeger strip steak pancetta.</p>

            <p>Meatball kielbasa cow, filet mignon shank ball tip spare ribs hamburger. Landjaeger t-bone cupim prosciutto porchetta pork biltong. Leberkas flank pork belly biltong swine ball tip. Ground round pork chop pancetta fatback ball tip picanha pork loin shankle short loin pork belly andouille.</p>

            <p>Prosciutto chislic tail, turducken pork loin tongue pork belly ribeye buffalo cupim. Cupim shoulder salami, doner drumstick beef ribs ground round tenderloin meatloaf. Pig frankfurter porchetta biltong, leberkas sausage ribeye ball tip short ribs shankle. Pork chop kevin sirloin frankfurter fatback short ribs ground round beef ribs. Tenderloin buffalo tri-tip pork loin.</p>
          </Container>
      </>
  );
}

export default Mention;
