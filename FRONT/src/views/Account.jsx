import React, { useEffect } from "react";
import { getArticles } from "../api/wrapper";
import jwt_decode from "jwt-decode";
import AccountUser from "../components/account/AccountUser";
import AccountAdmin from "../components/account/AccountAdmin";
import { connect } from "react-redux";
import { AccountContainer, AccountTitle } from "../style/views/Account.style";
import { useHistory } from "react-router";

function Account(props) {
  const { stateToken } = props;
  const CurrentUser = stateToken ? jwt_decode(stateToken) : { roles: ['anonymous'] };
  const history = useHistory();
  if(!stateToken){
    history.replace('/login')
  }
  useEffect(() => {
    if(!stateToken){
      history.replace('/login')
    } else {
    getArticles()
    }
  }, []);

  let AccountCard;
  if (CurrentUser.roles.find((role) => role === "ROLE_ADMIN")) {
    AccountCard = AccountAdmin;
  } else if (CurrentUser.roles.find((role) => role === "ROLE_USER")) {
    AccountCard = AccountUser;
  } else {
    AccountCard = 'Div';
  }
  return (
    <AccountContainer>
      <AccountTitle>Compte</AccountTitle>
      <AccountCard CurrentUser={CurrentUser}></AccountCard>
    </AccountContainer>
  );
}

const mapStateToProps = (state) => ({
  stateToken: state.user.token,
});

export default connect(mapStateToProps, {})(Account);
