import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";

import {
  HomeContainer,
} from "../style/views/Home.style";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { getArticles } from "../api/wrapper";
import HomeArticlesList from "../components/articles/HomeArticlesList";

function Home() {
  const [data, setData] = useState(null);

  async function fetch() {
    const { data: response } = await getArticles();
    setData(response);
  }

  useEffect(() => {
    fetch();
  }, []);

  const settings = {
    dots: true,
    infinite: true,
    slidesToShow: 1,
    arrows: true,
    style: { position: "relative" },
  };

  return (
    <>
      <HomeContainer>
        <HomeArticlesList data={data ?? null} />
      </HomeContainer>
    </>
  );
}

export default Home;
