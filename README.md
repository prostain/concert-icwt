# Concert-ICWT
*Une directrice de salles de concert travaillant pour la marque musicale SYMFONY, possède 5 salles de concert, réparties dans 5 villes différentes, et souhaite créer un site pour permettre de réserver des billets de concert pour chacune d’entre-elles. Les 5 salles ont les noms et le nombre
de places suivants :*

*▪ Salle Aix-en-Provence -> 150 places ;*

*▪ Salle Bourges -> 100 places ;*

*▪ Salle Cannes -> 200 places ;*

*▪ Salle Dunkerque ->100 places ;*

*▪ Salle Echirolles -> 150 places.*

*Le chef de projet vous donne la maquette du site qu’il faut réaliser et respecter. Il sera votre
interlocuteur principal !*

Projet réalisé à @aixynovcampus en Mastère 1 - Expert en développement web.

Date de rendu : 05/02/2021 avant 15h45.

Travail : groupe.

## Pré-requis

Langage PHP v7.3, framework Symfony, React, Composer, Outil de versionning Git, Serveur local (Xamp, Wamp, etc...).

## Installation

Télécharger le dossier ZIP [concert-icwt](https://gitlab.com/prostain/concert-icwt.git) sur gitlab.

## Commandes

### Symfony

*Create project*

`symfony new --version=4.4 --full <ProjectName>`



*Composer*

`composer require --dev doctrine/doctrine-fixtures-bundle`

`composer require --dev fzaninotto/faker`



*Make Controller*

`php bin/console make:controller <NameController>`



*Make Entity*

`php bin/console make:entity <EntityName>`



*Make form*

`php bin/console make:form <FormName>`



*Create user*

`php bin/console make:user`



*Create database*

`php bin/console doctrine:database:create`



*Delete database*

`php bin/console doctrine:database:drop --force`



*Migrations*

`php bin/console make:migration`

`php bin/console doctrine:migration:migrate`



*Fixtures*

`php bin/console make:fixtures`

`php bin/console doctrine:fixtures:load`



*Make Auth*

`php bin/console make:auth`



*Make CRUD*

`symfony console make:crud`


### React

*Installation des packages*

`npm i`

*Démarrer le projet : lien localhost:3000*

`npm start`

*Installer DatePicker*

`npm install react-datepicker --save`


### Faker

`composer require fzaninotto/faker`


## Construit avec

[Composer](https://getcomposer.org/) - Dependency Management for PHP

[React](https://fr.reactjs.org/) - Dependency Management for JavaScript

[Symfony](https://symfony.com/) - Framework PHP

[PHP](https://www.php.net/) - Programming language

## Contribution

Les contributions, les problèmes et les demandes de fonctionnalités sont les bienvenus.
N'hésitez pas à consulter la page des problèmes si vous souhaitez contribuer.

## Gestion de versions

Pour les versions disponibles, voir [concert-icwt](https://gitlab.com/prostain/concert-icwt.git). 


## Autheurs

**Anthony BUISSON** - *Étudiant de @aixynovcampus en Mastère 1 - Expert en développement web* - @Anthony-Buisson

**Lucas DUPERIER** - *Étudiant de @aixynovcampus en Mastère 1 - Expert en développement web* - @DuperierLucas

**Peterson ROSTAIN** - *Étudiant de @aixynovcampus en Mastère 1 - Expert en développement web* - @prostain

**Emma TARFI** - *Étudiante de @aixynovcampus en Mastère 1 - Expert en développement web* - @emma-tarfi

## License

Copyright (c) 2021 Groupe ICWT.

Ce projet est licencié [MIT](https://gitlab.com/prostain/concert-icwt.git).

## Remerciements

Aix Ynov Campus pour nous offrir leur formation ainsi que tous les intervenants pour leurs enseignements de qualité.
