<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210210235455 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE article (id INT AUTO_INCREMENT NOT NULL, category_id INT DEFAULT NULL, person_article_id INT DEFAULT NULL, image_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, description TEXT NOT NULL, date DATETIME NOT NULL, INDEX IDX_23A0E6612469DE2 (category_id), INDEX IDX_23A0E66442FE589 (person_article_id), INDEX IDX_23A0E663DA5256D (image_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE article_category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE basket (id INT AUTO_INCREMENT NOT NULL, person_id INT DEFAULT NULL, date DATETIME NOT NULL, total_amount DOUBLE PRECISION NOT NULL, INDEX IDX_2246507B217BBB47 (person_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE booking (id INT AUTO_INCREMENT NOT NULL, basket_id INT DEFAULT NULL, person_id INT DEFAULT NULL, numero VARCHAR(255) NOT NULL, number_person_restaurant INT DEFAULT NULL, number_seat_parking INT DEFAULT NULL, price DOUBLE PRECISION NOT NULL, price_insurance DOUBLE PRECISION NOT NULL, has_insurance_cancellation TINYINT(1) NOT NULL, comment VARCHAR(255) DEFAULT NULL, has_book_restaurant TINYINT(1) NOT NULL, has_book_parking TINYINT(1) NOT NULL, status INT DEFAULT 0, date DATETIME DEFAULT NULL, INDEX IDX_E00CEDDE1BE1FB52 (basket_id), INDEX IDX_E00CEDDE217BBB47 (person_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE booking_line (id INT AUTO_INCREMENT NOT NULL, concert_id INT NOT NULL, type_ticket_id INT NOT NULL, booking_id INT NOT NULL, seat INT NOT NULL, INDEX IDX_C98596B883C97B2E (concert_id), INDEX IDX_C98596B8C4C606C2 (type_ticket_id), INDEX IDX_C98596B83301C60 (booking_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE concert (id INT AUTO_INCREMENT NOT NULL, image_id INT DEFAULT NULL, artist_id INT DEFAULT NULL, room_id INT NOT NULL, tour_name VARCHAR(255) NOT NULL, date VARCHAR(255) NOT NULL, price DOUBLE PRECISION NOT NULL, location VARCHAR(255) NOT NULL, description TEXT NOT NULL, hour VARCHAR(255) NOT NULL, hour_opening VARCHAR(255) NOT NULL, declining_rate_percentage DOUBLE PRECISION DEFAULT NULL, has_parking INT DEFAULT 0, has_restaurant INT DEFAULT 0, tour_reference VARCHAR(254) DEFAULT NULL, price_categories INT DEFAULT 1, INDEX IDX_D57C02D23DA5256D (image_id), INDEX IDX_D57C02D2B7970CF8 (artist_id), INDEX IDX_D57C02D254177093 (room_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE concert_order (concert_id INT NOT NULL, order_id INT NOT NULL, INDEX IDX_F08C79BF83C97B2E (concert_id), INDEX IDX_F08C79BF8D9F6D38 (order_id), PRIMARY KEY(concert_id, order_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `group` (id INT AUTO_INCREMENT NOT NULL, music_category_id INT NOT NULL, image_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, description TEXT NOT NULL, INDEX IDX_6DC044C5BC36F4F1 (music_category_id), INDEX IDX_6DC044C53DA5256D (image_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE image (id INT AUTO_INCREMENT NOT NULL, url TEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE music_category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE newsletter (id INT AUTO_INCREMENT NOT NULL, person_id INT DEFAULT NULL, date DATETIME NOT NULL, email VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_7E8585C8217BBB47 (person_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `order` (id INT AUTO_INCREMENT NOT NULL, basket_id INT DEFAULT NULL, person_id INT DEFAULT NULL, number INT NOT NULL, date DATETIME NOT NULL, INDEX IDX_F52993981BE1FB52 (basket_id), INDEX IDX_F5299398217BBB47 (person_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE privatization (id INT AUTO_INCREMENT NOT NULL, person_id INT DEFAULT NULL, room_id INT NOT NULL, lastname_user VARCHAR(255) DEFAULT NULL, firtsname_user VARCHAR(255) DEFAULT NULL, company VARCHAR(255) DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, phone VARCHAR(255) DEFAULT NULL, number_of_person INT DEFAULT NULL, description_project TEXT DEFAULT NULL, date DATETIME DEFAULT NULL, budget DOUBLE PRECISION DEFAULT NULL, hour VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT NULL, INDEX IDX_35CCF7BD217BBB47 (person_id), INDEX IDX_35CCF7BD54177093 (room_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE role (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE room (id INT AUTO_INCREMENT NOT NULL, image_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(1000) DEFAULT NULL, number_seat INT NOT NULL, latitude VARCHAR(24) NOT NULL, longitude VARCHAR(24) NOT NULL, created_at DATETIME NOT NULL, address VARCHAR(255) NOT NULL, code_postal VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, INDEX IDX_729F519B3DA5256D (image_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_ticket (id INT AUTO_INCREMENT NOT NULL, libelle VARCHAR(255) NOT NULL, price DOUBLE PRECISION DEFAULT NULL, tagline VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_ticket_concert (type_ticket_id INT NOT NULL, concert_id INT NOT NULL, INDEX IDX_8F737214C4C606C2 (type_ticket_id), INDEX IDX_8F73721483C97B2E (concert_id), PRIMARY KEY(type_ticket_id, concert_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `user` (id INT AUTO_INCREMENT NOT NULL, pseudo VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, email VARCHAR(180) NOT NULL, civility VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, firstname VARCHAR(255) NOT NULL, address VARCHAR(500) NOT NULL, accomodation_type VARCHAR(255) NOT NULL, additional_address VARCHAR(255) DEFAULT NULL, code_postal VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, country VARCHAR(255) NOT NULL, phone VARCHAR(50) NOT NULL, date_birthday DATETIME NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_article (user_id INT NOT NULL, article_id INT NOT NULL, INDEX IDX_5A37106CA76ED395 (user_id), INDEX IDX_5A37106C7294869C (article_id), PRIMARY KEY(user_id, article_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_shares (user_id INT NOT NULL, article_id INT NOT NULL, INDEX IDX_F50D6A41A76ED395 (user_id), INDEX IDX_F50D6A417294869C (article_id), PRIMARY KEY(user_id, article_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_role (user_id INT NOT NULL, role_id INT NOT NULL, INDEX IDX_2DE8C6A3A76ED395 (user_id), INDEX IDX_2DE8C6A3D60322AC (role_id), PRIMARY KEY(user_id, role_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE video (id INT AUTO_INCREMENT NOT NULL, groupes_id INT NOT NULL, url VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_7CC7DA2C305371B (groupes_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E6612469DE2 FOREIGN KEY (category_id) REFERENCES article_category (id)');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E66442FE589 FOREIGN KEY (person_article_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E663DA5256D FOREIGN KEY (image_id) REFERENCES image (id)');
        $this->addSql('ALTER TABLE basket ADD CONSTRAINT FK_2246507B217BBB47 FOREIGN KEY (person_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE booking ADD CONSTRAINT FK_E00CEDDE1BE1FB52 FOREIGN KEY (basket_id) REFERENCES basket (id)');
        $this->addSql('ALTER TABLE booking ADD CONSTRAINT FK_E00CEDDE217BBB47 FOREIGN KEY (person_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE booking_line ADD CONSTRAINT FK_C98596B883C97B2E FOREIGN KEY (concert_id) REFERENCES concert (id)');
        $this->addSql('ALTER TABLE booking_line ADD CONSTRAINT FK_C98596B8C4C606C2 FOREIGN KEY (type_ticket_id) REFERENCES type_ticket (id)');
        $this->addSql('ALTER TABLE booking_line ADD CONSTRAINT FK_C98596B83301C60 FOREIGN KEY (booking_id) REFERENCES booking (id)');
        $this->addSql('ALTER TABLE concert ADD CONSTRAINT FK_D57C02D23DA5256D FOREIGN KEY (image_id) REFERENCES image (id)');
        $this->addSql('ALTER TABLE concert ADD CONSTRAINT FK_D57C02D2B7970CF8 FOREIGN KEY (artist_id) REFERENCES `group` (id)');
        $this->addSql('ALTER TABLE concert ADD CONSTRAINT FK_D57C02D254177093 FOREIGN KEY (room_id) REFERENCES room (id)');
        $this->addSql('ALTER TABLE concert_order ADD CONSTRAINT FK_F08C79BF83C97B2E FOREIGN KEY (concert_id) REFERENCES concert (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE concert_order ADD CONSTRAINT FK_F08C79BF8D9F6D38 FOREIGN KEY (order_id) REFERENCES `order` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE `group` ADD CONSTRAINT FK_6DC044C5BC36F4F1 FOREIGN KEY (music_category_id) REFERENCES music_category (id)');
        $this->addSql('ALTER TABLE `group` ADD CONSTRAINT FK_6DC044C53DA5256D FOREIGN KEY (image_id) REFERENCES image (id)');
        $this->addSql('ALTER TABLE newsletter ADD CONSTRAINT FK_7E8585C8217BBB47 FOREIGN KEY (person_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_F52993981BE1FB52 FOREIGN KEY (basket_id) REFERENCES basket (id)');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_F5299398217BBB47 FOREIGN KEY (person_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE privatization ADD CONSTRAINT FK_35CCF7BD217BBB47 FOREIGN KEY (person_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE privatization ADD CONSTRAINT FK_35CCF7BD54177093 FOREIGN KEY (room_id) REFERENCES room (id)');
        $this->addSql('ALTER TABLE room ADD CONSTRAINT FK_729F519B3DA5256D FOREIGN KEY (image_id) REFERENCES image (id)');
        $this->addSql('ALTER TABLE type_ticket_concert ADD CONSTRAINT FK_8F737214C4C606C2 FOREIGN KEY (type_ticket_id) REFERENCES type_ticket (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE type_ticket_concert ADD CONSTRAINT FK_8F73721483C97B2E FOREIGN KEY (concert_id) REFERENCES concert (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_article ADD CONSTRAINT FK_5A37106CA76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_article ADD CONSTRAINT FK_5A37106C7294869C FOREIGN KEY (article_id) REFERENCES article (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_shares ADD CONSTRAINT FK_F50D6A41A76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_shares ADD CONSTRAINT FK_F50D6A417294869C FOREIGN KEY (article_id) REFERENCES article (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_role ADD CONSTRAINT FK_2DE8C6A3A76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_role ADD CONSTRAINT FK_2DE8C6A3D60322AC FOREIGN KEY (role_id) REFERENCES role (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE video ADD CONSTRAINT FK_7CC7DA2C305371B FOREIGN KEY (groupes_id) REFERENCES `group` (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user_article DROP FOREIGN KEY FK_5A37106C7294869C');
        $this->addSql('ALTER TABLE user_shares DROP FOREIGN KEY FK_F50D6A417294869C');
        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E6612469DE2');
        $this->addSql('ALTER TABLE booking DROP FOREIGN KEY FK_E00CEDDE1BE1FB52');
        $this->addSql('ALTER TABLE `order` DROP FOREIGN KEY FK_F52993981BE1FB52');
        $this->addSql('ALTER TABLE booking_line DROP FOREIGN KEY FK_C98596B83301C60');
        $this->addSql('ALTER TABLE booking_line DROP FOREIGN KEY FK_C98596B883C97B2E');
        $this->addSql('ALTER TABLE concert_order DROP FOREIGN KEY FK_F08C79BF83C97B2E');
        $this->addSql('ALTER TABLE type_ticket_concert DROP FOREIGN KEY FK_8F73721483C97B2E');
        $this->addSql('ALTER TABLE concert DROP FOREIGN KEY FK_D57C02D2B7970CF8');
        $this->addSql('ALTER TABLE video DROP FOREIGN KEY FK_7CC7DA2C305371B');
        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E663DA5256D');
        $this->addSql('ALTER TABLE concert DROP FOREIGN KEY FK_D57C02D23DA5256D');
        $this->addSql('ALTER TABLE `group` DROP FOREIGN KEY FK_6DC044C53DA5256D');
        $this->addSql('ALTER TABLE room DROP FOREIGN KEY FK_729F519B3DA5256D');
        $this->addSql('ALTER TABLE `group` DROP FOREIGN KEY FK_6DC044C5BC36F4F1');
        $this->addSql('ALTER TABLE concert_order DROP FOREIGN KEY FK_F08C79BF8D9F6D38');
        $this->addSql('ALTER TABLE user_role DROP FOREIGN KEY FK_2DE8C6A3D60322AC');
        $this->addSql('ALTER TABLE concert DROP FOREIGN KEY FK_D57C02D254177093');
        $this->addSql('ALTER TABLE privatization DROP FOREIGN KEY FK_35CCF7BD54177093');
        $this->addSql('ALTER TABLE booking_line DROP FOREIGN KEY FK_C98596B8C4C606C2');
        $this->addSql('ALTER TABLE type_ticket_concert DROP FOREIGN KEY FK_8F737214C4C606C2');
        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E66442FE589');
        $this->addSql('ALTER TABLE basket DROP FOREIGN KEY FK_2246507B217BBB47');
        $this->addSql('ALTER TABLE booking DROP FOREIGN KEY FK_E00CEDDE217BBB47');
        $this->addSql('ALTER TABLE newsletter DROP FOREIGN KEY FK_7E8585C8217BBB47');
        $this->addSql('ALTER TABLE `order` DROP FOREIGN KEY FK_F5299398217BBB47');
        $this->addSql('ALTER TABLE privatization DROP FOREIGN KEY FK_35CCF7BD217BBB47');
        $this->addSql('ALTER TABLE user_article DROP FOREIGN KEY FK_5A37106CA76ED395');
        $this->addSql('ALTER TABLE user_shares DROP FOREIGN KEY FK_F50D6A41A76ED395');
        $this->addSql('ALTER TABLE user_role DROP FOREIGN KEY FK_2DE8C6A3A76ED395');
        $this->addSql('DROP TABLE article');
        $this->addSql('DROP TABLE article_category');
        $this->addSql('DROP TABLE basket');
        $this->addSql('DROP TABLE booking');
        $this->addSql('DROP TABLE booking_line');
        $this->addSql('DROP TABLE concert');
        $this->addSql('DROP TABLE concert_order');
        $this->addSql('DROP TABLE `group`');
        $this->addSql('DROP TABLE image');
        $this->addSql('DROP TABLE music_category');
        $this->addSql('DROP TABLE newsletter');
        $this->addSql('DROP TABLE `order`');
        $this->addSql('DROP TABLE privatization');
        $this->addSql('DROP TABLE role');
        $this->addSql('DROP TABLE room');
        $this->addSql('DROP TABLE type_ticket');
        $this->addSql('DROP TABLE type_ticket_concert');
        $this->addSql('DROP TABLE `user`');
        $this->addSql('DROP TABLE user_article');
        $this->addSql('DROP TABLE user_shares');
        $this->addSql('DROP TABLE user_role');
        $this->addSql('DROP TABLE video');
    }
}
