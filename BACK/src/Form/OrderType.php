<?php

namespace App\Form;

use App\Entity\Basket;
use App\Entity\Concert;
use App\Entity\Order;
use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('number', NumberType::class, ['required' => true])
        ->add('date', DateType::class, ['widget' => 'single_text'], ['required' => true])
        ->add('person', EntityType::class, array(
            'class' => User::class,
            'choice_label' => function (User $user) {
                return ['lastname' => $user->getFirstname(), 
                'firstname' => $user->getLastname()];
            },
            'expanded' => true,
            'required' => true))
        ->add('concerts', EntityType::class, array(
            'class' => Concert::class,
            'choice_label' => function (Concert $concert) {
                return ['price' => $concert->getPrice(), 
                'nameGroup' => $concert->getNameGroup(),
                'tourName' => $concert->getTourName(),
                'date' => $concert->getDate(),
                'hour' => $concert->getHourOpening(),
                'location' => $concert->getLocation(),
                'image' => $concert->getImages()];
            },
            'expanded' => true,
            'required' => true))
        ->add('basket', EntityType::class, array(
            'class' => Basket::class,
            'choice_label' => function (Basket $basket) {
                return ['basket' => $basket->getDate(), 
                'totalAmount' => $basket->getTotalAmount(),
                'reservations' => $basket->getReservations()];
            },
            'expanded' => true,
            'required' => true))
        ->add('Modifier', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Order::class,
        ]);
    }
}
