<?php

namespace App\Form;

use App\Entity\Article;
use App\Entity\ArticleCategory;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraints\File;

class ArticleType extends AbstractType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $roles = $this->security->getUser()->getRoles();
        $isAdmin = false;
        foreach($roles as $role){
            if($role == 'ROLE_ADMIN'){
                $isAdmin = true;
                break;
            }
        }
        $builder
            ->add('date', DateType::class, ['widget' => 'single_text'])
            ->add('title', TextType::class)
            ->add('description', TextareaType::class);

        if($isAdmin) {
            $builder->add('admin', EntityType::class, [
                'class' => User::class,
                'multiple' => false,
                'query_builder' => function (UserRepository $repo) {
                    return $repo->createQueryBuilder('r');
                }
            ]);
        }

        $builder->add('category', EntityType::class, array(
            'class' => ArticleCategory::class,
            'choice_label' => 'name',
            'expanded' => true,
            'required' => true
        ))
        ->add('image', FileType::class, [
        'label' => 'Ajouter une image',
        'mapped' => false,
        'required' => false,
        'constraints' => [
            new File([
                'maxSize' => '1024k',
                'mimeTypes' => [
                    'image/jpeg',
                    'image/jpg',
                    'image/png',
                    'image/webp'
                ],
                'mimeTypesMessage' => 'Please upload either a jpeg or png picture',
            ])

        ],
        ])->add('Publier', SubmitType::class)
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}
