<?php

namespace App\Form;

use App\Entity\Concert;
use App\Entity\MusicCategory;
use App\Entity\PriceCategory;
use Doctrine\DBAL\Types\BooleanType;
use Doctrine\DBAL\Types\FloatType;
use Doctrine\DBAL\Types\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraints\File;

class ConcertType extends AbstractType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    const AIX = 'AIX-EN-PROVENCE';
    const BOURGES = 'BOURGES';
    const CANNES = 'CANNES';
    const DUNKERQUE = 'DUNKERQUE';
    const ECHIROLLES = 'ECHIROLLES';

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nameGroup', TextType::class,['required' => true])
            ->add('tourName', TextType::class,['required' => true])
            ->add('date', DateType::class, ['widget' => 'single_text'],['required' => true])
            ->add('hour', TimeType::class, ['input'  => 'timestamp','widget' => 'choice'],['required' => true])
            ->add('hourOpening', TimeType::class, ['input'  => 'timestamp','widget' => 'choice'],['required' => true])
            ->add('location', ChoiceType::class, [
                'choices' => [
                    'AIX-EN-PROVENCE' => self::AIX,
                    'BOURGES' => self::BOURGES,
                    'CANNES' => self::CANNES,
                    'DUNKERQUE' => self::DUNKERQUE,
                    'ECHIROLLES' => self::ECHIROLLES,
                ],
                'multiple' => false,
                'required' => true
            ])
            ->add('priceCategories', EntityType::class, array(
                'class' => PriceCategory::class,
                'choice_label' => 'libelle',
                'expanded' => true,
                'required' => true))
            ->add('price', FloatType::class, ['required' => true])
            ->add('decliningRatePercentage', NumberType::class, ['required' => false])
            ->add('priceCategories', EntityType::class, array(
                'class' => PriceCategory::class,
                'choice_label' => 'libelle',
                'choice_value' => 'id',
                'choice_attr' => function ($choice, $key, $value) {
                    return ['minPrice' => $choice->getMinPrice(), 'maxPrice' => $choice->getMaxPrice()];
                },
                'expanded' => true,
                'required' => true)
                )
            ->add('musicCategory', EntityType::class, array(
                'class' => MusicCategory::class,
                'choice_label' => 'libelle',
                'choice_value' => 'id',
                'multiple' => false,
                'required' => true)
                )
            ->add('hasParking', BooleanType::class, ['required' => true])
            ->add('hasRestaurant', BooleanType::class, ['required' => true])
            ->add('description', TextareaType::class,['required' => true])
            ->add('image', FileType::class, [
                'label' => 'Ajouter une image',
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/jpg',
                            'image/png',
                            'image/webp'
                        ],
                        'mimeTypesMessage' => 'Please upload either a jpeg or png picture',
                    ])
                ],
                ])
            ->add('Créer le concert', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Concert::class,
        ]);
    }
}
