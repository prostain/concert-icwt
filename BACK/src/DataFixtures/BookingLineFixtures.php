<?php

namespace App\DataFixtures;

use App\Entity\Booking;
use App\Entity\Concert;
use App\Entity\TypeTicket;
use App\Entity\BookingLine;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Faker\Factory;

class BookingLineFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');


        //On ajoute la CAT1 aux 36 premières places
        for ($i = 1; $i < 38; $i++) {

            if(rand(0, 1))
            {
                $bookingLine = new BookingLine();

                $bookingLine->setSeat($i)
                ->setConcert($this->getReference(Concert::class.'_'. $faker->numberBetween(0, 49)))
                ->setTypeTicket($this->getReference(TypeTicket::class.'_'. $faker->numberBetween(0, 2)))
                ->setBooking($this->getReference(Booking::class.'_'. $faker->numberBetween(0, 499)));

                $manager->persist($bookingLine);
                $this->addReference(BookingLine::class.'_'.$i, $bookingLine);
                $manager->flush();
                
            }
         }

        //On ajoute la CAT2 aux 36 places suivantes
         for ($i = 38; $i < 75; $i++) {

            if(rand(0, 1))
            {
                $bookingLine = new BookingLine();

                $bookingLine->setSeat($i)
                ->setConcert($this->getReference(Concert::class.'_'. $faker->numberBetween(0, 49)))
                ->setTypeTicket($this->getReference(TypeTicket::class.'_'. $faker->numberBetween(0, 2)))
                ->setBooking($this->getReference(Booking::class.'_'. $faker->numberBetween(0, 499)));

                $manager->persist($bookingLine);
                $this->addReference(BookingLine::class.'_'.$i, $bookingLine);
                $manager->flush();
            }
         }

        //On ajoute la CAT3 aux 36 dernières places
         for ($i = 75; $i < 112; $i++) {

            if(rand(0, 1))
            {
                $bookingLine = new BookingLine();

                $bookingLine->setSeat($i)
                ->setTypeTicket($this->getReference(TypeTicket::class.'_'. $faker->numberBetween(0, 2)))
                ->setConcert($this->getReference(Concert::class.'_'. $faker->numberBetween(0, 49)))
                ->setBooking($this->getReference(Booking::class.'_'. $faker->numberBetween(0, 499)));

                $manager->persist($bookingLine);
                $this->addReference(BookingLine::class.'_'.$i, $bookingLine);
                $manager->flush();            
            }
         }
    }

    public function getOrder() {
        return 11;
    }
}
