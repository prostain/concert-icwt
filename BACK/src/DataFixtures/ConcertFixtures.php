<?php

namespace App\DataFixtures;

use App\Entity\Concert;
use App\Entity\Image;
use App\Entity\Group;
use App\Entity\Room;
use App\Entity\Order;
use App\Entity\TypeTicket;
use App\Entity\BookingConcertTicket;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

use Faker\Factory;

class ConcertFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        //Lieu des salles
        $locationList = ['AIX-EN-PROVENCE','BOURGES','CANNES','DUNKERQUE','ECHIROLLES'];

        //On ajoute des catégories de musique dans la table

        //On ajout des images de concert
        $imagesConcert = [];
        $urlsConcert = ['https://statics.lesinrocks.com/content/thumbs/uploads/2018/09/width-1125-height-612-quality-10/stones.jpg','https://toutelaculture.com/wp-content/uploads/2020/07/concert-2_0-525x349-1.jpg','https://img.huffingtonpost.com/asset/5f1a00b8270000b10fe674d7.jpeg?cache=vM0RbxN9YX&ops=scalefit_630_noupscale',
        'https://www.01net.com/i/0/0/a12/23c69056808bee716ff7a67f12e0c.jpeg','https://auxerrexpo.com/wp-content/uploads/2019/01/club-1-1200x675.jpg','https://cdn.radiofrance.fr/s3/cruiser-production/2020/06/44596e32-e897-4e91-94b0-0b7c3339f7d1/870x489_95438567_1653751264748526_5788465361988878336_n.jpg',
        'https://static.latribune.fr/full_width/978929/printemps-de-bourges-2017.jpg','https://www.toutsurmesfinances.com/argent/wp-content/uploads/sites/3/2017/02/concert.jpg','https://photos.lci.fr/images/1280/720/concert-discotheque-club-f0add5-0@1x.jpeg'];
        
        
        foreach($urlsConcert as $urlConcert){
            $image = new Image();
            $image->setUrl($urlConcert);
            $manager->persist($image);
            $imagesConcert[] = $image;
            $manager->flush();
        }
        $nbImages = count($imagesConcert) -1;
        

        //on ajoute 50 concerts
        for ($i = 0; $i < 360; $i++) {
            $description = $faker->sentence(500, true);
            $concert = new Concert();

            $dt = $faker->dateTimeBetween('+1 month', '+6 month', 'Europe/Paris');
            $date = $dt->format("d/m/Y");

            $first = 1;
            $concert->setDate($date)
            ->setHour($faker->time('H:i', 'now'))
            ->setHourOpening($faker->time('H:i', 'now'))
            ->setTourName($faker->sentence(1, true))
            ->setHasRestaurant($first)
            ->setHasParking($first)
            ->setPrice($faker->randomFloat(2,30,900))
            ->setDescription($description)
            ->setLocation($faker->randomElement($locationList))
            ->addOrder($this->getReference(Order::class . '_' . $faker->numberBetween(0, 349)))
            ->setTourReference('Tour_'.$faker->numberBetween(0, 200))
            ->setPriceCategories($faker->numberBetween(1, 3))
            ->setDecliningRatePercentage(5)
            ->setImage($imagesConcert[$faker->numberBetween(0, $nbImages)])
            ->addTypeTicket($this->getReference(TypeTicket::class . '_0'))
            ->addTypeTicket($this->getReference(TypeTicket::class . '_' . $faker->numberBetween(1, 2)))
            ->setRoom($this->getReference(Room::class . '_' . $faker->numberBetween(0, 4)))
            ->setArtist($this->getReference(Group::class . '_' . $faker->numberBetween(0, 49)));

            $manager->persist($concert);
            $this->addReference(Concert::class .'_' .$i, $concert);
            $manager->flush(); 
        }
        
    }

    public function getOrder() {
        return 8;
    }
}
