<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\ArticleCategory;
use App\Entity\Image;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Faker\Factory;

class ArticleCategoryFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        //On ajoute des catégories d'articles
        $articleCategoryList = ['Infos','Concert'];

        for ($i = 0; $i < count($articleCategoryList); $i++) {

            $articleCategory = new ArticleCategory();
            $articleCategory->setName($articleCategoryList[$i]);
            $this->addReference(ArticleCategory::class . '_' . $i, $articleCategory);
            $manager->persist($articleCategory);

         }

        //On ajoute 50 articles
        for ($i = 0; $i < 50; $i++) {
            $articleCat = $this->getReference(ArticleCategory::class.'_'.$faker->numberBetween(0, count($articleCategoryList)-1));
            $articleCat->addArticle($this->getReference(Article::class.'_'.$i));
            $manager->persist($articleCat);
       }
       $manager->flush();
    }

    public function getOrder() {
        return 5;
    }
}
