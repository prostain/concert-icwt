<?php

namespace App\DataFixtures;

use App\Entity\Booking;
use App\Entity\Concert;
use App\Entity\Group;
use App\Entity\PriceCategory;
use App\Entity\Privatization;
use App\Entity\Room;
use App\Entity\BookingLine;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class BookingFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        //On ajoute 500 réservations
        for ($i = 0; $i < 500; $i++) {
            $booking = new Booking();
            $booking->setNumero($faker->sentence(10, true))
            ->setPriceInsurance($faker->randomFloat(0,0,0))
            ->setComment($faker->sentence(10, true))
            ->setHasInsuranceCancellation($faker->boolean)
            ->setHasBookParking($faker->boolean)
            ->setHasBookRestaurant($faker->boolean)
            ->setPrice($faker->randomNumber());
            $cat = "CAT 1"; //$booking->getPriceCategory()->getLibelle();

            if($cat == "CAT 1"){
                $booking->setPrice($faker->randomFloat(2, 170, 650));
            }
            if($cat == "CAT 2"){
                $booking->setPrice($faker->randomFloat(2, 150, 450));
            }
            if($cat == "CAT 3"){
                $booking->setPrice($faker->randomFloat(2, 30, 130));
            }

            $hasBookPark = $booking->getHasBookParking();
            if($hasBookPark == true){
                $booking->setNumberSeatParking($faker->numberBetween(1,10));
            }

            $hasBookRestaurant = $booking->getHasBookRestaurant();
            if($hasBookRestaurant == true){
                $booking->setNumberPersonRestaurant($faker->numberBetween(1,5));
            }

            $hasInsuranceCancellation = $booking->getHasInsuranceCancellation();
            $price = $booking->getPrice();
            if($hasInsuranceCancellation == true){
                $booking->setPriceInsurance(10.00);
                $priceInsurance = $booking->getPriceInsurance();
                $booking->setPrice($price + $priceInsurance);
            }
            $this->addReference(Booking::class . '_' . $i, $booking);
            $manager->persist($booking);

       }
    }

    public function getOrder() {
        return 10;
    }
}
