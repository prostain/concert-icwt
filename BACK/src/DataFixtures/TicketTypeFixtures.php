<?php

namespace App\DataFixtures;

use App\Entity\TypeTicket;
use App\Entity\BookingConcertTycket;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
class TicketTypeFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        //E-ticket
        $typeTicket = new TypeTicket();        
        $tagLine = "Imprimez vos billets chez vous dès la fin de votre commande et recevez-les également par e-mail en format pdf.";
        $typeTicket->setLibelle('E-ticket')
        ->setPrice(0)
        ->setTagline($tagLine);
        $this->addReference(TypeTicket::class . '_' . 0, $typeTicket);
        $manager->persist($typeTicket);

        //Ticket retrait
        $typeTicket = new TypeTicket();       
        $tagLine = "Retirez vos billets auprès de nos guichets (comprend des frais de transaction).";
        $typeTicket->setLibelle('Retrait au guichet')
        ->setPrice(1,80)
        ->setTagline($tagLine);

        $this->addReference(TypeTicket::class . '_' . 1, $typeTicket);
        $manager->persist($typeTicket);

        //Ticket envoi posatal
        $typeTicket = new TypeTicket();        
        $tagLine = "Recevez vos billets à votre domicile ou sur votre lieu de travail. Envoi suivi avec remise contre signature. Livraison sous 24 à 48h.";
        $typeTicket->setLibelle('Envoi postal')
        ->setPrice(8,00)
        ->setTagline($tagLine);

        $manager->persist($typeTicket);
       $this->addReference(TypeTicket::class . '_' . 2, $typeTicket);
       $manager->flush();
    }
    public function getOrder() {
        return 1;
    }
}
