<?php

namespace App\DataFixtures;

use App\Entity\Concert;
use App\Entity\Image;
use App\Entity\Privatization;
use App\Entity\Room;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

use Faker\Factory;


class RoomFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        //Lieu des salles
        $locationList = ['AIX-EN-PROVENCE','BOURGES','CANNES','DUNKERQUE','ECHIROLLES'];
        $coo = [
           
            ["43.52829879854052","5.446696337865793"],
            ["47.0818902796614","2.3946980908189235"],
            ["43.55554122701528","7.000600954525664"],
            ["51.03114344459803","2.3750131700580472"],
            ["45.1480150502746","5.713632256776191"]
        ];

        //On ajoute des images de salle/parking
        $imagesRoom = [];
        $urlsRoom = ['https://congres.maisondelachimie.com/wp-content/uploads/2015/03/medias-salle-008-02.jpg','data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUSEhMVFRUXFxUVFRUVFxUVFRUVFRUYFxUVFRUYHSggGB0lHRUVITEhJSkrLi4uFx8zODMsNygtLisBCgoKDg0OGhAQGi0dHR8tLS0tLS0tLS0tLS0tKy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAMIBAwMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAACAAEDBAUGBwj/xABHEAABAwEDBwcIBwcDBQAAAAABAAIDEQQhMQUSQVFhcZEGIlKBsdHwExUjQnKSocEyU4KistLhBxQzYrPC8SRDcyU0Y4PD/8QAGQEAAwEBAQAAAAAAAAAAAAAAAAECAwQF/8QAKxEAAgIBAwMDBAEFAAAAAAAAAAECEQMSIVEEMUETInEUMoHwsQUzYZGh/9oADAMBAAIRAxEAPwCOTJ7xzXNdsIae0YaLlJk2aSB/0SQcQa0c35HboK9Cz0xctDnspWWdsjQ5pqDxrpBGg/5UhYpXAFBXQf8AKAInRhRmNWCgKQyq6O/xjo8bkxjU7go/H6pDIXRqMxqyUDggZVfGlmqchRgeOz4USKIi1CWqYhAQgZEQgp46v8qUoPHxSGAQhKkKEoGAobVMGtLjgASdwU6xsvwyvaGRtJBNXGoFwwF5138FLexUVbOdHpJHSPqB9J2wDAD4AJMbnudI/wCiLyAdGDWN+A+KuS5MmDQ0Rm+9xqMdAx0Y9exKXJ0vNjDDmjF2guOJ3DAdetItlNjQ9zpZKZopcNJ9WNuq4dQCt5MsJmeZJL21wpQE6GDUAKbgEYsD3uazNcxgwJbxcdpphuC6OzwhjQ0CgFyaJbHbHT9LuoDUjonopGMTIBYxXrLZC4qSx2MnctZjQ0UCBAQwhou4qnlPKjYhT6TtDR2nUFSyplylWxXnS/R9nXvXPOJJqbydJxKlsuML7klstT5HZzzXUNA2AKsURQlSaUAklRJAUehNtA196MSA6Vx0VtdtV2G2uWqyI5niaOjztqFyyoradRVlk5OtPUhaGWg5MSo86qcHYixUIlRv1qSiAsRYwM5AXJyyiEtRYxi5R5/jx1Ii1RualYxy9AXpEIHBFjEXIQ5C4JikMIlCSgKAlIZISmJUdUOcgCQlMSoy5CXIGSpKIOUsYQBLGxadisdbzh2obBZdJ4KfKGUWxChvdoaPnqCBVZammbG2riGtHigGkrmcp5WdLzRzWatJ9ruVW2Wt0js5x3DQNwVYlS2aRhQ5QkpFA4qSxyo3uAxKiktGho60DYSb3I7h27iNqGopkZawXEjiElWkWpGzHErkESije3pDiFche3WOIUIlhxxq7G1QMI1hWo1RDZI0KQJmhEmIZMSiqhKABQOKIoHIAAlA5ESgckMjco3I3KNxQAzgFG4BGSoyUWOkC4BRkBGSgJRY6QBCAgIygKVjpAkJs1OmRbCkE1i0bFCKVWbnIJ53FuZWg0007ynbDSaFtyyG82OhOl2gbtZWI+WpqTUnElCYtqEx7VLtlpJBF6EvQmPaq7rMTi87hQBFDtCtNva0Y+PmqRfLJgMxvScL+pverzbK0X6dd1U8lnBxvGqt3BWkS5FYWpjLs7OPV2hRPtLnbBs71dbZmjADgnzEybM8RJK/mpJ7BbNCHI87qUicaiooK3dW5W28n56VMTgNJIoAF2HJR9TTUGdr11xjBxCyhhjVjnnknR402zs0ysG9zR2q1FY6mjXNcf5SCeAXa8qOTNnlYXFpa6ovbdW/VrXKGxRwfwom6qvq8nfU04AJyjGIoTnMYWGQaDwKcQPGg/FTsy/MLi1lNjT3rVs8loeA4RCh05rgPiUoyhLtZUozj3oxOf8AzfFLPdrdxK6EWebSxo496jlhLb3Cg66K9BGswfKu6TuJSMzukeJWsZo9LvgVJFA0ioNRuqjQGtcGJ5V2s8UxmdrKnypyfMlXNkdXU4nN6qYcFQydySe0l0kxOoDOIHEqGmWmiYzHWhMrtfYtSPJAHrE7x+qfzaNfwRTH7TIMrtfYhMx1rW83DZwURybtHBLce3BmGUpjKUspWFxJzJmDRmVa0jccT1qnk3JE5NZLQ3Yxpa7ie5JulY0ldUWjKUJlKuDJpGLgUvN+74paitCKJlKbyqunJ52cSh83nwafJLUPQuCkZtiF0mxV7fBaW/RjB1ZtXcam5SWLJ9ppWXMqcGt0bzXsTcmJRT8Dl6EuVr9wfq+ITGwO1diNTDQioXISVcOT3ail5tf0TwVJsTiiiSlnIrc0xfSa/fmnN4qjBaXvdRkRcBi6tw3miq2RUbotlyEuVhtlOlELJXC/cnbBxRTzkle83u6PxCZFsVI7jkg7nu3M/vXbNwXCcj3ekduZ/eu6GC0x/ac+T7jPy4fRHeO1cTaxVdnygPoTvb2rjJSs8xtgK8MN69FyY30Md3qhcFCu/wAmfwI/ZHYpwrdlZ3sjIytygbBJmOiLq4FpGoG9p361JZ8pxyeo4bwPkVxvL0/9Qsgqfpy4HH0LcV0mTxgrUnZm4R0o03xtxoOAXNu5UWIXeWYDeCKZpqDShC6V/wBE7j2L5r5WxEWkAEirn/iHeoy5JKSivNm3T4YTUnK9qPbjyishwlbwPcg8+2b60e67uXBZGmspZIbQI2Pa2rB5JpEh1XDmlRxZSs2iBo+w3fr2LklnzcL9/J1rp8HL/fwegOy1Z8fKD3X46sEPniD6wcHdy5KO12VzWjyVDU35ooQaaNFNdepalkydZnkAsA+z+u1Q8+Xgr0cK7tmx51g+sHA9yQyhCf8AcapbPyVspxDhuHWhl5MweTFWlpFTQgE46XMdfcP8JuebgVYOWVZILMed6KhrUknr0JoX2ZhJa+IE40cPmqU8MDGFtXZhN7c6UAkCt7Qb6LKNosnQdxlHzWbyZODSMMXJ0v75D9bH77e9P+8xfWx++3vXMtyhZRcGuG4v/uqr1qjs/kGzMo5znFvkw+TPG033BClkfgbjiXk1/LR/WM99vekXM6bDuc1craLG90Esgq3MjkcKFxwYSK5xNcFz3ImaSV8pea5uYW3AUOcbxQbAnJzjFyfgqGPHP7W/9Hok0DX4k01BwA66KJthjGk+8nigcWZ773OLiTQCtXHQLlBIyiqOS1aM3jSdF4AaFK2BVoJDQAAaLysH9mjibNJU19M7H/jjWsXdmU00dZHGAnck1C7FbRMGQTqvQKaYKEhFCsVBpT0TBOrSM5MFJJJXRFmlyRPpXbmf3rvm4Lzzki/0zhsZ2vXf59y0xfaZZtpszeUrvQHe3tXGOK6vlPJ6E+035rkSVhn2Zv0+8SSMr0HJR9BH7DexeeMXoGS3egi9hvYlg7sfU9kcTytsHlLbFJUjyTnGmvPja29bNgKo5ed/qHfZ/CFZsL04P3v5Ca9i+DZeeadx7F8/8tmA2mDaXfiC95fJzTuPYvCeVw/1Vn3n8QWfVbZIfn+Do/p+6n+P5IMrxZp6iVasVmq5w2n8D0fKCKgd7KuWOOj3+1/ZIsvBT7lyOzACuwH77u9dNYgBQ/yD8LO5YBPMPs9lD81oRznN+yfg1KxPc7iyWoZoP8rfwEqS32oFvjouXJw27mfZH9EorTbzm1r4zHqtRno3IcrxA1oNMx4RtC5yey30ph+vctu2z1Lt1o7Gqq8c7fX4B3cpNFsc++yUrub8QFYyVB6RwOsDtVu0sufsEXxaE+Tmekk2O+Q70wbOitzQ2w2mn1M39Ny4L9m7PSTewz8RXc5Yf/o7R/wy/wBMriP2dH0svsN7Vh1X9p/vk7+hVwl8nrkNmDrOy7QfxFc/bbLQ3LosmS1gb9v8blQtrKqIfZH4RhbU5fLMmzMw8aFW5K5IFnhLWuLs97n30uuDaCnsjitYx0odfaAmsI9G3r7StsV2ycr2RI0JZt6kaE4au2MTilIrSsVdzFfexV3sV6SNRVzUEUWaKVJxvON5qrJYhLVSiQ2RUSUmakqoiznskcqY4pC/MkNQ0UAb6pffUu2/BdNZv2jwuOb5GYUpf6Ol/wBteX9/egjkoT1LzodVkjsj1MnRYpbs9VyxyojkicBHKKc6pDKXAmlzlzQ5QM6D/u96wm245jhXG7jcos7tWeXqJSdl4OljFNHTM5QN6D/u966TJ/LyINbEY5AQ0CtWUNBv2Lzdrr1K2B7nZwLQ0UBc5zWgHbU7RxU4+pnF7GmXpMco+46rL/KRvlc/McQ4Ai9t1BS+/YrFg5TMPqO4t79q4S1z1a2u1TWWeh8awhdRNOxvpIaEj0l/KJuY7mO+i7S3QDt2LyvlZ/3FnPX94LabbDmuv9U/EOWNygge+eLNY51GjBpPrg6Nyc80sk46vF/wV0uCOPVXmi9l0813s/3BSmWkrvaHxjepcp5KlkbzWYt0kg/SBwopPMk5eXZoxacToaR0dqr1I13OT05cAiarSP5XfharLJuaftD7hTRZDn1NwIxOltNSnbkaa+4Yu0nS0jUl6sOR+nLgFsxzOr/5kIrTN6N2wH+m9SjI02bgOJ6NNSefIsxY4UF4IxOlpGraj1I8h6cuCG0TXv8AZtH9iKSXnDe/4Z3elPkSc59zbxMMT6+bs2I3ZGnLq0bi/wBY6a/yo9SPIvTlwUHzVa//ANPYpMnu9JL7Q7GqR+QZ6EAMvzPWd6o9lS2TI87XvJa2jjW55OrW0alSyQ5E8cuC9lZ1bJOP/FJ/TcuM5AH08n/G3tC7e02d/kntcx17HDDOxaR6tVxXIezSNnfnRvb6OnOa4esNYU9XTxbcHf0G0Jp8o9RyRJ6Ee1J/UcmnKz8m5SiZHmvkY0h8lQXAG97iEUmVYPro/eCzx1oXwc84vW9vJoMZVtNxHBZMeUmsaGlrrq3jNpiTrWtY5WuaHtIc04EXg6CuYt5o871U5Sx7oUYqezNJuWWdB/3fzJ/PTB6r/u/mWIXXdaCV3N8bU11eQh9PA3Dl2PoP+5+bYonZcZ0JPufm2LHe7Qmeq+ryEfTYzSOXo+g/7n5k3ntn1b/ufmWNJ8j2BM04gAdddJTXV5OSX08ODVdl9gNMyT7v5kyzQBpaPj3pJ/V5ORfT4+P+nNwgFwDq000pX1sK12KzJZ4WtJAeTQ3lw1bAFVjpnA+PWU8x5h6+xZwSo68rdlGGWtOpXc7tWXZj9HqWmR2rGfc6MfYNhVic+gf7Te1qgzKHEG4G7aMN6rW1srnBrKllxIrQV2jToUxXuLk9gZDzR19ikhciGS5XNAFG3nG/sVqHIcvSbwKe3ItaGs8njirjLS7pOw6R1BNDkOQeu3gdqtx5Dk6TeB1IohyRELW7pv8Aedr3pG1P6b/edq3q15jf0hwPekMhP6Y4HvSa/wAgmip+8v6b/fds2pG0O+sk9923arnmJ9fpj3T3ohkJ3THunvUMtNFMWh/1knvu2Iv3l9P4knvu171b8xu6Y9096IZEd0x7p71DLTRSFof9ZJ77u9EJ3fWP993erjcinp/dRtyOekOCTHcTO8u+v8ST33bNqZ1qeP8Ack993etTzKel8Ez8hkg874JWPVEsxWp1Bz3X09Z2xM20EX51ML7tTdm1WPN5FOcNCjfYTs47tmxdkYXwcbnRWkNbyRorc3SW7NpXO5StErHXuaAb28xpu91dO+xOGGw4jRTbsWXlKyeUZQAZ4AAJ0XivzVTx6atChkvszFZlydoo2WgHRYB8Q2qksmWHucRK8uJoGXaTXG4awspzh0hw/RMXjpfD9EON7Mq0dcXGiad13jUsnJOUAWiNziXXmugius6b1pTG7xqWbjRnYb5LvG1EhERIL9ALQRpq7OpTgU4x4Ipom0RO+Z7Anj0ppWX4ax8AkDcd3iqa8CYQPi5JQmIG+/qqklQHOmUNvOGzrVK05XJq1oAFMSam9TywlwzRdWnzTwZAFauvW8Gkty8m72KlldeN4Wu2MuwHjerdmyNGKc0da2I7KFnOm9iozpGTBYzpx2d6vMs1BctBkCtNhFFKg2wllVGawEK3GSpnRomsVLGyHkQonKwx6jDELxzgq0snUmWw9CZUJUDpEaCdZMZimdIVEXqRrktCK1sdpKcuIU0Yu8ak8rLkvTQeqyqZCh8sdqUx+agc9J40WsjJxMURLlVa5WwTRCxIHlYZmKYTV8blBM/mlUbJMa4rTaIl7jUL/HurOtM9FcY+ov2qpJC2qMlSWwRbT3MTzezotA3BRSWePARt6wFvCBpxCqSQgG4LPcu0Ztnya0c6gB2BWTERtVmiGRMmyJsusfNTsmB0oLqHXdT5oHRjUhom0WDQi41x7EwbiNFAPkVUDaXgo/LnSEUSy5Cy4dfamVdtspcD8EkaWSZcEAGA68TxVpgAxxSjYpGxakzRsmierrJAqUTFYaEyWXY3rQs0BIrRZcIXVWCCkY48V14IqRzZpaUYszL6Io2bU9sPPO9FCUqVhewZjuKpuHOWg83LPGPjglNIcG6ZfZBVpNMFnSihXUZHs2dBI4mlAOHj5Ll7Y28jb2LN7DjuyLOViIqqApYyps0o1YKKW0NFFXsjVZtTObVIkxbU4VVNz1anCquagsliNStySznyIKxLML712OUGAWJhGs1uomtyJOqORm+iVRs2KsyvuKq2fEqZdjaDNBh5qgkKmH0VVlcpQ2yRr1TeVIXINCpKxORGXIHuUhaonFGkVjZyVUJTIoLGzkgcUDk1UxMLNCSiBSQI0GxKZsakayqtxWa7BVDG2ZyyUUfJKRrFakguUYYm4UwU7QdnbpXXB3NFNS5SDFdPnc0bl04NkznzbtGDa3c87yniKG1ir0zCs33LT2J3m47lQaS5wAxVyU80qnZ35rw4G8GvWFMyoHTWO1eSjkYbiaCh2aVzVpN6tW22F7iScTeqL37Fm9y1sO4aUzSjlmq1ooPF96gab/Fymi7NWw3mi6PLFgzYWEaRXjRcrYZaHrK6nK+VA6BrLq4afGhWqp2Zyu1Rx1oxUCkmcTeqxcs6NbLEWNVs2q21hDFhxS0KIzki9MT3GtTKAHXf8lUhxUsjyQAUEbL/ABxSGmWXvuCpvKsTClyqkISG2PVCHJq6E0ZVITZIhcE1OCZWiGRubRASpiUDr0UGoruQgonAqMhTRVjkpIC5JAHTWNgJC2xGKLHycOdVbZK7MSpHDkdsz7cFSJV22BUTcs8nc0g9ieHFbgl5o3Ln4nLWEiIBMrWhhzjRBmKSR6ic5DQJgvKgA1b1I8qF7sepQy0x1G86d6cuQuOOy5RRdjA3JvHBMUxN3WlQ7LETr+tW5p6todCzGdlFK6S9IYT380jbXgq2d3J3SaUFUUFhDUnrRRByWdeigsJxRWeSjgdR4qF+xM46UqHZat1pz3l3wVQuTOcos5FASEpAKMuTkpiDohJQF9Es9NCHJCYoCUxKoQnKJ46lIVE8oAAt3JISSkgdnV5OWokkuqHY459ytalnyJJLOZpAkb3K2CkkkhsjKjBw3HtTJIBEZN3BQjFJJQykSR49fyUKSSTKCOAUWtJJSUh24n7PYiOhJJICF+CFiSSQxFNq8aEkkADJ3Jj3JJIGNJgojoSSTBCHzQOx4pJJDD0oQmSTEC/HqQhJJMQUajcmSTEKiSSSok//2Q==','https://cap.img.pmdstatic.net/fit/http.3A.2F.2Fprd2-bone-image.2Es3-website-eu-west-1.2Eamazonaws.2Ecom.2Fcap.2F2020.2F08.2F04.2Fb5ed2604-6b20-4a6e-a96b-8ce8fea83817.2Ejpeg/1200x630/background-color/ffffff/quality/70/cr/wqkgTWloYWpsbyBNYWR6YXJldmljIC8gR2V0dHkgSW1hZ2VzIC8gQ0FQSVRBTA%3D%3D/immobilier-ce-que-coutent-et-rapportent-les-locations-de-parking-dans-les-grandes-villes-1377168.jpg','https://www.leparisien.fr/resizer/tBrdwbrvgidpsKh_Ph7mP8G3xfI=/932x582/arc-anglerfish-eu-central-1-prod-leparisien.s3.amazonaws.com/public/LQXOUZOYVR3OEUF7C7VN4YYAQA.jpg',
        'https://resize.programme-television.ladmedia.fr/r/670,670/img/var/premiere/storage/images/tele-7-jours/news-tv/fete-de-la-musique-france-2-le-concert-se-deroulera-dans-une-salle-vide-4658609/99143540-1-fre-FR/Fete-de-la-musique-France-2-le-concert-se-deroulera-dans-une-salle-vide.jpg','https://cdn-s-www.bienpublic.com/images/5AF226C3-D5DD-477A-9BDF-A9D347258BF2/NW_raw/le-zenith-de-dijon-en-configuration-concert-mais-encore-vide-photos-j-y-r-1571923103.jpg','https://i1.wp.com/assodarkroom.fr/wp-content/uploads/2011/11/olympia-salle.jpg?resize=576%2C256'];
        
        
        foreach($urlsRoom as $urlRoom){
            $image = new Image();
            $image->setUrl($urlRoom);

            $manager->persist($image);
            $manager->flush();
            $imagesRoom[] = $image;
        }
        $manager->flush();   

        //On ajout 15 salles disponibles
        for ($i = 0; $i < 5; $i++) {
            $description = $faker->sentence(50, true);
            $room = new Room();
            $room->setName($faker->name)
            ->setNumberSeat(108)
            ->setLatitude($coo[$i][0])
            ->setLongitude($coo[$i][1])
            ->setCity($locationList[$i])
            ->setCreatedAt($faker->dateTimeBetween('-60 month', '-1 month', 'Europe/Paris'))
            ->setAddress($faker->address)
            ->setCodePostal($faker->postcode)
            ->setDescription($description)
            ->setImage($faker->randomElement($imagesRoom));
            
            $this->addReference(Room::class . '_' . $i, $room);
            $manager->flush();
       }
    }

    public function getOrder() {
        return 6;
    }

}
