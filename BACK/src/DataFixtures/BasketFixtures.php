<?php

namespace App\DataFixtures;

use App\Entity\Basket;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class BasketFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        for ($i = 0; $i < 50; $i++) {

            $basket = new Basket();
            $basket->setDate($faker->dateTimeBetween('+1 month', '+6 month', 'Europe/Paris'))
            ->setTotalAmount($faker->randomFloat());

            $this->addReference(Basket::class . '_' . $i, $basket);
            
            $manager->persist($basket);
       }
       $manager->flush();
    }

    public function getOrder() {
        return 13;
    }
}
