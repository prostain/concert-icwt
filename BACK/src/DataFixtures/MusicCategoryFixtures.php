<?php

namespace App\DataFixtures;

use App\Entity\Group;
use App\Entity\MusicCategory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Faker\Factory;

class MusicCategoryFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        $musicCategoryList = ['Pop','Rock','Electro','Rap','Soul','Classique','Reggae', 'World'];

        //On boucle sur les types de musique
        for ($i = 0; $i < count($musicCategoryList); $i++) {

            $musicCategory = new MusicCategory();
            $musicCategory->setName($musicCategoryList[$i]);

            $manager->persist($musicCategory);
            $this->addReference(MusicCategory::class . '_' . $i, $musicCategory);
        }
        //On affecte les musiques aux groupes
        for ($i = 0; $i < 50; $i++) {
            $mC = $this->getReference(MusicCategory::class.'_'.$faker->numberBetween(0, count($musicCategoryList)-1));
            $mC->addGroup($this->getReference(Group::class.'_'.$i));
            $manager->persist($mC);
        }
        
        $manager->flush();

        
    }

    public function getOrder() {
        return 7;
    }
}
