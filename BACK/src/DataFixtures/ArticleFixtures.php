<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\ArticleCategory;
use App\Entity\Image;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ArticleFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        //On ajoute une image d'article
        $imagesArticle = [];
        $urls = ['https://congres.maisondelachimie.com/wp-content/uploads/2015/03/medias-salle-008-02.jpg',
        'https://resize.programme-television.ladmedia.fr/r/670,670/img/var/premiere/storage/images/tele-7-jours/news-tv/fete-de-la-musique-france-2-le-concert-se-deroulera-dans-une-salle-vide-4658609/99143540-1-fre-FR/Fete-de-la-musique-France-2-le-concert-se-deroulera-dans-une-salle-vide.jpg','https://cdn-s-www.bienpublic.com/images/5AF226C3-D5DD-477A-9BDF-A9D347258BF2/NW_raw/le-zenith-de-dijon-en-configuration-concert-mais-encore-vide-photos-j-y-r-1571923103.jpg','https://i1.wp.com/assodarkroom.fr/wp-content/uploads/2011/11/olympia-salle.jpg?resize=576%2C256','https://statics.lesinrocks.com/content/thumbs/uploads/2018/09/width-1125-height-612-quality-10/stones.jpg','https://toutelaculture.com/wp-content/uploads/2020/07/concert-2_0-525x349-1.jpg','https://img.huffingtonpost.com/asset/5f1a00b8270000b10fe674d7.jpeg?cache=vM0RbxN9YX&ops=scalefit_630_noupscale',
        'https://www.01net.com/i/0/0/a12/23c69056808bee716ff7a67f12e0c.jpeg','https://auxerrexpo.com/wp-content/uploads/2019/01/club-1-1200x675.jpg','https://cdn.radiofrance.fr/s3/cruiser-production/2020/06/44596e32-e897-4e91-94b0-0b7c3339f7d1/870x489_95438567_1653751264748526_5788465361988878336_n.jpg',
        'https://static.latribune.fr/full_width/978929/printemps-de-bourges-2017.jpg','https://www.toutsurmesfinances.com/argent/wp-content/uploads/sites/3/2017/02/concert.jpg','https://photos.lci.fr/images/1280/720/concert-discotheque-club-f0add5-0@1x.jpeg','https://media3.woopic.com/api/v1/images/93%2Fwebedia-musique-news%2F9b9%2Fe67%2F98c1fa8215934e01c920c73f49%2F3-raisons-qui-font-de-chuck-berry-le-vrai-king-du-rock-n-roll%7C1345564-chuck-berry-en-concert-a-saint-louis-le-orig-1.jpg?format=470x264&facedetect=1&quality=85',
        'https://www.telerama.fr/sites/tr_master/files/styles/simplecrop1000/public/gettyimages-1080635794_0.jpg?itok=8zdpyUu0','https://images-na.ssl-images-amazon.com/images/I/71IU1fKhjgL._SL1200_.jpg','https://metalshockfinland.files.wordpress.com/2011/11/deathstars.jpg?w=640',
        'https://i.pinimg.com/originals/73/e8/3f/73e83f12e86cb76853bbc87b75de7909.jpg','https://www.telerama.fr/sites/tr_master/files/styles/simplecrop1000/public/medias/2016/04/media_140820/wanted-posse-l-histoire-sans-fin-d-une-legende-de-la-danse-hip-hop%2CM324737.jpg?itok=A_CCdRm4',
        'https://s3-eu-west-1.amazonaws.com/images.linnlive.com/fe50132e61c7590af0f3485de98353dd/36a5d1cd-f5dd-44f2-bc54-5148721212d7.jpg','https://i0.wp.com/www.starmag.com/wp-content/uploads/2019/03/lady-gaga-a-star-is-born-premiere-sept-24-2018-ap-billboard-u-1548.jpg?resize=1200%2C675&ssl=1','https://www.cadre-dirigeant-magazine.com/wp-content/uploads/2020/04/lapresse.ca-cr%C3%A9dit-photo.jpg',
        'https://resize-parismatch.lanmedia.fr/img/var/news/storage/images/paris-match/people/la-star-fete-ses-50-ans-bon-anniversaire-david-guetta-1388213/23011235-1-fre-FR/La-star-fete-ses-50-ans-bon-anniversaire-David-Guetta.jpg','https://revrse.fr/wp-content/uploads/2017/10/wp1911081.jpg','https://intrld.com/wp-content/uploads/2019/12/juice-wrld.jpg','https://i1.sndcdn.com/artworks-000586663982-wcvyfw-t500x500.jpg','https://cdn-www.konbini.com/fr/images/files/2020/03/metallica-2016.jpg'];            

        foreach($urls as $url){
            $image = new Image();
            $image->setUrl($url);
            $manager->persist($image);
            $imagesArticle[] = $image;
        }
        $manager->flush();

        //On ajoute 50 articles
        for ($i = 0; $i < 500; $i++) {
            $image = new Image();
            $image->setUrl($faker->randomElement($urls));
            $manager->persist($image);
            $description = $faker->sentence(10, true);

            $article = new Article();
            $article->setDate($faker->dateTimeBetween('-100 days', '-1 days', 'Europe/Paris'))
            ->setTitle($faker->sentence(6, true))
            ->setDescription($description)
            ->setImage($image);

            $this->addReference(Article::class . '_' . $i, $article);
            $manager->persist($article);
       }
       $manager->flush();
    }
    public function getOrder() {
        return ;
    }

}
