<?php

namespace App\DataFixtures;

use App\Entity\Order;
use App\Entity\Booking;
use App\Entity\Role;
use App\Entity\User;
use App\Entity\Article;
use App\Entity\Basket;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;


class UserFixtures extends Fixture implements OrderedFixtureInterface
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        $userRole = new Role();
        $userRole->setName('ROLE_USER');
        $manager->persist($userRole);

        $adminRole = new Role();
        $adminRole->setName('ROLE_ADMIN');
        $manager->persist($adminRole);

        $manager->flush();

        //Liste des types de bâtiment
        $accomodationTypeList = ['Maison','Résidence','Immeuble'];

        //Liste civités
        $civilityList = ['Madame','Monsieur'];

        //Liste des différents pays
        $countryList = ['France','USA','Algérie','Canada','Suisse','Espagne','Italie','Japon','Chine','Corée du Sud','Maroc','Tunisie','Allemagne'];

        //on récupère les commandes

        for ($i=0; $i < 200; $i++) {
            $user = new User();
            $user->setPseudo($faker->userName)
            ->setPassword($this->encoder->encodePassword($user,'engage'.$i))
            ->setEmail($i.''.$faker->safeEmail)
            ->setCivility($faker->randomElement($civilityList))
            ->setLastname($faker->lastName)
            ->setFirstname($faker->firstName)
            ->setAddress($faker->streetAddress)
            ->setAccomodationType($faker->sentence(12, true))
            ->setAdditionalAddress($faker->streetName)
            ->setCodePostal($faker->countryCode)
            ->setCity($faker->city)
            ->setCountry($faker->randomElement($countryList))
            ->setPhone($faker->phoneNumber)
            ->setDateBirthday($faker->dateTimeBetween('-100 year', '-12 year', 'Europe/Paris'))
            ->addUserRole($userRole)
            ->addArticle($this->getReference(Article::class.'_'.$faker->numberBetween(0, 49)))
            ->addOrder($this->getReference(Order::class.'_'.$faker->numberBetween(0, 349)))
            ->addBasket($this->getReference(Basket::class.'_'.$faker->numberBetween(0, 49)));

            $manager->persist($user);
            $this->addReference(User::class . '_' . $i, $user);
            $manager->flush();
        }

        $user = new User();
        $user->setPseudo('admin-icwt')
        ->setPassword($this->encoder->encodePassword($user,'admin1234'))
        ->setEmail('admin@admin.com')
        ->setCivility('Madame')
        ->setLastname('Admin')
        ->setFirstname('Admin')
        ->setAddress('Ynov près de Casino')
        ->setAccomodationType('Bâtiment')
        ->setCodePostal('13100')
        ->setCity('Aix-en-Provence')
        ->setCountry('France')
        ->setPhone($faker->phoneNumber)
        ->setDateBirthday($faker->dateTimeBetween('-25 year', '-20 year', 'Europe/Paris'))
        ->addUserRole($userRole)
        ->addUserRole($adminRole);

        $manager->persist($user);
        $manager->flush();

        $user = new User();
        $user->setPseudo('icwt')
        ->setPassword($this->encoder->encodePassword($user,'user1234'))
        ->setEmail('user@user.com')
        ->setCivility('Madame')
        ->setLastname('User')
        ->setFirstname('User')
        ->setAddress('Ynov près de Casino')
        ->setAccomodationType('Bâtiment')
        ->setCodePostal('13100')
        ->setCity('Aix-en-Provence')
        ->setCountry('France')
        ->setPhone($faker->phoneNumber)
        ->setDateBirthday($faker->dateTimeBetween('-25 year', '-20 year', 'Europe/Paris'))
        ->addUserRole($userRole);

        $manager->persist($user);
        $manager->flush();
    }

    public function getOrder() {
        return 14;
    }
}
