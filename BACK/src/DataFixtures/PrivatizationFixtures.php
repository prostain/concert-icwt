<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\Privatization;
use App\Entity\Room;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

use Faker\Factory;

class PrivatizationFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        //On ajoute 5 privatizations
        for ($i = 0; $i < 5; $i++) {

            $privatizations = new Privatization();
            $privatizations->setCompany($faker->name)
            ->setFirtsnameUser($faker->firstName)
            ->setLastnameUser($faker->lastName)
            ->setPhone($faker->phoneNumber)
            ->setEmail($faker->safeEmail)
            ->setHour($faker->time('H:i:s','now'))
            ->setNumberOfPerson($faker->numberBetween(10,108))
            ->setBudget($faker->numberBetween(500, 100000))
            ->setDate($faker->dateTimeBetween('-60 month', '-1 month', 'Europe/Paris'))
            ->setRoom($this->getReference(Room::class . '_' .  $faker->numberBetween(0, 4)))
            ->setPerson($this->getReference(User::class . '_' .  $faker->numberBetween(0, 199)))
            ->setDescriptionProject($faker->sentence(333, true));

            $manager->persist($privatizations);
            $this->addReference(Privatization::class . '_' . $i, $privatizations);
       }
    }

        public function getOrder() {
        return 15;
    }
}
