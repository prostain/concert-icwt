<?php

namespace App\DataFixtures;

use App\Entity\Order;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class OrderFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        //On ajoute 350 commandes
        for ($i = 0; $i < 350; $i++) {

            $order = new Order();
            $order->setNumber($faker->randomNumber())
            ->setDate($faker->dateTimeBetween('-60 month', '-1 month', 'Europe/Paris'));

            $manager->persist($order);
            $this->addReference(Order::class . '_' . $i, $order);
            $manager->flush();
       }
    }

    public function getOrder() {
        return 3;
    }
}
