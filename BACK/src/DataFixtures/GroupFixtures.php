<?php

namespace App\DataFixtures;

use App\Entity\Group;
use App\Entity\Concert;
use App\Entity\Image;
use App\Entity\MusicCategory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Faker\Factory;

class GroupFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        //On ajoute des images de groupe
        $imagesGroup = [];
        $urls = ['https://cdn-s-www.ledauphine.com/images/D3CBC673-0F18-439E-B558-8D1875B51B60/NW_raw/le-groupe-ardechois-de-musique-celtique-ardech-celtique-sera-en-concert-mercredi-14-aout-place-de-la-liberation-photo-dr-donatien-gnakli-zebi-1564667630.jpg',
        'https://img.20mn.fr/k39EToafQqOmK38qWNxosw/830x532_kiss.jpg','https://phenixwebtv.files.wordpress.com/2019/11/le-groupe-eiffel-acc80-la-cigale-1.jpeg',
        'https://img.20mn.fr/fEVZk9DoSLGs57UmMo57fQ/830x532_groupe-iam-concert-paris-juillet-2017.jpg', 'https://intrld.com/wp-content/uploads/2018/07/xxxtentacion.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/5/5a/Ozma_French_Jazz_Band.jpg', 'https://sortirahaguenau.fr/site/wp-content/uploads/2020/01/2-1.jpg', 'https://www.placeminute.com/images/upload/photos/2019/07/21/unnamed-5086.jpg', 'https://i.f1g.fr/media/eidos/orig/2019/05/22/XVM5e600f5e-7b0e-11e9-8edc-eb5358b45745-300x200.jpg'];


        foreach($urls as $url){
            $image = new Image();
            $image->setUrl($url);
            $manager->persist($image);
            $imagesGroup[] = $image;
            $manager->flush();
        }
        $nbImages = count($imagesGroup) -1;
        
        $musicCategory = new MusicCategory();
        $musicCategory->setName('Funk');


        //On ajoute 50 artistes/groupes
        for ($i = 0; $i < 50; $i++) {
            $description = $faker->sentence(100, true);
            $group = new Group();
            $group->setName($faker->name)
            ->setDescription($description)
            ->setMusicCategory($musicCategory)
            ->setImage($imagesGroup[$faker->numberBetween(0, $nbImages)]);

            $manager->persist($group);
            $this->addReference(Group::class .'_'. $i, $group);
            $manager->flush();
       }

    }

    public function getOrder() {
        return 6;
    }
}
