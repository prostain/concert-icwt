<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\PrivatizationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *      attributes={
 *          "order"={"id":"ASC"},
 *      },
 *      paginationItemsPerPage=5,
 *      normalizationContext={"groups"={"read:privatizations"}},
 *      collectionOperations={"GET","POST"},
 *      itemOperations={"GET","DELETE","PUT"}
 * )
 * @ORM\Entity(repositoryClass=PrivatizationRepository::class)
 */
class Privatization
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"read:privatizations"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     * @Groups({"read:privatizations"})
     */
    private $lastnameUser;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"read:privatizations"})
     */
    private $firtsnameUser;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"read:privatizations"})
     */
    private $company;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"read:privatizations"})
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"read:privatizations"})
     */
    private $phone;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"read:privatizations"})
     */
    private $numberOfPerson;

    /**
     * @ORM\Column(type="text", length=1000, nullable=true)
     * @Groups({"read:privatizations"})
     */
    private $descriptionProject;


    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"read:privatizations"})
     */
    private $date;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"read:privatizations"})
     */
    private $budget;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"read:privatizations"})
     */
    private $hour;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="rentals",cascade={"persist"})
     */
    private $person;

    /**
     * @ORM\ManyToOne(targetEntity=Room::class, inversedBy="privatizations",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $room;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getLastnameUser(): ?string
    {
        return $this->lastnameUser;
    }

    public function setLastnameUser(string $lastnameUser): self
    {
        $this->lastnameUser = $lastnameUser;

        return $this;
    }

    public function getFirtsnameUser(): ?string
    {
        return $this->firtsnameUser;
    }

    public function setFirtsnameUser(string $firtsnameUser): self
    {
        $this->firtsnameUser = $firtsnameUser;

        return $this;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(?string $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getNumberOfPerson()
    {
        return $this->numberOfPerson;
    }

    public function setNumberOfPerson($numberOfPerson)
    {
        $this->numberOfPerson = $numberOfPerson;

        return $this;
    }

    public function getDescriptionProject(): ?string
    {
        return $this->descriptionProject;
    }

    public function setDescriptionProject(string $descriptionProject): self
    {
        $this->descriptionProject = $descriptionProject;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getBudget(): ?float
    {
        return $this->budget;
    }

    public function setBudget(float $budget): self
    {
        $this->budget = $budget;

        return $this;
    }

    public function getHour(): ?string
    {
        return $this->hour;
    }

    public function setHour(string $hour): self
    {
        $this->hour = $hour;

        return $this;
    }

    public function getPerson(): ?User
    {
        return $this->person;
    }

    public function setPerson(?User $person): self
    {
        $this->person = $person;

        return $this;
    }

    public function getRoom(): ?Room
    {
        return $this->room;
    }

    public function setRoom(?Room $room): self
    {
        $this->room = $room;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }
}
