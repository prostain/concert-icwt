<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\BookingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *      attributes={
 *          "order"={"id":"ASC"},
 *      },
 *      paginationItemsPerPage=5,
 *      normalizationContext={"groups"={"read:bookings"}},
 *      collectionOperations={"GET","POST"},
 *      itemOperations={"GET","DELETE","PUT"}
 * )  
 * @ORM\Entity(repositoryClass=BookingRepository::class)
 */
class Booking
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"read:bookings"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read:bookings"})
     */
    private $numero;


    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"read:bookings"})
     */
    private $numberPersonRestaurant;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"read:bookings"})
     */
    private $numberSeatParking;

    /**
     * @ORM\Column(type="float")
     * @Groups({"read:bookings"})
     */
    private $price;

    /**
     * @ORM\Column(type="float")
     * @Groups({"read:bookings"})
     */
    private $priceInsurance;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"read:bookings"})
     */
    private $hasInsuranceCancellation;

    /**
     * @ORM\ManyToOne(targetEntity=Basket::class, inversedBy="reservations",cascade={"persist"})
     * @Groups({"read:bookings"})
     */
    private $basket;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="reservations",cascade={"persist"})
     * @Groups({"read:bookings"})
     */
    private $person;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"read:bookings"})
     */
    private $comment;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"read:bookings"})
     */
    private $hasBookRestaurant;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"read:bookings"})
     */
    private $hasBookParking;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default" : 0})
     */
    private $status;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date;

    /**
     * @ORM\OneToMany(targetEntity=BookingLine::class, mappedBy="booking", orphanRemoval=true)
     */
    private $bookingLines;

    public function __construct()
    {
        $this->bookingLines = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumero(): ?string
    {
        return $this->numero;
    }

    public function setNumero(string $numero): self
    {
        $this->numero = $numero;

        return $this;
    }

    public function getNumberPersonRestaurant()
    {
        return $this->numberPersonRestaurant;
    }

    public function setNumberPersonRestaurant($numberPersonRestaurant)
    {
        $this->numberPersonRestaurant = $numberPersonRestaurant;

        return $this;
    }

    public function getNumberSeatParking()
    {
        return $this->numberSeatParking;
    }

    public function setNumberSeatParking($numberSeatParking)
    {
        $this->numberSeatParking = $numberSeatParking;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getPriceInsurance(): ?float
    {
        return $this->priceInsurance;
    }

    public function setPriceInsurance(float $priceInsurance): self
    {
        $this->priceInsurance = $priceInsurance;

        return $this;
    }

    public function getHasInsuranceCancellation(): ?bool
    {
        return $this->hasInsuranceCancellation;
    }

    public function setHasInsuranceCancellation(bool $hasInsuranceCancellation): self
    {
        $this->hasInsuranceCancellation = $hasInsuranceCancellation;

        return $this;
    }

    public function getBasket(): ?Basket
    {
        return $this->basket;
    }

    public function setBasket(?Basket $basket): self
    {
        $this->basket = $basket;

        return $this;
    }

    public function getPerson(): ?User
    {
        return $this->person;
    }

    public function setPerson(?User $person): self
    {
        $this->person = $person;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getHasBookRestaurant(): ?bool
    {
        return $this->hasBookRestaurant;
    }

    public function setHasBookRestaurant(bool $hasBookRestaurant): self
    {
        $this->hasBookRestaurant = $hasBookRestaurant;

        return $this;
    }

    public function getHasBookParking(): ?bool
    {
        return $this->hasBookParking;
    }

    public function setHasBookParking(bool $hasBookParking): self
    {
        $this->hasBookParking = $hasBookParking;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return Collection|BookingLine[]
     */
    public function getBookingLines(): Collection
    {
        return $this->bookingLines;
    }

    public function addBookingLine(BookingLine $bookingLine): self
    {
        if (!$this->bookingLines->contains($bookingLine)) {
            $this->bookingLines[] = $bookingLine;
            $bookingLine->setBooking($this);
        }

        return $this;
    }

    public function removeBookingLine(BookingLine $bookingLine): self
    {
        if ($this->bookingLines->removeElement($bookingLine)) {
            // set the owning side to null (unless already changed)
            if ($bookingLine->getBooking() === $this) {
                $bookingLine->setBooking(null);
            }
        }

        return $this;
    }
}
