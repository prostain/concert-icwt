<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\GroupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *      attributes={
 *          "order"={"id":"ASC"},
 *      },
 *      paginationItemsPerPage=5,
 *      normalizationContext={"groups"={"read:groups","read:concerts"}},
 *      collectionOperations={"GET","POST"},
 *      itemOperations={"GET","DELETE","PUT"}
 * )
 * @ORM\Entity(repositoryClass=GroupRepository::class)
 * @ORM\Table(name="`group`")
 */
class Group
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"read:groups","read:concerts"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read:groups","read:concerts"})
     */
    private $name;

    /**
     * @ORM\Column(type="text", length=1000)
     * @Groups({"read:groups","read:concerts"})
     */
    private $description;


    /**
     * @ORM\ManyToOne(targetEntity=MusicCategory::class, inversedBy="groups",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"read:groups","read:concerts"})
     */
    private $musicCategory;

    /**
     * @ORM\ManyToOne(targetEntity=Image::class, inversedBy="groupes",cascade={"persist"})
     * @Groups({"read:groups","read:concerts"})
     */
    private $image;

    /**
     * @ORM\OneToMany(targetEntity=Concert::class, mappedBy="artist")
     */
    private $concerts;


    public function __construct()
    {
        $this->musicCategories = new ArrayCollection();
        $this->concerts = new ArrayCollection();
        $this->images = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }


    public function getMusicCategory(): ?MusicCategory
    {
        return $this->musicCategory;
    }

    public function setMusicCategory(?MusicCategory $musicCategory): self
    {
        $this->musicCategory = $musicCategory;

        return $this;
    }

    public function getImage(): ?Image
    {
        return $this->image;
    }

    public function setImage(?Image $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection|Concert[]
     */
    public function getConcerts(): Collection
    {
        return $this->concerts;
    }

    public function addConcert(Concert $concert): self
    {
        if (!$this->concerts->contains($concert)) {
            $this->concerts[] = $concert;
            $concert->setArtist($this);
        }

        return $this;
    }

    public function removeConcert(Concert $concert): self
    {
        if ($this->concerts->removeElement($concert)) {
            // set the owning side to null (unless already changed)
            if ($concert->getArtist() === $this) {
                $concert->setArtist(null);
            }
        }

        return $this;
    }
}
