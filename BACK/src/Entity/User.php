<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ApiResource(
 *      attributes={
 *          "order"={"id":"ASC"},
 *      },
 *      paginationItemsPerPage=5,
 *      normalizationContext={"groups"={"read:user"}},
 *      collectionOperations={"GET","POST"},
 *      itemOperations={"GET","PUT","DELETE"}
 * )
 * @ApiFilter(SearchFilter::class, properties={"post":"exact"})
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"read:users"})     
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read:users"})     
     */
    private $pseudo;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read:users"})     
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups({"read:users"})
     */

    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read:users"})     
     */
    private $civility;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read:users"})     
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read:users"})     
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=500)
     * @Groups({"read:users"})     
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read:users"})     
     */
    private $accomodationType;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"read:users"})     
     */
    private $additionalAddress;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read:users"})     
     */
    private $codePostal;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read:users"})     
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read:users"})     
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups({"read:users"})     
     */
    private $phone;

    /**
     * @ORM\ManyToMany(targetEntity=Article::class, inversedBy="likes", fetch="EAGER")
     * @Groups({"read:users"})     
     */
    private $likedArticles;

    /**
     * @ORM\ManyToMany(targetEntity=Article::class, inversedBy="shares", fetch="EAGER")
     * @ORM\JoinTable(name="user_shares")
     * @Groups({"read:users"})     
     */
    private $shares;

    /**
     * @ORM\OneToMany(targetEntity=Order::class, mappedBy="person")
     * @Groups({"read:users"})     
     */
    private $orders;

    /**
     * @ORM\OneToMany(targetEntity=Article::class, mappedBy="personArticle")
     * @Groups({"read:users"})     
     */
    private $articles;

    /**
     * @ORM\OneToOne(targetEntity=Newsletter::class, mappedBy="person")
     * @Groups({"read:users"})     
     */
    private $newsletter;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"read:users"})     
     */
    private $dateBirthday;

    /**
     * @ORM\OneToMany(targetEntity=Basket::class, mappedBy="person")
     */
    private $baskets;

    /**
     * @ORM\OneToMany(targetEntity=Privatization::class, mappedBy="person")
     */
    private $rentals;

    /**
     * @var array|string[]
     */
    private $roles = [];


    /**
     * @var Collection
     * @ORM\ManyToMany(targetEntity=Role::class, inversedBy="users")
     * @ORM\JoinTable(name="user_role")
     * @Groups({"read:users"})     
     */
    private $userRoles;


    public function __construct()
    {
        $this->userRoles = new ArrayCollection();
        $this->orders = new ArrayCollection();
        $this->person = new ArrayCollection();
        $this->articles = new ArrayCollection();
        $this->baskets = new ArrayCollection();
        $this->rentals = new ArrayCollection();
        $this->roles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    public function setPseudo(string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCivility(): ?string
    {
        return $this->civility;
    }

    public function setCivility(string $civility): self
    {
        $this->civility = $civility;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getAccomodationType(): ?string
    {
        return $this->accomodationType;
    }

    public function setAccomodationType(?string $accomodationType): self
    {
        $this->accomodationType = $accomodationType;

        return $this;
    }

    public function getAdditionalAddress(): ?string
    {
        return $this->additionalAddress;
    }

    public function setAdditionalAddress(?string $additionalAddress): self
    {
        $this->additionalAddress = $additionalAddress;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->codePostal;
    }

    public function setCodePostal(string $codePostal): self
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

/**
     * @return Collection|Article[]
     */
    public function getLikedArticles(): Collection
    {
        return $this->likedArticles;
    }

    public function addLikedArticle(Article $likedArticle): self
    {
        if (!$this->likedArticles->contains($likedArticle)) {
            $this->likedArticles[] = $likedArticle;
        }

        return $this;
    }

    public function removeLikedArticle(Article $likedArticle): self
    {
        $this->likedArticles->removeElement($likedArticle);

        return $this;
    }

    /**
     * @return array|string[]
     */
    public function getRoles(): array
    {
        $roles = $this->userRoles->map(function($role){
            return $role->getName();
        })->toArray();
        return array_unique($roles);
    }

    public function addUserRole(Role $userRole): self
    {
        if (!$this->userRoles->contains($userRole)) {
            $this->userRoles[] = $userRole;
            $userRole->addUser($this);
        }

        return $this;
    }

    public function removeRole(Role $userRole): self
    {
        $this->userRoles->removeElement($userRole);

        return $this;
    }

    /**
     * @return Collection|Article[]
     */
    public function getShares(): Collection
    {
        return $this->shares;
    }

    public function addShare(Article $share): self
    {
        if (!$this->shares->contains($share)) {
            $this->shares[] = $share;
        }

        return $this;
    }

    public function removeShare(Article $share): self
    {
        $this->shares->removeElement($share);

        return $this;
    }

        /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @return Collection|Order[]
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->setPerson($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        if ($this->orders->removeElement($order)) {
            // set the owning side to null (unless already changed)
            if ($order->getPerson() === $this) {
                $order->setPerson(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Article[]
     */
    public function getArticles(): Collection
    {
        return $this->articles;
    }

    public function addArticle(Article $article): self
    {
        if (!$this->articles->contains($article)) {
            $this->articles[] = $article;
            $article->setPersonArticle($this);
        }

        return $this;
    }

    public function removeArticle(Article $article): self
    {
        if ($this->articles->removeElement($article)) {
            // set the owning side to null (unless already changed)
            if ($article->getPersonArticle() === $this) {
                $article->setPersonArticle(null);
            }
        }

        return $this;
    }

    public function getNewsletter(): ?Newsletter
    {
        return $this->newsletter;
    }

    public function setNewsletter(?Newsletter $newsletter): self
    {
        // unset the owning side of the relation if necessary
        if ($newsletter === null && $this->newsletter !== null) {
            $this->newsletter->setPerson(null);
        }

        // set the owning side of the relation if necessary
        if ($newsletter !== null && $newsletter->getPerson() !== $this) {
            $newsletter->setPerson($this);
        }

        $this->newsletter = $newsletter;

        return $this;
    }

    public function getDateBirthday(): ?\DateTimeInterface
    {
        return $this->dateBirthday;
    }

    public function setDateBirthday(\DateTimeInterface $dateBirthday): self
    {
        $this->dateBirthday = $dateBirthday;

        return $this;
    }


    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        $this->plainPassword = null;
    }

    /**
     * @return Collection|Basket[]
     */
    public function getBaskets(): Collection
    {
        return $this->baskets;
    }

    public function addBasket(Basket $basket): self
    {
        if (!$this->baskets->contains($basket)) {
            $this->baskets[] = $basket;
            $basket->setPerson($this);
        }

        return $this;
    }

    public function removeBasket(Basket $basket): self
    {
        if ($this->baskets->removeElement($basket)) {
            // set the owning side to null (unless already changed)
            if ($basket->getPerson() === $this) {
                $basket->setPerson(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Privatization[]
     */
    public function getRentals(): Collection
    {
        return $this->rentals;
    }

    public function addRental(Privatization $rental): self
    {
        if (!$this->rentals->contains($rental)) {
            $this->rentals[] = $rental;
            $rental->setPerson($this);
        }

        return $this;
    }

    public function removeRental(Privatization $rental): self
    {
        if ($this->rentals->removeElement($rental)) {
            // set the owning side to null (unless already changed)
            if ($rental->getPerson() === $this) {
                $rental->setPerson(null);
            }
        }

        return $this;
    }

    public function addRole(Role $role): self
    {
        if (!$this->roles->contains($role)) {
            $this->roles[] = $role;
        }

        return $this;
    }
}
