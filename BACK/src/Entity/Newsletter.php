<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\NewsletterRepository;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *      attributes={
 *          "order"={"date":"DESC"},
 *      },
 *      paginationItemsPerPage=5,
 *      normalizationContext={"groups"={"read:newsletters"}},
 *      collectionOperations={"GET","POST"},
 *      itemOperations={}
 * )
 * @ApiFilter(SearchFilter::class, properties={"post":"exact"})
 * @ORM\Entity(repositoryClass=NewsletterRepository::class)
 */
class Newsletter
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"read:newsletters"})
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"read:newsletters"})
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read:newsletters"})
     */
    private $email;

    /**
     * @ORM\OneToOne(targetEntity=User::class, inversedBy="newsletter")
     * @Groups({"read:newsletters"})
     */
    private $person;

    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getDate()
    {
        return $this->date;
    }

    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPerson(): ?User
    {
        return $this->person;
    }

    public function setPerson(?User $person): self
    {
        $this->person = $person;

        return $this;
    }
}
