<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\RoomRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *      attributes={
 *          "order"={"id":"ASC"},
 *      },
 *      paginationItemsPerPage=5,
 *      normalizationContext={"groups"={"read:rooms"}},
 *      collectionOperations={"GET","POST"},
 *      itemOperations={"GET","DELETE","PUT"}
 * )   
 * @ORM\Entity(repositoryClass=RoomRepository::class)
 */
class Room
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"read:rooms","read:concerts"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read:rooms","read:concerts"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     * @Groups({"read:rooms","read:concerts"})
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"read:rooms","read:concerts"})
     */
    private $numberSeat;

    /**
     * @ORM\Column(type="string", length=24)
     * @Groups({"read:rooms","read:concerts"})
     */
    private $latitude;

    /**
     * @ORM\Column(type="string", length=24)
     * @Groups({"read:rooms","read:concerts"})
     */
    private $longitude;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"read:rooms","read:concerts"})
     */
    private $created_at;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read:rooms","read:concerts"})
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read:rooms","read:concerts"})
     */
    private $codePostal;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read:rooms","read:concerts"})
     */
    private $city;

    /**
     * @ORM\ManyToOne(targetEntity=Image::class, inversedBy="rooms",cascade={"persist"})
     * @Groups({"read:rooms","read:concerts"})
     */
    private $image;

    /**
     * @ORM\OneToMany(targetEntity=Privatization::class, mappedBy="room", orphanRemoval=true)
     */
    private $privatizations;

    /**
     * @ORM\OneToMany(targetEntity=Concert::class, mappedBy="room", orphanRemoval=true)
     */
    private $concerts;


    public function __construct()
    {
        $this->privatizations = new ArrayCollection();
        $this->concerts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getNumberSeat()
    {
        return $this->numberSeat;
    }

    public function setNumberSeat($numberSeat)
    {
        $this->numberSeat = $numberSeat;

        return $this;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function setLatitude(string $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function setLongitude(string $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->codePostal;
    }

    public function setCodePostal(string $codePostal): self
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getImage(): ?Image
    {
        return $this->image;
    }

    public function setImage(?Image $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection|Privatization[]
     */
    public function getPrivatizations(): Collection
    {
        return $this->privatizations;
    }

    public function addPrivatization(Privatization $privatization): self
    {
        if (!$this->privatizations->contains($privatization)) {
            $this->privatizations[] = $privatization;
            $privatization->setRoom($this);
        }

        return $this;
    }

    public function removePrivatization(Privatization $privatization): self
    {
        if ($this->privatizations->removeElement($privatization)) {
            // set the owning side to null (unless already changed)
            if ($privatization->getRoom() === $this) {
                $privatization->setRoom(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Concert[]
     */
    public function getConcerts(): Collection
    {
        return $this->concerts;
    }

    public function addConcert(Concert $concert): self
    {
        if (!$this->concerts->contains($concert)) {
            $this->concerts[] = $concert;
            $concert->setRoom($this);
        }

        return $this;
    }

    public function removeConcert(Concert $concert): self
    {
        if ($this->concerts->removeElement($concert)) {
            // set the owning side to null (unless already changed)
            if ($concert->getRoom() === $this) {
                $concert->setRoom(null);
            }
        }

        return $this;
    }
}
