<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\ConcertRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *      attributes={
 *          "order"={"id":"ASC"},
 *          "pagination_enabled"=true,
 *          "paginationItemsPerPage"=10,
 *      },
 *      normalizationContext={"groups"={"read:concerts"}},
 *      collectionOperations={"GET","POST"},
 *      itemOperations={"GET","DELETE","PUT"}
 * )
 * @ApiFilter(SearchFilter::class, properties={"post":"exact"})
 * @ORM\Entity(repositoryClass=ConcertRepository::class)
 */
class Concert
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"read:concerts"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read:concerts"})
     */
    private $tourName;

    /**
     * @ORM\Column(type="string")
     * @Groups({"read:concerts"})
     */
    private $date;

    /**
     * @ORM\Column(type="float")
     * @Groups({"read:concerts"})
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read:concerts"})
     */
    private $location;

    /**
     * @ORM\Column(type="text", length=1000)
     * @Groups({"read:concerts"})
     */
    private $description;



    /**
     * @ORM\ManyToMany(targetEntity=Order::class, inversedBy="concerts")
     * @Groups({"read:concerts"})
     */
    private $orders;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read:concerts"})
     */
    private $hour;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read:concerts"})
     */
    private $hourOpening;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"read:concerts"})
     */
    private $decliningRatePercentage;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default" : 0})
     * @Groups({"read:concerts"})
     */
    private $hasParking;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default" : 0})
     * @Groups({"read:concerts"})
     */
    private $hasRestaurant;

    /**
     * @ORM\ManyToOne(targetEntity=Image::class, inversedBy="concerts")
     * @Groups({"read:concerts"})
     */
    private $image;

    /**
     * @ORM\ManyToOne(targetEntity=Group::class, inversedBy="concerts",cascade={"persist"})
     * @Groups({"read:concerts"})
     */
    private $artist;

    /**
     * @ORM\OneToMany(targetEntity=BookingLine::class, mappedBy="concert", orphanRemoval=true)
     */
    private $bookingLines;

    /**
     * @ORM\Column(type="string", length=254, nullable=true)
     */
    private $tour_reference;

    /**
     * @ORM\ManyToMany(targetEntity=TypeTicket::class, mappedBy="concerts")
     */
    private $typeTickets;

    /**
     * @ORM\ManyToOne(targetEntity=Room::class, inversedBy="concerts",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"read:concerts"})
     */
    private $room;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default" : 1})
     * @Groups({"read:concerts"})
     */
    private $priceCategories;


    public function __construct()
    {
        $this->musicCategories = new ArrayCollection();
        $this->orders = new ArrayCollection();
        $this->bookingConcertTickets = new ArrayCollection();
        $this->bookingLines = new ArrayCollection();
        $this->typeTickets = new ArrayCollection();

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTourName(): ?string
    {
        return $this->tourName;
    }

    public function setTourName(string $tourName): self
    {
        $this->tourName = $tourName;

        return $this;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }


    /**
     * @return Collection|Order[]
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
        }

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        $this->orders->removeElement($order);

        return $this;
    }

    public function getHour(): ?string
    {
        return $this->hour;
    }

    public function setHour(string $hour): self
    {
        $this->hour = $hour;

        return $this;
    }

    public function getHourOpening(): ?string
    {
        return $this->hourOpening;
    }

    public function setHourOpening(string $hourOpening): self
    {
        $this->hourOpening = $hourOpening;

        return $this;
    }

    public function getDecliningRatePercentage(): ?float
    {
        return $this->decliningRatePercentage;
    }

    public function setDecliningRatePercentage(?float $decliningRatePercentage): self
    {
        $this->decliningRatePercentage = $decliningRatePercentage;

        return $this;
    }

    public function getHasParking(): ?int
    {
        return $this->hasParking;
    }

    public function setHasParking(int $hasParking): self
    {
        $this->hasParking = $hasParking;

        return $this;
    }

    public function getHasRestaurant(): ?int
    {
        return $this->hasRestaurant;
    }

    public function setHasRestaurant(int $hasRestaurant): self
    {
        $this->hasRestaurant = $hasRestaurant;

        return $this;
    }

    public function getImage(): ?Image
    {
        return $this->image;
    }

    public function setImage(?Image $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getArtist(): ?Group
    {
        return $this->artist;
    }

    public function setArtist(?Group $artist): self
    {
        $this->artist = $artist;

        return $this;
    }

    /**
     * @return Collection|BookingLine[]
     */
    public function getBookingLines(): Collection
    {
        return $this->bookingLines;
    }

    public function addBookingLine(BookingLine $bookingLine): self
    {
        if (!$this->bookingLines->contains($bookingLine)) {
            $this->bookingLines[] = $bookingLine;
            $bookingLine->setConcert($this);
        }

        return $this;
    }

    public function removeBookingLine(BookingLine $bookingLine): self
    {
        if ($this->bookingLines->removeElement($bookingLine)) {
            // set the owning side to null (unless already changed)
            if ($bookingLine->getConcert() === $this) {
                $bookingLine->setConcert(null);
            }
        }

        return $this;
    }

    public function getTourReference(): ?string
    {
        return $this->tour_reference;
    }

    public function setTourReference(?string $tour_reference): self
    {
        $this->tour_reference = $tour_reference;

        return $this;
    }

    /**
     * @return Collection|TypeTicket[]
     */
    public function getTypeTickets(): Collection
    {
        return $this->typeTickets;
    }

    public function addTypeTicket(TypeTicket $typeTicket): self
    {
        if (!$this->typeTickets->contains($typeTicket)) {
            $this->typeTickets[] = $typeTicket;
            $typeTicket->addConcert($this);
        }

        return $this;
    }

    public function removeTypeTicket(TypeTicket $typeTicket): self
    {
        if ($this->typeTickets->removeElement($typeTicket)) {
            $typeTicket->removeConcert($this);
        }

        return $this;
    }

    public function getRoom(): ?Room
    {
        return $this->room;
    }

    public function setRoom(?Room $room): self
    {
        $this->room = $room;

        return $this;
    }
    

    public function getPriceCategories(): ?int
    {
        return $this->priceCategories;
    }

    public function setPriceCategories(int $priceCategories): self
    {
            $this->priceCategories = $priceCategories;


        return $this;
    }

}
