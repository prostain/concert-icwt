<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\ArticleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *      attributes={
 *          "paginationItemsPerPage"=16,
 *          "order"={"id":"ASC"},
 *      },
 *      normalizationContext={"groups"={"read:articles"}},
 *      collectionOperations={"GET","POST"},
 *      itemOperations={"GET","DELETE","PUT"}
 * )
 * @ApiFilter(SearchFilter::class, properties={"post":"exact"})
 * @ORM\Entity(repositoryClass=ArticleRepository::class)
 */
class Article
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"read:articles"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read:articles"})
     */
    private $title;

    /**
     * @ORM\Column(type="text", length=1000)
     * @Groups({"read:articles"})
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"read:articles"})
     */
    private $date;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, mappedBy="likedArticles", fetch="EAGER")
     * @Groups({"read:articles"})
     */
    private $likes;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, mappedBy="shares")
     * @ORM\JoinTable(name="user_shares")
     * @Groups({"read:articles"})
     */
    private $shares;

    /**
     * @ORM\ManyToOne(targetEntity=ArticleCategory::class, inversedBy="articles",cascade={"persist"})
     * @Groups({"read:articles"})
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="articles",cascade={"persist"})
     * @Groups({"read:articles"})
     */
    private $personArticle;

    /**
     * @ORM\ManyToOne(targetEntity=Image::class, inversedBy="articles",cascade={"persist"})
     * @Groups({"read:articles"})
     */
    private $image;

    public function __construct()
    {
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }


    /**
     * @return Collection|User[]
     */
    public function getLikes(): Collection
    {
        return $this->likes;
    }

    public function addLike(User $like): self
    {
        if (!$this->likes->contains($like)) {
            $this->likes[] = $like;
            $like->addLikedArticle($this);
        }

        return $this;
    }

    public function removeLike(User $like): self
    {
        if ($this->likes->removeElement($like)) {
            $like->removeLikedArticle($this);
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getShares(): Collection
    {
        return $this->shares;
    }

    public function addShare(User $share): self
    {
        if (!$this->shares->contains($share)) {
            $this->shares[] = $share;
            $share->addShare($this);
        }

        return $this;
    }

    public function removeShare(User $share): self
    {
        if ($this->shares->removeElement($share)) {
            $share->removeShare($this);
        }

        return $this;
    }

    public function getCategory(): ?ArticleCategory
    {
        return $this->category;
    }

    public function setCategory(?ArticleCategory $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getPersonArticle(): ?User
    {
        return $this->personArticle;
    }

    public function setPersonArticle(?User $personArticle): self
    {
        $this->personArticle = $personArticle;

        return $this;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    public function getImage(): ?Image
    {
        return $this->image;
    }

    public function setImage(?Image $image): self
    {
        $this->image = $image;

        return $this;
    }
}
