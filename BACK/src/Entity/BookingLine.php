<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\BookingLineRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=BookingLineRepository::class)
 */
class BookingLine
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $seat;

    /**
     * @ORM\ManyToOne(targetEntity=Concert::class, inversedBy="bookingLines",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $concert;

    /**
     * @ORM\ManyToOne(targetEntity=TypeTicket::class, inversedBy="bookingLines",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $typeTicket;

    /**
     * @ORM\ManyToOne(targetEntity=Booking::class, inversedBy="bookingLines",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $booking;

    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSeat(): ?int
    {
        return $this->seat;
    }

    public function setSeat(int $seat): self
    {
        $this->seat = $seat;

        return $this;
    }

    public function getConcert(): ?Concert
    {
        return $this->concert;
    }

    public function setConcert(?Concert $concert): self
    {
        $this->concert = $concert;

        return $this;
    }

    public function getTypeTicket(): ?TypeTicket
    {
        return $this->typeTicket;
    }

    public function setTypeTicket(?TypeTicket $typeTicket): self
    {
        $this->typeTicket = $typeTicket;

        return $this;
    }

    public function getBooking(): ?Booking
    {
        return $this->booking;
    }

    public function setBooking(?Booking $booking): self
    {
        $this->booking = $booking;

        return $this;
    }

}
