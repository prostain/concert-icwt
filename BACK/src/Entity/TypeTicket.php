<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\TypeTicketRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *      attributes={
 *          "order"={"id":"ASC"},
 *      },
 *      paginationItemsPerPage=5,
 *      normalizationContext={"groups"={"read:type_tickets"}},
 *      collectionOperations={"GET"},
 *      itemOperations={"GET"}
 * )    * @ORM\Entity(repositoryClass=TypeTicketRepository::class)
 */
class TypeTicket
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"read:type_tickets","read:concerts"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read:type_tickets","read:concerts"})
     */
    private $libelle;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"read:type_tickets","read:concerts"})
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"read:type_tickets","read:concerts"})
     */
    private $tagline;

    /**
     * @ORM\OneToMany(targetEntity=BookingLine::class, mappedBy="typeTicket", orphanRemoval=true)
     */
    private $bookingLines;

    /**
     * @ORM\ManyToMany(targetEntity=Concert::class, inversedBy="typeTickets")
     */
    private $concerts;


    public function __construct()
    {
        $this->bookingLines = new ArrayCollection();
        $this->concerts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getTagline(): ?string
    {
        return $this->tagline;
    }

    public function setTagline(?string $tagline): self
    {
        $this->tagline = $tagline;

        return $this;
    }

    /**
     * @return Collection|BookingLine[]
     */
    public function getBookingLines(): Collection
    {
        return $this->bookingLines;
    }

    public function addBookingLine(BookingLine $bookingLine): self
    {
        if (!$this->bookingLines->contains($bookingLine)) {
            $this->bookingLines[] = $bookingLine;
            $bookingLine->setTypeTicket($this);
        }

        return $this;
    }

    public function removeBookingLine(BookingLine $bookingLine): self
    {
        if ($this->bookingLines->removeElement($bookingLine)) {
            // set the owning side to null (unless already changed)
            if ($bookingLine->getTypeTicket() === $this) {
                $bookingLine->setTypeTicket(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Concert[]
     */
    public function getConcerts(): Collection
    {
        return $this->concerts;
    }

    public function addConcert(Concert $concert): self
    {
        if (!$this->concerts->contains($concert)) {
            $this->concerts[] = $concert;
        }

        return $this;
    }

    public function removeConcert(Concert $concert): self
    {
        $this->concerts->removeElement($concert);

        return $this;
    }
}
