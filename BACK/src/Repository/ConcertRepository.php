<?php

namespace App\Repository;

use App\Entity\Concert;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Tools\Pagination\Paginator as DoctrinePaginator;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Paginator;
use DateTime;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Concert|null find($id, $lockMode = null, $lockVersion = null)
 * @method Concert|null findOneBy(array $criteria, array $orderBy = null)
 * @method Concert[]    findAll()
 * @method Concert[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConcertRepository extends ServiceEntityRepository
{
    const ITEMS_PER_PAGE = 5;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Concert::class);
    }

    /**
     * @param integer $nbConcert
     * @return Concert[]
     */
    public function findLast($nbConcert)
    {
        return $this->createQueryBuilder('c')
            ->orderBy('c.date', 'DESC')
            ->setMaxResults($nbConcert)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param integer $page
     * @return Paginator
     */
    public function getConcertsWithPaging(int $page = 1): Paginator 
    {
        $firstResult = ($page -1) * self::ITEMS_PER_PAGE;
        $queryBuilder = $this->createQueryBuilder('c')
            ->orderBy('c.date', 'DESC');

        $criteria = Criteria::create()
            ->setFirstResult($firstResult)
            ->setMaxResults(self::ITEMS_PER_PAGE);
        $queryBuilder->addCriteria($criteria);

        $doctrinePaginator = new DoctrinePaginator($queryBuilder);
        $paginator = new Paginator($doctrinePaginator);

        return $paginator;
    }

        /**
     * @param integer $concertId
     * @return integer[]
     */
    public function getReservedPlaces(int $concertId)
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT bl.seat 
            FROM App\Entity\BookingLine bl
            WHERE bl.concert > :concertId
            ORDER BY bl.seat ASC'
        )->setParameter('concertId', $concertId);
        return $query->getResult(); 
    }

    /**
     * @param integer $concertId
     * @return integer[]
     */
    public function getConcertDetails(int $concertId)
    {
        $concert = $this->find($concertId);

        $qb = $this->createQueryBuilder('c');
        $qb->where('c.tour_reference = :ref')
        ->setParameter('ref', $concert->getTourReference());    
        return $qb->getQuery()
              ->getResult();


    }

     /**
      * @param string $location
      * @param integer $musicCategoryId
      * @param string $dateStart
      * @param string $dateEnd
      * @return void
      */
    public function getConcertsBySort(?string $location, ?int $musicCategoryId, ?string $dateStart, ?string $dateEnd) 
    {
        $query = $this->createQueryBuilder('c')->orderBy('c.date', 'DESC');

        if($musicCategoryId != null){
            $query->leftJoin('c.artist','g')
                ->leftJoin('g.musicCategory','m')
                ->where('m.id = :mcId')
                ->setParameter('mcId', $musicCategoryId);
        }

        if($location != null){
            $query->andWhere('c.location = :loc')->setParameter('loc', $location);
        }

        if ($dateStart != null && $dateEnd != null){
            $query->andWhere('c.date <= :e')
            ->andWhere('c.date >= :s')
            ->setParameter('e', $dateEnd)
            ->setParameter('s', $dateStart);
        }

        return $query->getQuery()->getResult();
    }

    /**
     * @param string $location
     */
    public function getConcertsByLocation(string $location) 
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.location = :loc')
            ->setParameter('loc', $location)
            ->orderBy('c.date', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param integer $musicCategoryId
     */
    public function getConcertsByMusicCategory(int $musicCategoryId) 
    {
        return $this->createQueryBuilder('c')
            ->join('c.artist_id','g')
            ->addSelect('g')
            ->where('g.music_category_id = :val')
            ->setParameter('val', $musicCategoryId)
            ->orderBy('c.date', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param string $dateStart
     * @param string $dateEnd
     */
    public function getConcertsByDate(string $dateStart, string $dateEnd) 
    {
        return $this->createQueryBuilder('c')
            ->where('c.date <= :e')
            ->andWhere('c.date >= :s')
            ->setParameter('e', $dateEnd)
            ->setParameter('s', $dateStart)
            ->orderBy('c.date', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param string $search
     */
    public function searchSomeData(string $search) 
    {
        return $this->createQueryBuilder('c')
            ->where('c.tourName LIKE :data')
            ->setParameter('data', '%'.$search.'%')
            ->orderBy('c.date', 'DESC')
            ->setMaxResults(5)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param string $search
     */
    public function searchAllData(string $search) 
    {
        return $this->createQueryBuilder('c')
            ->where('c.tourName LIKE :data')
            ->setParameter('data', '%'.$search.'%')
            ->orderBy('c.date', 'DESC')
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Concert[] Returns an array of Concert objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Concert
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
