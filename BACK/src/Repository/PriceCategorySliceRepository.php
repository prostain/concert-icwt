<?php

namespace App\Repository;

use App\Entity\PriceCategorySlice;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PriceCategorySlice|null find($id, $lockMode = null, $lockVersion = null)
 * @method PriceCategorySlice|null findOneBy(array $criteria, array $orderBy = null)
 * @method PriceCategorySlice[]    findAll()
 * @method PriceCategorySlice[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PriceCategorySliceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PriceCategorySlice::class);
    }

    // /**
    //  * @return PriceCategorySlice[] Returns an array of PriceCategorySlice objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PriceCategorySlice
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
