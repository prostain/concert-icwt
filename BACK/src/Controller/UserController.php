<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Repository\RoleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController; 
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\Exception\conte;
use App\Controller\AppBundle\Entity;
use Symfony\Component\Validator\Constraints\DateTime;




/**
 * @Route("/api")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/users/new", name="user_new", methods={"POST"})
     *
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function New(Request $req, UserRepository $userRepository, RoleRepository $roleRepository, EntityManagerInterface $em, UserPasswordEncoderInterface $encoder): Response
    {
        try
        {
            $request = json_decode(
                $req->getContent(),
                true
            );


            $user = $userRepository->find(1);
            $role = $roleRepository->findBy(array('name' => 'ROLE_USER'));
        
            $email = $request['email'];
            $password = $request['password'];
            $civility = $request['civility'];
            $lastname = $request['lastname'];
            $firstname = $request['firstname'];
            $pseudo = $lastname.".".$firstname."".rand(0,99);
            $address = $request['address'];
            $accomodationType = $request['accomodationType'];
            $additionalAddress = $request['additionalAddress'];
            $codePostal = $request['codePostal'];
            $city =$request['city'];
            $country = $request['country'];
            $phone = $request['phone'];
            $dateBirthday = $request['dateBirthday'];

            $userExist = $userRepository->findBy(array('email' => $email));

            $date = \DateTime::createFromFormat('d/m/Y',$dateBirthday);
            
            if($userExist == null)
            {
                $encoded = $encoder->encodePassword( new User(), $password);
                $user = new User(); 
                $user->setPassword($encoded)
                ->setEmail($email)
                ->setCivility($civility)
                ->setLastname($lastname)
                ->setFirstname($firstname)
                ->setAddress($address)
                ->setAccomodationType($accomodationType)
                ->setAdditionalAddress($additionalAddress)
                ->setCodePostal($codePostal)
                ->setCity($city)
                ->setCountry($country)
                ->setPhone($phone)
                ->setPseudo($pseudo)
                ->setDateBirthday($date)
                ->addRole($role[0]);
                $em->persist($user);
                $em->flush();
                
                return$this->json([
                    'status' => 200,
                    'message' => 'Votre compte a bien été entregistré.'
                ], 200);
            }
            else
            {
                return $this->json([
                    'status' => 500,
                    'message' => 'Cette adresse email est déjà atribuée à un compte.'
                ], 500);
            }
        }
        catch(NotEncodableValueException $e)
        {
            return $this->json([
                'status' => 400,
                'message' => $e->getMessage()
            ], 400);
        }
    }
}
