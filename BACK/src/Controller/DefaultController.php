<?php

namespace App\Controller;

use App\Entity\Newsletter;
use App\Form\NewsletterType;
use App\Repository\ArticleRepository;
use App\Repository\ConcertRepository;
use App\Repository\ImageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/api")
 */
class DefaultController extends AbstractController
{
    /**
     * @Route("/home", name="api_home", methods={"GET"})
     *
     * @param ArticleRepository $articleRepository
     * @param ConcertRepository $concertRepository
     * @param ImageRepository $imageRepository
     * @return JsonResponse
     */
    public function index(ArticleRepository $articleRepository, ConcertRepository $concertRepository, ImageRepository $imageRepository, SerializerInterface $serializer): JsonResponse
    {
        //Les 4 derniers articles
        $lastArticles = $articleRepository->findLast(4);

        //Les 8 derniers concerts
        $lastConcerts = $concertRepository->findLast(8);

        //Les 4 dernières images artistes/group
        $lastGroupImages = $imageRepository->findLastImageGroup(4);

        $result = $serializer->serialize(['lastArticles' => $lastArticles,'lastConcerts' => $lastConcerts,'lastGroupImages' => $lastGroupImages], 'json');

        return $this->json(json_decode($result, true),200, ['Access-Control-Allow-Origin' => '*']);
    }

     /**
      * @Route("/newsletter", name="api_newsletter", methods={"POST"})
      *
      * @param EntityManagerInterface $em
      * @return JsonResponse
      */
    public function addNewsletter(Request $request, EntityManagerInterface $em, ValidatorInterface $validatorInterface): JsonResponse
    {
        $emailConstraint = new Email();
        $emailConstraint->message = 'Email invalide';
         $data = json_decode(
            $request->getContent(),
            true
        );

        $email = $data['email'];

        $errors = $validatorInterface->validate(
            $email,
            $emailConstraint
        );

        if(0 === count($errors))
        {
            if($email !== null){
                try {
                    $newsletter = new Newsletter();
                    $newsletter->setDate(new \DateTime())->setEmail($email);
    
                    $em->persist($newsletter);
                    $em->flush();
    
                    return $this->json([
                        'status' => 201,
                        'message' => 'Votre mail a bien été sauvegardée. Rendez-vous sur votre compte dans la partie Newsletters pour reçevoir les dernières actualités.'.$email
                    ], 201, ['Access-Control-Allow-Origin' => '*']);
    
                } catch(NotEncodableValueException $e){
                    return $this->json([
                        'status' => 400,
                        'message' => 'Veuillez vous connecter'
                    ], 400);
                }
            } else {
                return $this->json([
                    'status' => 400,
                    'message' => 'Email vide'
                ], 400);
            }
        } else {
            $errorMessage = $errors[0]->getMessage();
            return $this->json([
                'status' => 400,
                'message' => $errorMessage
            ], 400);
        }
    }
}
