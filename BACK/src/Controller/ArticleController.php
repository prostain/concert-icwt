<?php

namespace App\Controller;

use ApiPlatform\Core\DataProvider\PaginatorInterface;
use App\Entity\Article;
use App\Entity\Image;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class ArticleController extends AbstractController
{
    /**
     * @Route("/articles", name="api_article", methods={"GET"})
     * @param ArticleRepository $articleRepository
     * @return JsonResponse
     */
    public function getArticles(ArticleRepository $articleRepository): JsonResponse
    {
        return $this->json($articleRepository->getAllArticle());
    }

     /**
      * @Route("/articles/{id}", name="api_article_detail", methods={"GET"})
      *
      * @param int $id
      * @param ArticleRepository $articleRepository
      * @return JsonResponse
      */
    public function detailArticle($id, ArticleRepository $articleRepository ): JsonResponse
    {
        return $this->json(json_decode($articleRepository->find($id), true), 200, ['Access-Control-Allow-Origin' => '*']);
    }
}
