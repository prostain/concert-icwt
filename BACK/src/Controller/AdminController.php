<?php

namespace App\Controller;

use App\Entity\Concert;
use App\Entity\Image;
use App\Entity\MusicCategory;
use App\Entity\PriceCategoryType;
use App\Entity\PriceCategory;
use App\Repository\ArticleRepository;
use App\Repository\ConcertRepository;
use App\Repository\RoomRepository;
use App\Repository\GroupRepository;
use App\Repository\OrderRepository;
use App\Repository\PriceCategoryRepository;
use App\Repository\PriceCategoryTypeRepository;
use App\Repository\MusicCategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;

/**
 * @Route("/api/admin")
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/concerts", name="api_admin_concert", methods={"GET"})
     *
     * @param ConcertRepository $concertRepository
     * @return JsonResponse
     */
    public function getAdminConcerts(ConcertRepository $concertRepository): JsonResponse
    {
        return $this->json($concertRepository->findAll());
    }

    /**
     * @Route("/orders", name="api_admin_order", methods={"GET"})
     *
     * @param OrderRepository $orderRepository
     * @return JsonResponse
     */
    public function getAdminOrders(OrderRepository $orderRepository): JsonResponse
    {
        return $this->json($orderRepository->findAll());
    }

    /**
     * @Route("/articles", name="api_admin_article", methods={"GET"})
     *
     * @param ArticleRepository $articleRepository
     * @return JsonResponse
     */
    public function getAdminArticles(ArticleRepository $articleRepository): JsonResponse
    {
        return $this->json($articleRepository->findAll());
    }

    /**
      * @Route("/groups", name="api_admin_group", methods={"GET"})
      *
      * @param GroupRepository $groupRepository
      * @return JsonResponse
      */
    public function getGroupsForNewConcert(GroupRepository $groupRepository): JsonResponse
    {
        return $this->json($groupRepository->findAll());
    }
  
    /**
     * @Route("/concerts/new", name="api_admin_new_concert", methods={"POST"})
     *
     * @param Request $request
     * @param GroupRepository $groupRepository
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function newConcert(
        Request $req,
         GroupRepository $groupRepository,
          RoomRepository $roomRepository,
            EntityManagerInterface $em,
            MusicCategoryRepository $musicCategoryRepository): JsonResponse
    {
        $request = json_decode(
            $req->getContent(),
            true
        );
        //On récupère l'artiste/groupe
        $artistId = $request['artistId'];   
        $roomId = $request['roomId']; 
        $artist = $groupRepository->find($artistId);
        $room = $roomRepository->find($roomId);

        //On récupère les infos du concert ajoutées
        $tourNameConcert = $request['tourName'];
        $dateConcert = $request['date'];
        $descriptionConcert = $request['descriptionConcert'];
        $hourConcert = $request['hour'];
        $hourOpeningConcert = $request['hourOpening'];
        $locationConcert = "exempleLocation";
        $priceConcert = $request['priceMax'];
        $decliningRatePercentageConcert = $request['decliningRatePercentage'];
        $hasParkingConcert = $request['hasParking'];
        $hasRestaurantConcert = $request['hasRestaurant'];
        $tourReferenceConcert = $request['tourReference'];
        $priceCategories = $request['priceCategories'];
        
        //pas sûr
        //$imageFileConcert = $request['image']->getData();

        try {

            $image = new Image();
            $image->setUrl("https://www.google.com/imgres?imgurl=https%3A%2F%2Fimages.radio-canada.ca%2Fv1%2Fici-info%2F16x9%2Fconcert-gens.jpg%3Fim%3DResize%3D(1250)%3BComposite%3D(type%3DURL%2Curl%3Dhttps%3A%2F%2Fimages.radio-canada.ca%2Fv1%2Fassets%2Felements%2F16x9%2Foutdated-content-2018.png)%2Cgravity%3DSouthEast%2Cplacement%3DOver%2Clocation%3D(0%2C0)%2Cscale%3D1&imgrefurl=https%3A%2F%2Fici.radio-canada.ca%2Fnouvelle%2F1141236%2Fbavardage-pendant-concerts-silence-obligatoire&tbnid=mgjRhJ__fVyoLM&vet=12ahUKEwimsaCZltHuAhUOXRoKHf_TCLAQMygFegUIARCcAQ..i&docid=Um0beDC2M2dLxM&w=1250&h=703&q=concert%20url&ved=2ahUKEwimsaCZltHuAhUOXRoKHf_TCLAQMygFegUIARCcAQ"); // $pathImage
            $em->persist($image);
            $em->flush();

            //$time =new \DateTime($dateConcert);
            //On ajoute le concert
            $concert = new Concert();
            $concert->setArtist($artist)
                ->setTourName($tourNameConcert)
                ->setDate($dateConcert)
                ->setDescription($descriptionConcert)
                ->setHour($hourConcert)
                ->setHourOpening($hourOpeningConcert)
                ->setLocation($locationConcert)
                ->setPrice($priceConcert)
                ->setDecliningRatePercentage($decliningRatePercentageConcert)
                ->setHasParking($hasParkingConcert)          
                ->setRoom($room)
                ->setPriceCategories($priceCategories)
                ->setHasRestaurant($hasRestaurantConcert)
                ->setTourReference($tourReferenceConcert);
                $concert->setImage($image);

            /*if ($imageFileConcert) {
                $originalFilename = pathinfo($imageFileConcert->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename . '-' . uniqid() . '.' . $imageFileConcert->guessExtension();

                try {
                    $imageFileConcert->move(
                        $this->getParameter('image_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                }
                $pathImage = "/img/uploads/" . $newFilename;

                $image = new Image();
                $image->setUrl($pathImage)
                ->addConcert($concert);
                $em->persist($image);
            }*/

            $em->persist($concert);
            $em->flush();

            return $this->json([
                'status' => 201,
                'message' => 'Votre Concert a bien été sauvegardée.'
            ], 201);

        } catch(NotEncodableValueException $e){
            return $this->json([
                'status' => 400,
                'message' => $e->getMessage()
            ], 400);
        }
    }
   
    /**
     * @Route("/concerts/{id}/edit", name="api_admin_edit_concert", methods={"PUT"})
     *
     * @param Request $request
     * @param Concert $concert
     * @param GroupRepository $groupRepository
     * @param PriceCategoryRepository $priceCategoryRepository
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function editConcert(Request $req, Concert $concert, GroupRepository $groupRepository, PriceCategoryRepository $priceCategoryRepository, EntityManagerInterface $em): JsonResponse
    {
        $request = json_decode(
            $request->getContent(),
            true
        );
        //On récupère l'artiste/groupe
        $artistId = $request->get('artistId');
        $artist = $groupRepository->find($artistId);

        ///////////////TO DO////////////////
        
        // //On récupère les catégories
        // $priceCategoriesId[] = $request->get('categoryPriceId');
        // $priceCategories = [];
        // foreach($priceCategoriesId as $priceCategoryId)
        // {
        //     $categoryPrice = new PriceCategory();
        //     $priceCategories[] = $priceCategoryRepository->find($priceCategoryId);
        // }

        //On récupère les infos du concert ajoutées
        $tourNameConcert = $request->get('tourName');
        $dateConcert = $request->get('date');
        $descriptionConcert = $request->get('descriptionConcert');
        $hourConcert = $request->get('hour');
        $hourOpeningConcert = $request->get('hourOpening');
        $locationConcert = $request->get('location');
        $priceConcert = $request->get('priceMax');
        $decliningRatePercentageConcert = $request->get('decliningRatePercentage');
        $hasParkingConcert = $request->get('hasParking');
        $hasRestaurantConcert = $request->get('hasRestaurant');
        $tourReferenceConcert = $request->get('tourReference');
        $imageFileConcert = $request->get('image')->getData();

        try {

            //On modifie le concert
            $concert->setArtist($artist)
                ->setTourName($tourNameConcert)
                ->setDate($dateConcert)
                ->setDescription($descriptionConcert)
                ->setHour($hourConcert)
                ->setHourOpening($hourOpeningConcert)
                ->setLocation($locationConcert)
                ->setPrice($priceConcert)
                ->setDecliningRatePercentage($decliningRatePercentageConcert)
                ->setHasParking($hasParkingConcert)
                ->setHasRestaurant($hasRestaurantConcert)
                ->setTourReference($tourReferenceConcert);

            if ($imageFileConcert) {
                $originalFilename = pathinfo($imageFileConcert->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename . '-' . uniqid() . '.' . $imageFileConcert->guessExtension();

                try {
                    $imageFileConcert->move(
                        $this->getParameter('image_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                }
                $pathImage = "/img/uploads/" . $newFilename;

                $image = new Image();
                $image->setUrl($pathImage)
                ->addConcert($concert);
                $em->persist($image);
            }

            $em->persist($concert);
            $em->flush();

            return $this->json([
                'status' => 201,
                'message' => 'Le concert a bien été modifié.'
            ], 201);

        } catch(NotEncodableValueException $e){
            return $this->json([
                'status' => 400,
                'message' => $e->getMessage()
            ], 400);
        }
    }

     /**
      * @Route("/concerts/{id}", name="api_admin_delete_concert", methods={"DELETE"})
      *
      * @param Request $request
      * @param Concert $concert
      * @return JsonResponse
      */
    public function deleteConcert(Request $request, Concert $concert): JsonResponse
    {
        if ($this->isCsrfTokenValid('delete'.$concert->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($concert);
            $entityManager->flush();
        }

        return $this->json([
            'status' => 201,
            'message' => 'Le concert a bien été supprimé.'
        ], 201);
    }
}