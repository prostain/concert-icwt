<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Validator\ValidatorInterface;


/**
 * @Route("/api")
 */
class ParticularsController extends AbstractController
{
     /**
      * @Route("/users/{id}", name="api_coordonnees", methods={"PUT"})
      *
      * @param Request $request
      * @param User $user
      * @param UserRepository $userRepository
      * @param EntityManagerInterface $em
      * @param ValidatorInterface $validatorInterface
      * @return JsonResponse
      */
    public function checkParticulars(Request $request, User $user, UserRepository $userRepository, EntityManagerInterface $em, ValidatorInterface $validatorInterface): JsonResponse
    {
        //On récupère l'user
        $userId = $request->get('userId');
        $user = $userRepository->find($userId);

        //On récupère les coordonnées du client en question
        $emailConstraint = new Email();
        $emailConstraint->message = 'Email invalide';

        $email = $request->get('email');

        $errors = $validatorInterface->validate(
            $email,
            $emailConstraint
        );

        //On récupère toutes ses informations
        $civility = $request->get('civility');
        $lastname = $request->get('lastname');
        $firstname = $request->get('firstname');
        $address = $request->get('address');
        $accomodationType = $request->get('accomodationType');
        $additionalAddress = $request->get('additionalAddress');
        $codePostal = $request->get('codePostal');
        $city = $request->get('city');
        $country = $request->get('country');
        $phone = $request->get('phone');
        $dateBirthday = $request->get('dateBirthday');

        try 
        {
            if(0 === count($errors))
            {
                $user->setEmail($email);
            } else {
                $errorMessage = $errors[0]->getMessage();
                return $this->json([
                    'status' => 400,
                    'message' => $errorMessage
                ], 400);
            }

            //Le client modifie ses informations
            $user->setCivility($civility)
            ->setLastname($lastname)
            ->setFirstname($firstname)
            ->setAddress($address)
            ->setAccomodationType($accomodationType)
            ->setAdditionalAddress($additionalAddress)
            ->setCodePostal($codePostal)
            ->setCity($city)
            ->setCountry($country)
            ->setPhone($phone)
            ->setDateBirthday($dateBirthday);

            $em->persist($user);
            $em->flush();

            return $this->json([
                'status' => 201,
                'message' => 'Les informations ont bien été modifiées et enregistrées.'
            ], 201, ['groups' => 'read:users']);

        } catch(NotEncodableValueException $e){
            return $this->json([
                'status' => 400,
                'message' => $e->getMessage()
            ], 400);
        }
    }
}
