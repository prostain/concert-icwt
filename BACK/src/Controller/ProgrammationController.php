<?php

namespace App\Controller;

use App\Repository\ConcertRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class ProgrammationController extends AbstractController
{
    /**
     * @Route("/programmation", name="api_programmation_concerts_filtered", methods={"GET"})
     *
     * @param Request $request
     * @param ConcertRepository $concertRepository
     * @return JsonResponse
     */
    public function getProgrammationConcertsBySort(Request $request, ConcertRepository $concertRepository): JsonResponse
    {
        $location = $request->get('location');
        $musicCategoryId = (int) $request->get('musicCategoryId');
        $dateStart = $request->get('dateStart');
        $dateEnd = $request->get('dateEnd');

        return $this->json($concertRepository->getConcertsBySort($location, $musicCategoryId, $dateStart, $dateEnd), 200, ['Access-Control-Allow-Origin' => '*']);
    }
}
