<?php

namespace App\Controller;

use App\Entity\Concert;
use App\Entity\BookingLine;
use App\Repository\ConcertRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class ConcertController extends AbstractController
{
    /**
    * @Route("/concerts/{id}/places", name="places_concert")
    * @param int $id
    * @param ConcertRepository $concertRepository
    * @return JsonResponse
    */
    public function ReservedPlaces($id, ConcertRepository $concertRepository ): JsonResponse
    {
        return $this->json($concertRepository->getReservedPlaces($id));
    }
}
