<?php

namespace App\Controller;

use App\Entity\Booking;
use App\Entity\BookingLine;
use App\Entity\TypeTicket;
use App\Entity\Concert;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Repository\BookingRepository;
use App\Repository\BookingLineRepository;
use App\Repository\TypeTicketRepository;
use App\Repository\ConcertRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController; 
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\conte;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/api")
 */
class BookingController extends AbstractController
{
    /**
     * @Route("/booking/new", name="booking_new", methods={"POST"})
     *
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function New(Request $req, EntityManagerInterface $em, UserRepository $userRepository, TypeTicketRepository $typeTicketRepository, ConcertRepository $concertRepository, BookingRepository $bookingRepository): Response
    {
        try
        {
            $request = json_decode(
                $req->getContent(),
                true
            );

            $userId =$request['userId'];
            $concertId = $request['concertId'];
            $billingOption = $request['billingOption'];
            $seats = $request['seats'];
            $typeTicket = $typeTicketRepository->find($billingOption);

            $user = $userRepository->find($userId);
            $concert = $concertRepository->find($concertId);

            $nbSeats = $concert->getRoom()->getNumberSeat();

            $percentage = $concert->getDecliningRatePercentage();

            $priceConcert = $concert->getPrice();

            $total = 0;

            foreach($seats as $seat)
            {
                for ($i = 0; $i < $nbSeats/11; $i++) {
                    if ( $i * 12 < $seat && $seat <= ($i+1) * 12){

                        $priceFactor = 1 - $percentage/100*$i;
                        $price = ceil($priceConcert * $priceFactor);
                        $total += $price;
                    }
                }
            }

            $typeTicketPrice = $typeTicket->getPrice();
            $total += $typeTicketPrice;
            $booking = new Booking();
            $booking->setPerson($user)
            ->setPrice(round($total,2))
            ->setPriceInsurance(0)
            ->setHasInsuranceCancellation(false)
            ->setHasBookRestaurant(false)
            ->setHasBookParking(false)
            ->setStatus(2)
            ->setDate(new \DateTime())
            ->setNumero($concertId);

            $em->persist($booking);
            $em->flush();



            foreach($seats as $seat)
            {
               $bookingLine = new BookingLine();
               $bookingLine->setConcert($concert)
               ->setTypeTicket($typeTicket)
               ->setBooking($booking)
               ->setSeat($seat);
               $em->persist($bookingLine);
               $em->flush();
            }

            return $this->json($booking, 200);
        }
        catch(NotEncodableValueException $e)
        {
            return $this->json([
                'status' => 400,
                'message' => $e->getMessage()
            ], 400);
        }
    }
}
