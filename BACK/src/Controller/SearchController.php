<?php

namespace App\Controller;

use App\Entity\Concert;
use App\Repository\ConcertRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/api")
 */
class SearchController extends AbstractController
{
    /**
     * @Route("/search", name="api_search", methods={"GET"})
     *
     * @param Request $request
     * @param ConcertRepository $concertRepository
     * @return JsonResponse
     */
    public function Search(Request $request, ConcertRepository $concertRepository): JsonResponse
    {
        $req = $request->get('searchText');
        return $this->json($concertRepository->searchSomeData($req), 200, ['Access-Control-Allow-Origin' => '*']);
    }

    // /**
    // * @Route("/searchAll", name="api_search", methods={"GET"})
    //  *
    //  * @param Request $request
    //  * @param EntityManagerInterface $em
    //  * @return JsonResponse
    //  */
    // public function SearchAll(Request $request): JsonResponse
    // {
    //     $req = $request->get('search');
    //     $results = $this->getDoctrine()->getRepository(Concert::class)->searchAllData($req);
  
    //     return $this->json($results);
    // }
  

}
