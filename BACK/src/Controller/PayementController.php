<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class PayementController extends AbstractController
{
    /**
     * @Route("/payement", name="payement")
     */
    public function index(): Response
    {
        $secretAccount = $this->getParameter('PAYPAL_SANDBOX_ACCOUNT');
        $secretId = $this->getParameter('PAYPAL_CLIENT_ID');

        return $this->render('payement/index.html.twig', [
            'controller_name' => 'PayementController',
        ]);
    }
}
